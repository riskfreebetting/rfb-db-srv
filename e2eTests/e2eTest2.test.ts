import { createDB } from "../scripts/db/db.ts";
import { ReturnOutcome } from "../scripts/factories/ReturnBet.ts";
import { ReturnMatch } from "../scripts/factories/ReturnMatch.ts";
import { BetTypeInputs } from "../scripts/inputs/BetTypeInputs.ts";
import { BookmakerInputs } from "../scripts/inputs/BookmakerInputs.ts";
import {
  BetInputs,
  MatchInputs,
  OddsInputs,
  OutcomeInputs,
} from "../scripts/inputs/MatchInputs.ts";
import { TeamInputs } from "../scripts/inputs/TeamInputs.ts";
import { createPostBody } from "../scripts/RequestFormats.ts";
import {
  handleGetMatches,
  handleUpdateMatch,
  handlePostBetType,
  handlePostBookmaker,
  handlePostMatch,
  handlePostTeam,
  handlePostCompetition,
  handlePostSport,
  handlePostCountry,
} from "../scripts/server.ts";
import { trace, UUIDString } from "../scripts/utils.ts";
import describe from "../unitTests/describe.ts";

describe("E2E 2: Update Matches", async (assert) => {
  const db = await createDB({
    id: "e2e test 2 DB",
  });

  const bookmaker = (await handlePostBookmaker(db)(createPostBody({
    source: "test",
    date: 0,
    inputs: {
      name: "Bwin",
    } as BookmakerInputs,
  }))
).result;

  const country = (await handlePostCountry(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Deutschland",
      alternativeNames: {
        [bookmaker?.id as string]: ["Deutschland"],
      }
    }
  })).result;

  const sport = (await handlePostSport(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Fußball",
      alternativeNames: {
        [bookmaker?.id as string]: ["Fußball"],
      }
    }
  })).result;

  const competition = (await handlePostCompetition(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Bundesliga",
      alternativeNames: {
        [bookmaker?.id as string]: ["Bundesliga"],
      },
      sportId: sport?.id,
      countryId: country?.id
    }
  })).result;

  const betType = (await handlePostBetType(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Winner",
        outcomeNames: ["1", "X", "2"],
      } as BetTypeInputs,
    }))
  ).result;

  const teams = (await Promise.all([
    handlePostTeam(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "FC Bayern München",
        alternativeNames: {
          [bookmaker?.id || ""]: ["FC Bayern München"]
        }
      } as TeamInputs,
    })),
    handlePostTeam(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Borussia Dortmund",
        alternativeNames: {
          [bookmaker?.id || ""]: ["Borussia Dortmund"]
        }
      } as TeamInputs,
    })),
  ])).map((res) => res.result);

  await handlePostMatch(db)(createPostBody({
    source: "test",
    date: 1608393600000,
    inputs: {
      competitionId: competition?.id,
      teams: [
        { teamId: teams[0]?.id },
        { teamId: teams[1]?.id },
      ],
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1608399000000,
      endDate: 1608405300000,
      odds: [
        {
          bookmakerId: bookmaker?.id,
          bets: [
            {
              typeId: betType?.id,
              date: 1608397200000,
              outcomes: [
                {
                  name: betType?.outcomeNames[0].id,
                  odd: 2.0,
                },
                {
                  name: betType?.outcomeNames[1].id,
                  odd: 3.1,
                },
                {
                  name: betType?.outcomeNames[2].id,
                  odd: 4.1,
                },
              ] as OutcomeInputs[],
            }
          ] as BetInputs[],
        },
      ] as OddsInputs[],
    } as MatchInputs,
  }));

  const matches = await (await handleGetMatches(db)({})).result as ReturnMatch[];
  const gotMatch = matches.find(m => m.teams[0].name === "FC Bayern München" && m.teams[1].name === "Borussia Dortmund");

  assert({
    given: "a database where a match was posted",
    should: "be able to find the match by the team names by getting all matches",
    actual: gotMatch !== undefined,
    expected: true
  });

  const updateDate = 1608397200000;
  const updated = (await handleUpdateMatch(db)({
    inputs: {
      teams: [
        { teamId: gotMatch?.teams[0]?.team?.id },
        { teamId: gotMatch?.teams[1]?.team?.id },
      ],
      result: gotMatch?.result,
      status: gotMatch?.status,
      currentMinute: gotMatch?.currentMinute,
      startDate: gotMatch?.startDate,
      endDate: gotMatch?.endDate,
      odds: [
        {
          bookmakerId: bookmaker?.id,
          bets: [
            {
              typeId: betType?.id,
              date: 1608397200000,
              outcomes: [
                {
                  name: betType?.outcomeNames[0].id,
                  odd: 2.3,
                },
                {
                  name: betType?.outcomeNames[1].id,
                  odd: 3.5,
                },
                {
                  name: betType?.outcomeNames[2].id,
                  odd: 2.8,
                },
              ] as OutcomeInputs[],
            }
          ] as BetInputs[],
        },
      ] as OddsInputs[],
    } as MatchInputs,
    source: "test",
    date: updateDate
  }, {
    id: gotMatch?.id as UUIDString
  })).result;

  const matches2 = await (await handleGetMatches(db)({})).result as ReturnMatch[];
  const gotMatch2 = matches2.find(m => m.teams[0].name === "FC Bayern München" && m.teams[1].name === "Borussia Dortmund");

  assert({
    given: "a database where a match was overwritten",
    should: "be able to get a match with the updated odds",
    actual: (gotMatch2?.odds[0]?.bets[0]?.outcomes as ReturnOutcome[])[0]?.odd,
    expected: 2.3
  });

  const matches3 = await (await handleGetMatches(db)({ beforeDate: updateDate.toString() })).result as ReturnMatch[];
  const gotMatch3 = matches3.find(m => m.teams[0].name === "FC Bayern München" && m.teams[1].name === "Borussia Dortmund");

  assert({
    given: "a database where a match was overwritten and a beforeDate",
    should: "be able to get the match odds from before the update",
    actual: (gotMatch3?.odds[0]?.bets[0]?.outcomes as ReturnOutcome[])[0]?.odd,
    expected: 2.0
  });
});
