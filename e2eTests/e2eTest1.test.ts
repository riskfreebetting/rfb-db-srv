import { createDB } from "../scripts/db/db.ts";
import { ReturnBetType } from "../scripts/factories/ReturnBetType.ts";
import { ReturnBookmaker } from "../scripts/factories/ReturnBookmaker.ts";
import { ReturnSport } from "../scripts/factories/ReturnSport.ts";
import { ReturnTeam } from "../scripts/factories/ReturnTeam.ts";
import { BetTypeInputs } from "../scripts/inputs/BetTypeInputs.ts";
import { BookmakerInputs } from "../scripts/inputs/BookmakerInputs.ts";
import {
  BetInputs,
  MatchInputs,
  OddsInputs,
  OutcomeInputs,
} from "../scripts/inputs/MatchInputs.ts";
import { TeamInputs } from "../scripts/inputs/TeamInputs.ts";
import { createPostBody } from "../scripts/RequestFormats.ts";
import {
  handleGetBetTypes,
  handleGetBookmakers,
  handleGetCountries,
  handleGetMatches,
  handleGetSports,
  handleGetTeams,
  handlePostBetType,
  handlePostBookmaker,
  handlePostCompetition,
  handlePostCountry,
  handlePostMatch,
  handlePostSport,
  handlePostTeam,
} from "../scripts/server.ts";
import { generateUUIDString } from "../scripts/utils.ts";
import describe from "../unitTests/describe.ts";

describe("E2E 1: GET + POST Matches", async (assert) => {
  const db = await createDB({
    id: "e2e test 1 DB",
  });
  
  const bookmakers = (await Promise.all([
    handlePostBookmaker(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Bwin",
      } as BookmakerInputs,
    })),
    handlePostBookmaker(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "bet-at-home",
      } as BookmakerInputs,
    })),
  ])).map((res) => res.result);

  const country = (await handlePostCountry(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Deutschland",
      alternativeNames: {
        [bookmakers[0]?.id as string]: ["Deutschland"],
        [bookmakers[1]?.id as string]: ["Deutschland"],
      }
    }
  })).result;

  const sport = (await handlePostSport(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Fußball",
      alternativeNames: {
        [bookmakers[0]?.id as string]: ["Fußball"],
        [bookmakers[1]?.id as string]: ["Fußball"],
      }
    }
  })).result;

  const competition = (await handlePostCompetition(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "Bundesliga",
      alternativeNames: {
        [bookmakers[0]?.id as string]: ["Bundesliga"],
        [bookmakers[1]?.id as string]: ["1. Bundesliga"],
      },
      sportId: sport?.id,
      countryId: country?.id
    }
  })).result;

  const betTypes = (await Promise.all([
    handlePostBetType(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Winner",
        outcomeNames: ["1", "X", "2"],
      } as BetTypeInputs,
    })),
    handlePostBetType(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Doppelte Chance",
        outcomeNames: ["1X", "12", "X2"],
      } as BetTypeInputs,
    })),
  ])).map((res) => res.result);

  const teams = (await Promise.all([
    handlePostTeam(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "FC Bayern München",
        alternativeNames: bookmakers.reduce((acc, bookmaker) => (bookmaker 
          ? {
            ...acc,
            [bookmaker.id]: "FC Bayern München"
          } : acc), {})
      } as TeamInputs,
    })),
    handlePostTeam(db)(createPostBody({
      source: "test",
      date: 0,
      inputs: {
        name: "Borussia Dortmund",
        alternativeNames: bookmakers.reduce((acc, bookmaker) => (bookmaker 
          ? {
            ...acc,
            [bookmaker.id]: "Borussia Dortmund"
          } : acc), {})
      } as TeamInputs,
    })),
  ])).map((res) => res.result);

  const match = (await handlePostMatch(db)(createPostBody({
    source: "test",
    date: 0,
    inputs: {
      competitionId: competition?.id,
      teams: [
        { teamId: teams[0]?.id },
        { teamId: teams[1]?.id },
      ],
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1608399000000,
      endDate: 1608405300000,
      odds: [
        {
          bookmakerId: bookmakers[0]?.id,
          bets: [
            {
              typeId: betTypes[0]?.id,
              date: 1608398000000,
              outcomes: [
                {
                  name: betTypes[0]?.outcomeNames[0].id,
                  odd: 2.0,
                },
                {
                  name: betTypes[0]?.outcomeNames[1].id,
                  odd: 3.1,
                },
                {
                  name: betTypes[0]?.outcomeNames[2].id,
                  odd: 4.1,
                },
              ] as OutcomeInputs[],
            },
            {
              typeId: betTypes[1]?.id,
              date: 1608398000000,
              outcomes: [
                {
                  name: betTypes[1]?.outcomeNames[0].id,
                  odd: 1.22,
                },
                {
                  name: betTypes[1]?.outcomeNames[1].id,
                  odd: 1.34,
                },
                {
                  name: betTypes[1]?.outcomeNames[2].id,
                  odd: 1.77,
                },
              ] as OutcomeInputs[],
            },
          ] as BetInputs[],
        },
        {
          bookmakerId: bookmakers[1]?.id,
          bets: [
            {
              typeId: betTypes[0]?.id,
              date: 1608398000000,
              outcomes: [
                {
                  name: betTypes[0]?.outcomeNames[0].id,
                  odd: 1.9,
                },
                {
                  name: betTypes[0]?.outcomeNames[1].id,
                  odd: 3.05,
                },
                {
                  name: betTypes[0]?.outcomeNames[2].id,
                  odd: 5.2,
                },
              ] as OutcomeInputs[],
            },
            {
              typeId: betTypes[1]?.id,
              date: 1608398000000,
              outcomes: [
                {
                  name: betTypes[1]?.outcomeNames[0].id,
                  odd: 1.17,
                },
                {
                  name: betTypes[1]?.outcomeNames[1].id,
                  odd: 1.39,
                },
                {
                  name: betTypes[1]?.outcomeNames[2].id,
                  odd: 1.92,
                },
              ] as OutcomeInputs[],
            },
          ] as BetInputs[],
        } as OddsInputs,
      ] as OddsInputs[],
    } as MatchInputs,
  }))).result;


  const savedBookmakers = (await handleGetBookmakers(db)()).result;
  assert({
    given: "an empty database where bookmakers were inserted",
    should: "return all existing bookmakers",
    actual: savedBookmakers?.every((bm: ReturnBookmaker) =>
      bookmakers.some((bm2) => bm.name === bm2?.name)
    ),
    expected: true,
  });

  const savedSports = (await handleGetSports(db)()).result;
  assert({
    given: "an empty database where a sport were inserted",
    should: "return one existing sports",
    actual: savedSports?.length === 1 && savedSports[0].name === sport?.name,
    expected: true,
  });

  const savedCountries = (await handleGetCountries(db)()).result;
  assert({
    given: "an empty database where countries were inserted",
    should: "return one existing country",
    actual: savedCountries?.length === 1 && savedCountries[0].name === country?.name,
    expected: true,
  });

  const savedBetTypes = (await handleGetBetTypes(db)()).result;
  assert({
    given: "an empty database where bettypes were inserted",
    should: "return all existing bettypes",
    actual: savedBetTypes?.every((bt: ReturnBetType) =>
      betTypes.some((bt2) => bt.name === bt2?.name)
    ),
    expected: true,
  });

  const savedTeams = (await handleGetTeams(db)()).result;
  assert({
    given: "an empty database where teams were inserted",
    should: "return all existing teams",
    actual: savedTeams?.every((t: ReturnTeam) =>
      teams.some((t2) => t.name === t2?.name)
    ),
    expected: true,
  });

  const [savedMatch] = (await handleGetMatches(db)({
    source: "test"
  })).result ?? [];
  assert({
    given: "an empty database where matches were inserted",
    should: "return all existing matches",
    actual: savedMatch,
    expected: match,
  });
});