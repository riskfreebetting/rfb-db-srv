import { createFsDB } from "../scripts/db/fsDB.ts";
import { TeamSaveFormat } from "../scripts/db/saveFormats/TeamSaveFormat.ts";
import { UUIDString } from "../scripts/utils.ts";

const db = await createFsDB({ id: "tipico" });

const teams = await db.retrieveAll("teams");

const teamsWithSeveralAlternativeNames = teams.filter((t: TeamSaveFormat) => {
  let keys = Object.keys(t.alternativeNames);
  
  let i = keys.reduce((acc, key) => acc + t.alternativeNames[key].length, 0)

  return i > 1;
});

console.log(teamsWithSeveralAlternativeNames);