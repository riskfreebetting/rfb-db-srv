import { createFsDB } from "../scripts/db/fsDB.ts";
import { CompetitionSaveFormat } from "../scripts/db/saveFormats/CompetitionSaveFormat.ts";

const db = await createFsDB({ id: "bwin" });

const teams = await db.retrieveAll("competitions");

const teamsWithSeveralAlternativeNames = teams.filter((t: CompetitionSaveFormat) => {
  let keys = Object.keys(t.alternativeNames);
  
  let i = keys.reduce((acc, key) => acc + t.alternativeNames[key].length, 0)

  return i > 1;
});

console.log(teamsWithSeveralAlternativeNames.length, teamsWithSeveralAlternativeNames);