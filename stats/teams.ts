import { createFsDB } from "../scripts/db/fsDB.ts";
import { MatchSaveFormat } from "../scripts/db/saveFormats/MatchSaveFormat.ts";
import { MatchTeamSaveFormat } from "../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import { UUIDString } from "../scripts/utils.ts";

const db = await createFsDB({ id: "dev" });

const matchTeams = await db.retrieveAll("matchTeams");
const matches = await db.retrieveAll("matches");

const duplicateTeamMatches = matches.reduce((acc: MatchSaveFormat[][], m: MatchSaveFormat) => (
  acc.concat([
    m.childIds.reduce((acc: MatchSaveFormat[], matchTeamId1: UUIDString) => {
      const matchTeam1 = matchTeams.find((team: MatchTeamSaveFormat) => team.id === matchTeamId1);

      return acc.concat(matches
        .filter((m2: MatchSaveFormat) => {
          return m2.childIds.some(matchTeamId2 => {
            const matchTeam2 = matchTeams.find((team: MatchTeamSaveFormat) => team.id === matchTeamId2);

            return matchTeam1.teamId === matchTeam2.teamId;
          });
        })
        .filter((m2: MatchSaveFormat) => m2.id !== m.id)
      );
    }, [])
  ]).filter((ms: MatchSaveFormat[]) => ms.length > 1)
), []);

console.log(duplicateTeamMatches);