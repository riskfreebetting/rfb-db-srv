import { createFsDB } from "../scripts/db/fsDB.ts";
import { trace } from "../scripts/utils.ts";
import { MatchSaveFormat } from "../scripts/db/saveFormats/MatchSaveFormat.ts";

const db = await createFsDB({ id: "dev" });

const matches = await db.retrieveAll("matches");
console.log(matches.length);
const duplicates = await matches.reduce(async (acc: Promise<{m: MatchSaveFormat, otherId: string}[]>, m1: MatchSaveFormat) => {
  const otherId = await matches.reduce(async (acc: Promise<string>, m2: MatchSaveFormat) => (
    ((await acc) || 
    (
      m2.id !== m1.id &&
      m2.startDate === m1.startDate && 
      m2.competitionId === m1.competitionId &&
      await m1.childIds.reduce(async (acc: Promise<boolean>, childId1, i) => (
        (await acc) && (await db.retrieve("matchTeams", childId1))?.teamId === (await db.retrieve("matchTeams", m2.childIds[i]))?.teamId
      ), Promise.resolve(true))) ?
        await acc || m2.id :
        acc
    )
  ), Promise.resolve(""));

  return otherId ? 
    (await acc).concat({ m: m1, otherId }) :
    acc;  
}, Promise.resolve([]));

const withoutDoubles = duplicates/* .reduce((acc: { m: MatchSaveFormat, otherId: string }[], d1: { m: MatchSaveFormat, otherId: string }) => {
  const idx = acc.findIndex((d2: { m: MatchSaveFormat, otherId: string }) => d2.otherId === d1.m.id);

  return acc.filter((_: any, i: number) => i !== idx);
}, duplicates) */;

console.log(withoutDoubles.length);
console.log(withoutDoubles);