import { createFsDB } from "../scripts/db/fsDB.ts";
import { MatchSaveFormat } from "../scripts/db/saveFormats/MatchSaveFormat.ts";
import { OddsSaveFormat } from "../scripts/db/saveFormats/OddsSaveFormat.ts";
import { BetSaveFormat } from "../scripts/db/saveFormats/BetSaveFormat.ts";
import { UUIDString } from "../scripts/utils.ts";

const db = await createFsDB({ id: "bwin_0" });

const matches = await db.retrieveAll("matches");

const betTypes = await matches.reduce(async (acc: Promise<Record<UUIDString, number>>, match: MatchSaveFormat) => {
  const odds = await Promise.all(match.oddsIds.map(id => db.retrieve("odds", id)));

  const betTypes = await Promise.all(odds.map(async (odd: OddsSaveFormat) => {
    const bets: BetSaveFormat[] = await Promise.all(odd.childIds.map(betId => db.retrieve("bets", betId)));
    
    return bets.map((b: BetSaveFormat) => b.typeId);
  }));

  const flattenedBetTypes = betTypes.flat();
  
  return flattenedBetTypes.reduce((acc2, typeId) => ({
    ...acc2,
    [typeId]: (acc2[typeId] ?? 0) + 1
  }), await acc);
}, Promise.resolve({}));

console.log(Object.keys(betTypes).length, betTypes);