import { createFsDB } from "../scripts/db/fsDB.ts";
import { BetSaveFormat, OutcomeSaveFormat, createBetSaveFormat,createOutcomeSaveFormat } from "../scripts/db/saveFormats/BetSaveFormat.ts";
import { Property, createProperty, generateUUIDString, trace } from "../scripts/utils.ts";

const db = await createFsDB({ id: "dev" });

const bets = await db.retrieveAll("properties");

const isDifferent = (outcomes: Property<OutcomeSaveFormat[]>) => 
  (property: Property<OutcomeSaveFormat[]>) => property.value.some(
    (outcome: OutcomeSaveFormat, i: number) => outcome.odd !== outcomes.value[i].odd
  );

const withDifferentOdds = bets
  .map((bet: BetSaveFormat) => (
    bet.properties.reduce((acc: Property<OutcomeSaveFormat[]>[], curr) => {
      const allDifferent = acc.every(isDifferent(curr));
      
      if (acc.length === 0 || allDifferent)
        return acc.concat(curr);

      return acc;
    }, [])
  ))
  .filter((properties: Property<OutcomeSaveFormat[]>[]) => properties.length > 0)
  .map((properties: Property<OutcomeSaveFormat[]>[]) => properties.map(p => p.value));

console.log(withDifferentOdds.length, withDifferentOdds);