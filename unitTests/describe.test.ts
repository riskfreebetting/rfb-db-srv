import { AssertionError } from "https://deno.land/std@0.93.0/testing/asserts.ts";
import describe from "./describe.ts";

console.log("Running test for test-functions: ");

(async () => {
  try {
    testSyncShouldBeSuccessful();
    testSyncShouldFail();

    testAsyncShouldBeSuccessful();
    testAsyncShouldFail();

    testShouldThrow();
    testShouldThrow2();
    testShouldThrow3();
    testShouldThrowAnyError();
    testShouldThrowAssertionError();
    testShouldNotThrow();
  } catch (err) {
    console.error("Test for test functions failed!");
    console.error(err);
    throw err;
  }
})();

async function testSyncShouldBeSuccessful() {
  try {
    await describe("sync test: should be successful", (assert) => {
      assert({
        given: "true",
        should: "true",
        actual: true,
        expected: true,
      });
    });
  } catch (e) {
    throw "Threw when it shouldnt have: " + e.toString();
  }
}

async function testSyncShouldFail() {
  try {
    await describe("sync test: should fail", (assert) => {
      assert({
        given: "false value",
        should: "be true value",
        actual: false,
        expected: true,
      });
    });

    throw "Should have thrown error.";
  } catch (err) {
    if (!(err instanceof AssertionError)) {
      throw "Threw error not of type AssertionError";
    }

    if (
      !err.toString().includes(
        "\nGiven false value: should be true value. \nExpected: true \nBut got: false",
      )
    ) {
      throw `False text in error response: \n Given false value: should be true value.\n Expected: true \n But got: false \n Actual: ${err.toString()}`;
    }
  }
}

async function testAsyncShouldBeSuccessful() {
  try {
    await describe("async test: should be successful", async (assert) => {
      await Promise.resolve();

      assert({
        given: "true value after waiting some time",
        should: "be true value",
        actual: true,
        expected: true,
      });
    });
  } catch (e) {
    throw "Threw when it shouldnt have: " + e.toString();
  }
}

async function testAsyncShouldFail() {
  try {
    await describe("async test: should fail", async (assert) => {
      await Promise.resolve();

      assert({
        given: "false value after waiting some time",
        should: "be true value",
        actual: false,
        expected: true,
      });
    });

    throw "Should have thrown error.";
  } catch (err) {
    if (!(err instanceof AssertionError)) {
      throw "Threw error not of type AssertionError";
    }

    if (
      !err.toString().includes(
        "\nGiven false value after waiting some time: should be true value. \nExpected: true \nBut got: false",
      )
    ) {
      throw `False text in error response: \n Given false value: should be true value.\n Expected: true \n But got: false \n Actual: ${err.toString()}`;
    }
  }
}

class SpecificError extends Error {
  constructor() {
    super();
  }
}

async function testShouldThrow() {
  const asyncThrowSpecificError = () => {
    return Promise.reject(new SpecificError());
  };

  try {
    await describe("async test: should throw", async (assert) => {
      await assert.shouldThrow({
        given: "an async function",
        should: "throw a SpecificError",
        call: asyncThrowSpecificError,
        expectedError: (err) => err instanceof SpecificError,
      });
    });

    throw "Should have thrown";
  } catch (e) {}
}

async function testShouldThrow2() {
  const asyncThrowSpecificError2 = async () => {
    await Promise.resolve();
    throw new SpecificError();
  };

  try {
    await describe("async test: should throw 2", async (assert) => {
      await assert.shouldThrow({
        given: "an async function",
        should: "throw a SpecificError",
        call: asyncThrowSpecificError2,
        expectedError: (err) => err instanceof SpecificError,
      });
    });

    throw "Should have thrown";
  } catch (e) {}
}

async function testShouldThrow3() {
  const o = {
    shouldThrow: true,
    async fnThrowIfThisIsBound() {
      await Promise.resolve();

      if (this.shouldThrow) {
        throw new SpecificError();
      }
    },
  };

  try {
    await describe("async test: should throw 3", async (assert) => {
      await assert.shouldThrow({
        given: "an async function where this has to be bound",
        should: "throw a SpecificError",
        call: o.fnThrowIfThisIsBound,
        thisArg: o,
        expectedError: (err) => err instanceof SpecificError,
      });
    });

    throw "Should have thrown";
  } catch (e) {}
}

async function testShouldThrowAnyError() {
  const asyncThrowSpecificError = async () => {
    await Promise.resolve();
    throw new SpecificError();
  };

  try {
    await describe("async test: should throw any error", async (assert) => {
      await assert.shouldThrow({
        given: "an async function which throws a specific error",
        should: "notice that any error was thrown",
        call: asyncThrowSpecificError,
      });
    });

    throw "Should have thrown";
  } catch (e) {}
}

async function testShouldThrowAssertionError() {
  const asyncThrowAssertionError = async () => {
    await Promise.resolve();
    throw new AssertionError("Test AssertionError");
  };

  try {
    await describe("async test: should throw assertion error", async (assert) => {
      await assert.shouldThrow({
        given: "an async function which throws a assertion error",
        should: "notice that an assertion error was thrown",
        call: asyncThrowAssertionError,
        expectedError: (err: Error) => err instanceof AssertionError,
      });
    });

    throw "Should have thrown";
  } catch (e) {}

  try {
    await describe("async test: should throw assertion error", async (assert) => {
      await assert.shouldThrow({
        given: "an async function which doesnt throw a assertion error",
        should: "notice that an assertion error was thrown",
        call: () => true,
        expectedError: (err: Error) => err instanceof AssertionError,
      });
    });
  } catch (e) {
    console.error(e);

    throw "Should not have thrown!";
  }
}

async function testShouldNotThrow() {
  const o = {
    x: 2,
    async fnThrowIfThisIsNotBound() {
      await Promise.resolve();

      if (this.x !== 2) {
        throw new Error();
      }
    },
  };

  try {
    await describe("async test: should not throw", async (assert) => {
      await assert.shouldNotThrow({
        given: "an async function where this has to be bound",
        should: "not throw any error",
        call: async () => {
          await Promise.resolve();
          throw new Error();
        },
      });
    });

    await describe("async test: should not throw this bound", async (assert) => {
      await assert.shouldNotThrow({
        given: "an async function where this has to be bound",
        should: "not throw any error",
        call: o.fnThrowIfThisIsNotBound,
        thisArg: o,
      });
    });
  } catch (e) {
    throw "Should not have thrown: \n" + e.toString();
  }
}
