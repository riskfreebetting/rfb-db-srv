import {
  createProperty,
  findProperty,
  generateUUIDString,
  getProperty,
  isDateNumber,
  isProperty,
  isUUIDString,
  trace,
} from "../scripts/utils.ts";
import describe from "./describe.ts";

describe("utils: DateNumber", (assert) => {
  assert({
    given: "a date string",
    should: "return true",
    actual: isDateNumber(0),
    expected: true,
  });

  assert({
    given: "an invalid date string",
    should: "return false",
    actual: isDateNumber("XX"),
    expected: false,
  });

  assert({
    given: "null",
    should: "return false",
    actual: isDateNumber(null),
    expected: false,
  });
});

describe("utils: IdString", (assert) => {
  assert({
    given: "a UUID",
    should: "return true",
    actual: isUUIDString("2f806c1c-50ed-4ba2-bcd0-ab91878529aa"),
    expected: true,
  });

  assert({
    given: "a generated UUID string",
    should: "return true",
    actual: isUUIDString(generateUUIDString()),
    expected: true,
  });

  assert({
    given: "not a UUID",
    should: "return false",
    actual: isUUIDString("tessssssssst"),
    expected: false,
  });

  assert({
    given: "null",
    should: "return false",
    actual: isUUIDString(null),
    expected: false,
  });
});

describe("utils: Property: isProperty", (assert) => {
  assert({
    given: "a Property object with a string value",
    should: "return true",
    actual: isProperty<string>({
      name: "testName",
      date: 0,
      source: "testSource",
      value: "test",
    }),
    expected: true,
  });

  assert({
    given: "a Property object with a number value",
    should: "return true",
    actual: isProperty<number>({
      name: "testName",
      date: 0,
      source: "testSource",
      value: 2,
    }),
    expected: true,
  });

  assert({
    given: "a Property object with a null value",
    should: "return true",
    actual: isProperty<null>({
      name: "testName",
      date: 0,
      source: "testSource",
      value: null,
    }),
    expected: true,
  });

  type T = {
    x: boolean;
  };
  assert({
    given: "a Property object with an object",
    should: "return true",
    actual: isProperty<T>({
      name: "testName",
      date: 0,
      source: "testSource",
      value: {
        x: true,
      },
    }),
    expected: true,
  });

  assert({
    given: "an object without name",
    should: "return false",
    actual: isProperty<string>({
      date: 0,
      source: "testSource",
      value: "hi",
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy name",
    should: "return false",
    actual: isProperty<string>({
      name: 2,
      date: 0,
      source: "testSource",
      value: "hi",
    }),
    expected: false,
  });

  assert({
    given: "an object without date",
    should: "return false",
    actual: isProperty<string>({
      name: "name",
      source: "testSource",
      value: "hi",
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy date",
    should: "return false",
    actual: isProperty<string>({
      name: "name",
      source: "testSource",
      date: -33,
      value: "hi",
    }),
    expected: false,
  });

  assert({
    given: "an object with no source",
    should: "return false",
    actual: isProperty<number>({
      name: "testName",
      date: 0,
      value: 2,
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy source",
    should: "return false",
    actual: isProperty<number>({
      name: "testName",
      date: 0,
      source: null,
      value: 2,
    }),
    expected: false,
  });

  assert({
    given: "an object with no value",
    should: "return false",
    actual: isProperty<string>({
      name: "testName",
      date: 0,
      source: "source",
    }),
    expected: false,
  });

  assert({
    given: "an empty object",
    should: "return false",
    actual: isProperty<string>({}),
    expected: false,
  });

  assert({
    given: "null",
    should: "return false",
    actual: isProperty<string>(null),
    expected: false,
  });
});

describe("utils: Property: createProperty", (assert) => {
  const createPropertyOfSource = createProperty(
    { source: "", date: 0 },
  );

  assert({
    given: "a source and a date",
    should: "return another function",
    expected: typeof createPropertyOfSource,
    actual: "function",
  });

  assert({
    given: "a name and a value with string type",
    should: "return a property",
    actual: isProperty(createPropertyOfSource<string>("hi2", "hi back")),
    expected: true,
  });

  assert({
    given: "an almost empty property template with null type",
    should: "set default values and return a property",
    actual: isProperty(createPropertyOfSource("hi2", null)),
    expected: true,
  });
});

describe("utils: Property: GetProperty", (assert) => {
  const createPropertyOfSource = createProperty(
    { source: "", date: 0 },
  );

  const properties = [
    createPropertyOfSource<string>("NOPE", "NOPE"),
    createPropertyOfSource<string>("FIND_ME", "FLAG"),
    createPropertyOfSource<string>("NOPE_AGAIN", "NOPE"),
  ];

  assert({
    given: "several properties and a name to find",
    should: "find property by name and return its value",
    actual: getProperty<String>(properties, "FIND_ME"),
    expected: "FLAG",
  });

  assert({
    given: "several properties and an name that doesnt exist",
    should: "return null",
    actual: getProperty<String>(properties, "NON_EXISTANT_NAME"),
    expected: null,
  });
});

describe("utils: Property: findProperty", assert => {
  const properties = [
    createProperty(
      { source: "MY_SOURCE", date: 0 },
    )("property", "of source"),
    createProperty(
      { source: "", date: 0 },
    )("property", "Oldest"),
    createProperty(
      { source: "", date: 1000 },
    )("property", "Middle"),
    createProperty(
      { source: "", date: 2000 },
    )("property", "Newest2"),
    createProperty(
      { source: "", date: 2000 },
    )("property", "Newest"),
    createProperty(
      { source: "", date: 2000 },
    )("other Property", "Different name"),
  ];

  assert({
    given: "several properties from different dates/sources and an empty PropertyFilter",
    should: "return the newest property",
    actual: findProperty("property")({})(properties)?.value,
    expected: "Newest"
  });

  assert({
    given: "several properties from different dates/sources and an empty PropertyFilter and a different name",
    should: "return a property with a different value",
    actual: findProperty("other Property")({})(properties)?.value,
    expected: "Different name"
  });

  assert({
    given: "several properties from different dates/sources and an empty PropertyFilter and a non-existant name",
    should: "return null",
    actual: findProperty("NON_EXISTANT")({})(properties),
    expected: null
  });

  assert({
    given: "several properties from different dates/sources and a property filter with before date",
    should: "return a property older than the newest",
    actual: findProperty("property")({ 
      beforeDate: 2000 
    })(properties)?.value,
    expected: "Middle"
  });

  assert({
    given: "several properties from different dates/sources and a property filter with source",
    should: "return a property of different source",
    actual: findProperty("property")({ 
      source: "MY_SOURCE" 
    })(properties)?.value,
    expected: "of source"
  });

  assert({
    given: "several properties from different dates/sources and a property filter with a after date in the future",
    should: "return null",
    actual: findProperty("property")({ 
      afterDate: 1000000
    })(properties),
    expected: null
  });

  assert({
    given: "several properties from different dates/sources and a property filter with before- and afterdate",
    should: "return null",
    actual: (findProperty("property")({ 
      beforeDate: 2000, 
      afterDate: 0 
    })(properties))?.value,
    expected: "Middle"
  });

  assert({
    given: "several properties from different dates/sources and a property filter with a before date and a source",
    should: "return oldest",
    actual: findProperty("property")({ 
      beforeDate: 1000, 
      source: "" 
    })(properties)?.value,
    expected: "Oldest"
  });
});