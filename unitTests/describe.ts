import {
  assertEquals,
  AssertionError,
} from "https://deno.land/std@0.93.0/testing/asserts.ts";

const noop = () => {};
const requiredKeys = ["given", "should", "actual", "expected"];
const concatToString = (keys: string, key: string, index: number) =>
  keys + (index ? ", " : "") + key;

const describe = (
  unitName: string = "",
  fnTest: (
    assert: Assert,
  ) => (void | Promise<void>) = noop,
): Promise<any> => {
  const assert = (args: AssertArgs) => {
    const missing = requiredKeys.filter(
      (k) => !Object.keys(args).includes(k),
    );

    if (missing.length > 0) {
      throw new Error(
        `The following parameters are required by \`assert\`: ${
          missing.reduce(concatToString, "")
        }`,
      );
    }

    const {
      // initialize values to undefined so TypeScript doesn't complain
      given = undefined,
      should = "",
      actual = undefined,
      expected = undefined,
    } = args;

    assertEquals(
      actual,
      expected,
      `\nGiven ${given}: should ${should}. \nExpected: ${expected} \nBut got: ${actual}`,
    );
  };

  assert.shouldThrow = async ({
    given,
    should,
    call,
    thisArg,
    expectedError,
  }: ShouldThrowArgs): Promise<void> => {
    try {
      if (thisArg) {
        await call.bind(thisArg)();
      } else {
        await call();
      }

      assert({
        given,
        should,
        actual: "Did not throw when it should have!",
        expected: null,
      });
    } catch (err) {
      if (expectedError) {
        const actual = expectedError ? expectedError(err) : true;
        if (actual) {
          assert({
            given,
            should,
            actual: true,
            expected: true,
          });
        } else if (err instanceof AssertionError) {
          throw err;
        } else {
          assert({
            given,
            should,
            actual: false,
            expected: true,
          });
        }
      } else {
        if (err instanceof AssertionError) {
          throw err;
        } else {
          assert({
            given,
            should,
            actual: true,
            expected: true,
          });
        }
      }
    }
  };

  assert.shouldNotThrow = async ({
    given,
    should,
    call,
    thisArg,
  }: ShouldNotThrowArgs) => {
    try {
      if (thisArg) {
        await call.bind(thisArg)();
      }
    } catch (error) {
      assert({
        given,
        should,
        actual: error,
        expected: "No error",
      });
    }
  };

  return new Promise((resolve, reject) => {
    Deno.test(unitName, async () => {
      try {
        resolve(await fnTest(assert));
      } catch (err) {
        reject(err);
      }
    });
  });
};

export default describe;

export interface Assert {
  (args: AssertArgs): void;
  shouldThrow: (args: ShouldThrowArgs) => void;
  shouldNotThrow: (args: ShouldNotThrowArgs) => void;
}

interface AssertArgs {
  given: string;
  should: string;
  actual: any;
  expected: any;
}

interface ShouldThrowArgs {
  given: string;
  should: string;
  call: Function;
  thisArg?: any;
  expectedError?: (err: Error) => boolean;
}

interface ShouldNotThrowArgs {
  given: string;
  should: string;
  call: Function;
  thisArg?: any;
}
