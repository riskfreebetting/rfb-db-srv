import { AssertionError } from "https://deno.land/std@0.93.0/testing/asserts.ts";
import { createDB, DB } from "../scripts/db/db.ts";
import {
  createReturnObject,
  ReturnObject,
} from "../scripts/factories/ReturnObject.ts";
import { validateTypes } from "../scripts/inputs/Validators.ts";

import describe from "./describe.ts";
import { testIfIsIsFunctionForObject, testValidator } from "./testUtils.ts";

describe("testUtils: testIfIsIsFunctionForObject", async assert => {
  interface I {
    notNull: string;
    notNullToo: string;
    maybeNull: number | null;
    maybeUndefined?: number;
  }

  await assert.shouldNotThrow({
    given: "an isFunction which ",
    should: "not throw",
    call: () =>
      testIfIsIsFunctionForObject(
        (o: any): o is I => (
          typeof o?.notNull === "string" &&
          typeof o?.notNullToo === "string" &&
          (typeof o?.maybeNull === "number" || o?.maybeNull === null) &&
          (typeof o?.maybeUndefined === "number" ||
            o?.maybeUndefined === undefined)
        ),
        {
          notNull: "test",
          noNullToo: "test2",
          maybeNull: 2,
          maybeUndefined: 3,
        },
        [
          "notNull",
          "noNullToo",
        ],
        assert,
      ),
  });

  await assert.shouldThrow({
    given: "an isFunction which always returns true",
    should: "throw",
    call: () =>
      testIfIsIsFunctionForObject(
        (o: any): o is I => true,
        {
          notNull: "test",
          noNullToo: "test2",
          maybeNull: 2,
          maybeUndefined: 3,
        },
        [
          "notNull",
          "noNullToo",
        ],
        assert,
      ),
    expectedError: (err: Error) => err instanceof AssertionError,
  });

  await assert.shouldNotThrow({
    given:
      "an isFunction which returns true if null values are not allowed as null",
    should: "not throw!!",
    call: () =>
      testIfIsIsFunctionForObject(
        (o: any): o is I => (
          typeof o?.notNull === "string" &&
          typeof o?.notNullToo === "string" &&
          typeof o?.maybeNull === "string" &&
          typeof o?.maybeUndefined === "string"
        ),
        {
          notNull: "test",
          noNullToo: "test2",
          maybeNull: 2,
          maybeUndefined: 3,
        },
        [
          "notNull",
          "noNullToo",
        ],
        assert,
      ),
  });

  await assert.shouldThrow({
    given: "an isFunction which falsly allows null for one property",
    should: "throw",
    call: () =>
      testIfIsIsFunctionForObject(
        (o: any): o is I => (
          (typeof o?.notNull === "string" || o?.notNull === null) &&
          typeof o?.notNullToo === "string" &&
          typeof o?.maybeNull === "string" &&
          typeof o?.maybeUndefined === "string"
        ),
        {
          notNull: "test",
          noNullToo: "test2",
          maybeNull: 2,
          maybeUndefined: 3,
        },
        [
          "notNull",
          "noNullToo",
        ],
        assert,
      ),
    expectedError: (err: Error) => err instanceof AssertionError,
  });

  await assert.shouldThrow({
    given: "an isFunction which returns false for the inputted template",
    should: "throw",
    call: () =>
      testIfIsIsFunctionForObject(
        (o: any): o is I => (
          typeof o?.notNull === "number" &&
          typeof o?.notNullToo === "string" &&
          typeof o?.maybeNull === "string" &&
          typeof o?.maybeUndefined === "string"
        ),
        {
          notNull: "test",
          noNullToo: "test2",
          maybeNull: 2,
          maybeUndefined: 3,
        },
        [
          "notNull",
          "noNullToo",
        ],
        assert,
      ),
    expectedError: (err: Error) => err instanceof AssertionError,
  });
});

describe("testUtils: testValidator", async (assert) => {
  const db = await createDB({ id: "testDB" });
  interface I {
    name: string;
    inputValue: number[];
  }
  const filled = { name: "myName", inputValue: [0, 1, 2] };
  const notNullOrUndefined = ["name", "inputValue"] as (keyof I)[];

  await testValidator(
    db,
    async (db: DB, inputs: any): Promise<ReturnObject<I>> =>
      createReturnObject({
        result: inputs as I,
        messages: validateTypes("I", inputs, {
          name: {
            type: "string",
            fn: (x: any) => typeof x === "string",
          },
          inputValue: {
            type: "number[]",
            fn: (x: any) =>
              Array.isArray(x) && x.every((e: any) => typeof e === "number"),
          },
        }),
      }),
    filled,
    notNullOrUndefined,
    assert,
  );

  await assert.shouldThrow({
    given: "a falsy validator (no validator for name)",
    should: "throw",
    call: () =>
      testValidator(
        db,
        async (db: DB, inputs: any): Promise<ReturnObject<I>> =>
          createReturnObject({
            result: inputs as I,
            messages: validateTypes("I", inputs, {
              inputValue: {
                type: "number[]",
                fn: (x: any) =>
                  Array.isArray(x) &&
                  x.every((e: any) => typeof e === "number"),
              },
            }),
          }),
        filled,
        notNullOrUndefined,
        assert,
      ),
    expectedError: (err: Error) => err instanceof AssertionError,
  });

  await assert.shouldThrow({
    given: "a falsy validator (falsy validator for inputValue)",
    should: "throw",
    call: () =>
      testValidator(
        db,
        async (db: DB, inputs: any): Promise<ReturnObject<I>> =>
          createReturnObject({
            result: inputs as I,
            messages: validateTypes("I", inputs, {
              name: {
                type: "string",
                fn: (x: any) => typeof x === "string",
              },
              inputValue: {
                type: "number[]",
                fn: (x: any) => true,
              },
            }),
          }),
        filled,
        notNullOrUndefined,
        assert,
      ),
    expectedError: (err: Error) => err instanceof AssertionError,
  });
});
