import { DB } from "../scripts/db/db.ts";
import { ReturnObject } from "../scripts/factories/ReturnObject.ts";
import { trace } from "../scripts/utils.ts";
import { Assert } from "./describe.ts";

export const testIfIsIsFunctionForObject = <T>(
  functionToBeTested: (o: any) => boolean,
  filled: T,
  notNullOrUndefinedProperties: (keyof T)[] = [],
  assert: Assert,
) => {
  assert({
    given: "An filled object",
    should: "Return true",
    actual: functionToBeTested(filled),
    expected: true,
  });

  assert({
    given: "null",
    should: "Return false",
    actual: functionToBeTested(null),
    expected: false,
  });

  assert({
    given: "An empty object",
    should: "Return false",
    actual: functionToBeTested({}),
    expected: false,
  });

  notNullOrUndefinedProperties.forEach((key) => {
    const withoutKey: T = { ...filled };
    delete withoutKey[key];

    assert({
      given: `An object without ${key}`,
      should: "Return false",
      actual: functionToBeTested(withoutKey),
      expected: false,
    });

    assert({
      given: `An object with null ${key}`,
      should: "Return false",
      actual: functionToBeTested({
        ...filled,
        [key]: null,
      }),
      expected: false,
    });
  });
};

export const testValidator = async <T>(
  db: DB,
  validatorToBeTested: (db: DB, o: any) => Promise<ReturnObject<T>>,
  filled: T,
  notNullOrUndefinedProperties: (keyof T)[] = [],
  assert: Assert,
) => {
  assert({
    given: "An filled object",
    should: "Return an successful validation",
    actual: (await validatorToBeTested(db, filled)).successful,
    expected: true,
  });

  assert({
    given: "null",
    should: "Return an unsuccessful validation",
    actual: (await validatorToBeTested(db, null)).successful,
    expected: false,
  });

  assert({
    given: "An empty object",
    should: "Return an unsuccessful validation",
    actual: (await validatorToBeTested(db, {})).successful,
    expected: false,
  });

  await Promise.all(notNullOrUndefinedProperties.map(async (key) => {
    const withoutKey: T = { ...filled };
    delete withoutKey[key];

    assert({
      given: `An object without ${key}`,
      should: "Return an unsuccessful validation",
      actual: (await validatorToBeTested(db, withoutKey)).successful,
      expected: false,
    });

    assert({
      given: `An object with null ${key}`,
      should: "Return an unsuccessful validation",
      actual: (await validatorToBeTested(db, {
        ...filled,
        [key]: null,
      })).successful,
      expected: false,
    });
  }));
};
