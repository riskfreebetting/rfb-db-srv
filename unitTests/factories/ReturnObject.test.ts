import {
  createErrorWithInfo,
  createMessageWithInfo,
  createNotFoundError,
  createReturnObject,
  EntryNotFoundError,
} from "../../scripts/factories/ReturnObject.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";

describe("ReturnObject: createErrorWithInfo", (assert) => {
  const e1 = createErrorWithInfo({
    ErrorClass: Error,
    code: "E001",
    msg: "Error: Test error",
  });
  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e1.error.message,
    expected: "E001: Error: Test error",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e1.msg,
    expected: "E001: Error: Test error",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e1.replaceableMsg,
    expected: "E001: Error: Test error",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e1.params,
    expected: [],
  });

  const e2 = createErrorWithInfo({
    code: "E100",
    msg: "Error: {{Test}} error called by {{me}}",
  });
  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e2.error.message,
    expected: "E100: Error: Test error called by me",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e2.msg,
    expected: "E100: Error: Test error called by me",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e2.replaceableMsg,
    expected: "E100: Error: {{0}} error called by {{1}}",
  });

  assert({
    given: "an error and a message without params",
    should: "return a error with infos without any params",
    actual: e2.params,
    expected: ["Test", "me"],
  });
});

describe("ReturnObject: createNotFoundError", (assert) => {
  const e = createNotFoundError({ id: "testId", dbId: "db", type: "matches" });
  assert({
    given: "a dbId, id and type",
    should: "create an EntryNotFoundError",
    actual: e.error instanceof EntryNotFoundError,
    expected: true,
  });

  assert({
    given: "a dbId, id and type",
    should: "create an message",
    actual: e.msg,
    expected:
      "E001: EntryNotFound: Could not find entry of type 'matches' with id 'testId' in database 'db'.",
  });

  assert({
    given: "a dbId, id and type",
    should: "create an params array",
    actual: e.params,
    expected: ["matches", "testId", "db"],
  });
});

describe("ReturnObject: createReturnObject", (assert) => {
  assert({
    given: "empty messages and result",
    should: "return a successful return object",
    actual: createReturnObject({ result: "res", messages: [] }),
    expected: {
      result: "res",
      messages: [],
      successful: true,
    },
  });

  assert({
    given: "error messages and result",
    should: "return a successful return object",
    actual: createReturnObject({
      result: "res",
      messages: [
        createNotFoundError(
          { dbId: "db", type: "testType", id: generateUUIDString() },
        ),
      ],
    }).successful,
    expected: false,
  });

  assert({
    given: "error messages and result",
    should: "return a successful return object",
    actual: createReturnObject({
      result: "res",
      messages: [createMessageWithInfo("I001", "This is a info")],
    }).successful,
    expected: true,
  });
});
