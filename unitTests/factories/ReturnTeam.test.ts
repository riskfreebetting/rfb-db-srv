import { createDB } from "../../scripts/db/db.ts";
import { ReturnObject } from "../../scripts/factories/ReturnObject.ts";
import {
  createReturnTeam,
  createReturnTeamById,
  createReturnTeamFromSaveFormat,
  isReturnTeam,
  ReturnTeam,
} from "../../scripts/factories/ReturnTeam.ts";
import { generateUUIDString, trace } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

describe("ReturnTeam: isReturnTeam", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnTeam,
    {
      id: generateUUIDString(),
      name: "MY_TEAM",
      alternativeNames: {
        [generateUUIDString()]: ["myTeam"]
      }
    },
    [
      "id",
      "name",
      "alternativeNames"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnTeam({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: []
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isReturnTeam({
      id: generateUUIDString(),
      name: 0,
    }),
    expected: false,
  });
  
  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isReturnTeam({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non-string alternative name",
    should: "return false",
    actual: isReturnTeam({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["2", 42],
    }),
    expected: false,
  });
});

describe("ReturnTeam: createReturnTeam", (assert) => {
  assert({
    given: "some Team properties",
    should: "return a Team",
    actual: isReturnTeam(createReturnTeam({
      id: generateUUIDString(),
      name: "name :)",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      }
    })),
    expected: true,
  });
});

describe("ReturnTeam: createReturnTeamFromSaveFormat", (assert) => {
  assert({
    given: "a Team in save format",
    should: "return a Team",
    actual: isReturnTeam(createReturnTeamFromSaveFormat({
      id: generateUUIDString(),
      properties: [],
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      }
    })),
    expected: true,
  });
});

describe("ReturnTeam: createReturnTeamById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const teamId = generateUUIDString();
  await db.insert("teams", {
    id: teamId,
    properties: [],
    name: "testName",
    alternativeNames: {
      [generateUUIDString()]: ["testName"]
    }
  });

  assert({
    given: "a db and an id",
    should: "return a team",
    actual: isReturnTeam((await createReturnTeamById(db)(teamId)).result),
    expected: true,
  });

  const res = await createReturnTeamById(db)(generateUUIDString());
  assert({
    given: "a match in save format with a non existant id",
    should: "not return a team",
    actual: res.result,
    expected: null,
  });

  const err = res.messages && res.messages[0];
  assert({
    given: "a match in save format with a non existant id",
    should: "return a team not found error",
    actual: err?.code === "E001" && err?.params[0] === "team",
    expected: true,
  });
});
