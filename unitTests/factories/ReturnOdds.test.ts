import { createDB, DB } from "../../scripts/db/db.ts";
import {
  BetSaveFormat,
  createOutcomeSaveFormat,
  OutcomeSaveFormat,
} from "../../scripts/db/saveFormats/BetSaveFormat.ts";
import { TaxTypes } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { BetTypeSaveFormat } from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { createReturnOutcomeName } from "../../scripts/factories/ReturnBetType.ts";
import {
  createReturnOdds,
  createReturnOddsById,
  createReturnOddsFromSaveFormat,
  isReturnOdds,
} from "../../scripts/factories/ReturnOdds.ts";
import {
  createProperty,
  generateUUIDString,
  UUIDString,
} from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

const date = 0;

const bookmakerId = generateUUIDString();
const filledOdds = {
  id: generateUUIDString(),
  name: "BVB-FCB on Bwin",
  bookmaker: {
    id: bookmakerId,
    name: "Bwin",
    tax: "NONE" as keyof typeof TaxTypes,
  },
  bets: [
    {
      id: generateUUIDString(),
      date: 0,
      type: {
        id: generateUUIDString(),
        name: "Winner",
        outcomeNames: [
          createReturnOutcomeName({
            name: "1",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "X",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "2",
            id: generateUUIDString(),
          }),
        ],
      },
      outcomes: [
        {
          name: "1",
          odd: 1.8,
          date,
        },
        {
          name: "X",
          odd: 3.8,
          date,
        },
        {
          name: "2",
          odd: 2.1,
          date,
        },
      ],
    },
    {
      id: generateUUIDString(),
      date: 0,
      type: {
        id: generateUUIDString(),
        name: "Total Goals Over",
        outcomeNames: [
          createReturnOutcomeName({
            name: "0.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "1.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "2.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "3.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "4.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "5.5",
            id: generateUUIDString(),
          }),
          createReturnOutcomeName({
            name: "6.5",
            id: generateUUIDString(),
          }),
        ],
      },
      outcomes: [
        {
          name: "0.5",
          odd: 1.05,
          date,
        },
        {
          name: "1.5",
          odd: 1.35,
          date,
        },
        {
          name: "2.5",
          odd: 1.82,
          date,
        },
        {
          name: "3.5",
          odd: 2.42,
          date,
        },
        {
          name: "4.5",
          odd: 3.1,
          date,
        },
        {
          name: "5.5",
          odd: 4.9,
          date,
        },
        {
          name: "6.5",
          odd: 8,
          date,
        },
      ],
    },
  ],
};

const emptyOdds = {
  id: generateUUIDString(),
  name: "BVB-FCB on Bwin",
  bookmaker: {
    id: bookmakerId,
    tax: "NONE",
    name: "Bwin",
  },
  bets: [],
};

describe("ReturnOdds: isReturnOdds", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnOdds,
    filledOdds,
    [
      "id",
      "name",
      "bookmaker",
      "bets",
    ],
    assert,
  );

  assert({
    given: "An empty odds object",
    should: "Return true",
    actual: isReturnOdds(emptyOdds),
    expected: true,
  });

  assert({
    given: "An object with falsy id",
    should: "Return false",
    actual: isReturnOdds({
      id: "test FALSY ID",
      name: "testName",
      bookmaker: {
        tax: "NONE",
        name: "Bwin",
      },
      bets: [],
    }),
    expected: false,
  });

  assert({
    given: "An object with falsy bookmaker",
    should: "Return false",
    actual: isReturnOdds({
      id: generateUUIDString(),
      name: "testName",
      bookmaker: {
        falsy: true,
      },
      bets: [],
    }),
    expected: false,
  });

  assert({
    given: "An object with some falsy bets (no id)",
    should: "Return false",
    actual: isReturnOdds({
      id: generateUUIDString(),
      name: "testName",
      bookmaker: {
        tax: "NONE",
        name: "test",
      },
      bets: [
        {
          id: generateUUIDString(),
          type: {
            id: generateUUIDString(),
            name: "Winner",
            outcomeNames: [
              createReturnOutcomeName({
                name: "1",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "X",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "2",
                id: generateUUIDString(),
              }),
            ],
          },
          date:
            "Thu Dec 10 2020 19:08:20 GMT+0100 (Mitteleuropäische Normalzeit)",
          outcomes: [
            {
              name: "1",
              odd: 1.8,
              date,
            },
            {
              name: "X",
              odd: 3.8,
              date,
            },
            {
              name: "2",
              odd: 2.1,
              date,
            },
          ],
        },
        {
          noid: generateUUIDString(),
          type: {
            id: generateUUIDString(),
            name: "Winner",
          },
          date:
            "Thu Dec 10 2020 19:08:20 GMT+0100 (Mitteleuropäische Normalzeit)",
          outcomes: [
            {
              name: "1",
              odd: 1.8,
              date,
            },
            {
              name: "X",
              odd: 3.8,
              date,
            },
            {
              name: "2",
              odd: 2.1,
              date,
            },
          ],
        },
      ],
    }),
    expected: false,
  });

  assert({
    given: "An object with some falsy bets (falsy outcomes)",
    should: "Return false",
    actual: isReturnOdds({
      id: generateUUIDString(),
      name: "testName",
      bookmaker: {
        tax: "NONE",
        name: "test",
      },
      bets: [
        {
          id: generateUUIDString(),
          type: {
            id: generateUUIDString(),
            name: "Winner",
            outcomeNames: [
              createReturnOutcomeName({
                name: "1",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "X",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "2",
                id: generateUUIDString(),
              }),
            ],
          },
          date:
            "Thu Dec 10 2020 19:08:20 GMT+0100 (Mitteleuropäische Normalzeit)",
          outcomes: [
            {
              name: "1",
              odd: 1.8,
              date,
            },
            {
              name: "X",
              noodd: 3.8,
              date,
            },
            {
              name: "2",
              odd: 2.1,
              date,
            },
          ],
        },
        {
          noid: generateUUIDString(),
          type: {
            id: generateUUIDString(),
            name: "Winner",
            outcomeNames: [
              createReturnOutcomeName({
                name: "1",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "X",
                id: generateUUIDString(),
              }),
              createReturnOutcomeName({
                name: "2",
                id: generateUUIDString(),
              }),
            ],
          },
          date:
            "Thu Dec 10 2020 19:08:20 GMT+0100 (Mitteleuropäische Normalzeit)",
          outcomes: [
            {
              name: "1",
              odd: 1.8,
              date,
            },
            {
              name: "X",
              odd: 3.8,
              date,
            },
            {
              name: "2",
              odd: 2.1,
              date,
            },
          ],
        },
      ],
    }),
    expected: false,
  });
});

describe("ReturnOdds: createReturnOdds", async (assert) => {
  assert({
    given: "properties for Odds",
    should: "return a Odds object",
    actual: isReturnOdds(await createReturnOdds(filledOdds)),
    expected: true,
  });
});

describe("ReturnOdds: createReturnOddsFromSaveFormat", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await db.insert("bookmakers", {
    id: bookmakerId,
    properties: [],
    tax: "NONE",
    name: "Bwin",
  });

  const typeId = generateUUIDString();
  db.insert("betTypes", {
    id: typeId,
    name: "TEST",
    properties: [],
    outcomeNames: [],
  } as BetTypeSaveFormat);
  const betIds = createMockBets(db, typeId, 2);

  const odds = (await createReturnOddsFromSaveFormat(db)({})({
    id: generateUUIDString(),
    name: "FCB-BVB on Bwin",
    properties: [],
    bookmakerId,
    parentId: generateUUIDString(),
    childIds: betIds,
  })).result;

  assert({
    given: "an odds object in save format",
    should: "return an return Odds object",
    actual: isReturnOdds(odds),
    expected: true,
  });

  assert({
    given: "an odds object in save format",
    should: "return an return Odds object with same bets length as inputted",
    actual: odds?.bets.length,
    expected: betIds.length,
  });

  const falsyBookmakerId = generateUUIDString();
  const falsyOdds1 = await createReturnOddsFromSaveFormat(db)({})({
    id: generateUUIDString(),
    name: "FCB-BVB on Bwin",
    properties: [],
    bookmakerId: falsyBookmakerId,
    parentId: generateUUIDString(),
    childIds: betIds,
  });

  assert({
    given: "an odds object in save format with falsy bookmakerId",
    should: "return no result",
    actual: falsyOdds1.result,
    expected: null,
  });

  const bookmakerErr = falsyOdds1.messages && falsyOdds1.messages[0];
  assert({
    given: "an odds object in save format with falsy bookmakerId",
    should: "an bookmaker not found error",
    actual: bookmakerErr?.code === "E001" &&
      bookmakerErr.params[0] === "bookmaker",
    expected: true,
  });

  const falsyBetIds = [betIds[0], generateUUIDString()];
  const falsyOdds2 = await createReturnOddsFromSaveFormat(db)({})({
    id: generateUUIDString(),
    name: "FCB-BVB on Bwin",
    properties: [],
    bookmakerId,
    parentId: generateUUIDString(),
    childIds: falsyBetIds,
  });
  assert({
    given: "an odds object in save format with one falsy bet id",
    should: "still return an returnOdds object",
    actual: isReturnOdds(falsyOdds2.result),
    expected: true,
  });

  const falsyOddsErr = falsyOdds2.messages && falsyOdds2.messages[0];
  assert({
    given: "an odds object in save format with falsy bet id",
    should: "an bet not found error",
    actual: falsyOddsErr?.code === "E001" && falsyOddsErr.params[0] === "bet",
    expected: true,
  });
});

describe("ReturnMatchTeam: createReturnOddsById", async (assert) => {
  const filledDB = await createDB({ id: "filled" });

  await filledDB.insert("bookmakers", {
    id: bookmakerId,
    properties: [],
    tax: "NONE",
    name: "Bwin",
  });

  const oddsId = generateUUIDString();
  await filledDB.insert("odds", {
    id: oddsId,
    name: "testName",
    bookmakerId,
    properties: [],
    childIds: [],
    parentId: generateUUIDString(),
  });

  const retrievedOdds = (await createReturnOddsById(filledDB)({})(oddsId)).result;

  assert({
    given:
      "a database with one bookmaker, an odds object and the id of that odds object",
    should: "return a ReturnOdds",
    actual: isReturnOdds(retrievedOdds),
    expected: true,
  });

  assert({
    given:
      "a database with one bookmaker, an odds object and the id of that odds object",
    should: "return a ReturnOdds with the id of the odds in the DB",
    actual: retrievedOdds?.id,
    expected: oddsId,
  });
});

function createMockBets(db: DB, typeId: UUIDString, i: number): UUIDString[] {
  return Array.from({ length: i })
    .map(() => {
      const id = generateUUIDString();

      db.insert("bets", {
        id,
        typeId,
        properties: [
          createProperty({
            source: "A",
            date: 0,
          })<OutcomeSaveFormat[]>(
            "outcomes",
            [
              createOutcomeSaveFormat("1", Math.random() + 1),
              createOutcomeSaveFormat("X", Math.random() + 1),
              createOutcomeSaveFormat("2", Math.random() + 1),
            ],
          ),
        ],
      } as BetSaveFormat);

      return id;
    });
}
