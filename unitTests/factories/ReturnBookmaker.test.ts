import { createDB } from "../../scripts/db/db.ts";
import {
  createReturnBookmaker,
  createReturnBookmakerById,
  createReturnBookmakerFromSaveFormat,
  isReturnBookmaker,
} from "../../scripts/factories/ReturnBookmaker.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";
import { TaxTypes } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";

const bookmaker = {
  id: generateUUIDString(),
  name: "test",
  tax: "NONE" as keyof typeof TaxTypes,
};

describe("ReturnBookmaker: isReturnBookmaker", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnBookmaker,
    bookmaker,
    [
      "id",
      "name",
      "tax"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnBookmaker({
      id: "FALSY_ID",
      name: "test",
      tax: "NONE",
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isReturnBookmaker({
      id: generateUUIDString(),
      name: 0,
      tax: "NONE",
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy tax type",
    should: "return false",
    actual: isReturnBookmaker({
      id: generateUUIDString(),
      name: "Name",
      tax: 0.05,
    }),
    expected: false,
  });
});

describe("ReturnBookmaker: createReturnBookmaker", (assert) => {
  assert({
    given: "some bookmaker properties",
    should: "return a bookmaker",
    actual: isReturnBookmaker(createReturnBookmaker(bookmaker)),
    expected: true,
  });
});

describe("ReturnBookmaker: createReturnBookmakerFromSaveFormat", (assert) => {
  assert({
    given: "a bookmaker in save format",
    should: "return a bookmaker",
    actual: isReturnBookmaker(createReturnBookmakerFromSaveFormat({
      id: generateUUIDString(),
      properties: [],
      tax: "NONE",
      name: "testName",
    })),
    expected: true,
  });
});

describe("ReturnBookmaker: createReturnBookmakerById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bookmakerId = generateUUIDString();
  await db.insert("bookmakers", {
    id: bookmakerId,
    properties: [],
    tax: "NONE",
    name: "testName",
  });

  assert({
    given: "a db and an id",
    should: "return a bookmaker",
    actual: isReturnBookmaker(
      (await createReturnBookmakerById(db)(bookmakerId)).result,
    ),
    expected: true,
  });
});
