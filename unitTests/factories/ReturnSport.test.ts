import { createDB } from "../../scripts/db/db.ts";
import { ReturnObject } from "../../scripts/factories/ReturnObject.ts";
import {
  createReturnSport,
  createReturnSportById,
  createReturnSportFromSaveFormat,
  isReturnSport,
  ReturnSport,
} from "../../scripts/factories/ReturnSport.ts";
import { generateUUIDString, trace } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

describe("ReturnSport: isReturnSport", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnSport,
    {
      id: generateUUIDString(),
      name: "MY_SPORT",
      alternativeNames: {
        [generateUUIDString()]: ["mySport"]
      }
    },
    [
      "id",
      "name",
      "alternativeNames"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnSport({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: []
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isReturnSport({
      id: generateUUIDString(),
      name: 0,
    }),
    expected: false,
  });
  
  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isReturnSport({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non-string alternative name",
    should: "return false",
    actual: isReturnSport({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["2", 42],
    }),
    expected: false,
  });
});

describe("ReturnSport: createReturnSport", (assert) => {
  assert({
    given: "some Sport properties",
    should: "return a Sport",
    actual: isReturnSport(createReturnSport({
      id: generateUUIDString(),
      name: "name :)",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      }
    })),
    expected: true,
  });
});

describe("ReturnSport: createReturnSportFromSaveFormat", (assert) => {
  assert({
    given: "a Sport in save format",
    should: "return a Sport",
    actual: isReturnSport(createReturnSportFromSaveFormat({
      id: generateUUIDString(),
      properties: [],
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      }
    })),
    expected: true,
  });
});

describe("ReturnSport: createReturnSportById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const sportId = generateUUIDString();
  await db.insert("sports", {
    id: sportId,
    properties: [],
    name: "testName",
    alternativeNames: {
      [generateUUIDString()]: ["testName"]
    }
  });

  assert({
    given: "a db and an id",
    should: "return a sport",
    actual: isReturnSport((await createReturnSportById(db)(sportId)).result),
    expected: true,
  });

  const res = await createReturnSportById(db)(generateUUIDString());
  assert({
    given: "a match in save format with a non existant id",
    should: "not return a sport",
    actual: res.result,
    expected: null,
  });

  const err = res.messages && res.messages[0];
  assert({
    given: "a match in save format with a non existant id",
    should: "return a sport not found error",
    actual: err?.code === "E001" && err?.params[0] === "sport",
    expected: true,
  });
});
