import { createDB, DB, DBDeletedError } from "../../scripts/db/db.ts";
import {
  BetSaveFormat,
  createOutcomeSaveFormat,
  OutcomeSaveFormat,
} from "../../scripts/db/saveFormats/BetSaveFormat.ts";
import { BetTypeSaveFormat } from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { retrieveAllCompetitions } from "../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { MatchSaveFormat } from "../../scripts/db/saveFormats/MatchSaveFormat.ts";
import { MatchTeamSaveFormat } from "../../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import { OddsSaveFormat } from "../../scripts/db/saveFormats/OddsSaveFormat.ts";
import { createReturnOutcomeName } from "../../scripts/factories/ReturnBetType.ts";
import { ReturnCompetition } from "../../scripts/factories/ReturnCompetition.ts";
import {
  MatchStatus,
  ReturnMatch,
} from "../../scripts/factories/ReturnMatch.ts";
import {
  createReturnMatch,
  createReturnMatchFromSaveFormat,
  isReturnMatch,
} from "../../scripts/factories/ReturnMatch.ts";
import { ReturnMatchTeam } from "../../scripts/factories/ReturnMatchTeam.ts";
import { ReturnTeam } from "../../scripts/factories/ReturnTeam.ts";
import {
  createProperty,
  generateUUIDString,
  UUIDString,
} from "../../scripts/utils.ts";
import { createFilledMockDB } from "../db/mockFactories.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";
import { TaxTypes } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { TeamSaveFormat } from "../../scripts/db/saveFormats/TeamSaveFormat.ts";

const date = 0;
const matchId = generateUUIDString();

const sampleCompetition: ReturnCompetition = {
  id: generateUUIDString(),
  name: "Bundesliga",
  alternativeNames: {
    [generateUUIDString()]: ["Bundesliga"]
  },
  sport: {
    id: generateUUIDString(),
    name: "Fußball",
    alternativeNames: {
      [generateUUIDString()]: ["Fußball"]
    },
  },
  country: {
    id: generateUUIDString(),
    name: "Deutschland",
    alternativeNames: {
      [generateUUIDString()]: ["Deutschland"]
    },
  }
};

const outcomeNames = [
  createReturnOutcomeName({
    name: "1",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "X",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "2",
    id: generateUUIDString(),
  }),
];

const teamIds = [generateUUIDString(), generateUUIDString()];
const returnTeams: ReturnTeam[] = teamIds.map(id => ({
  id,
  name: "X",
  alternativeNames: {}
}));
const matchTeamIds = [generateUUIDString(), generateUUIDString()];
const sampleMatchTeams: ReturnMatchTeam[] = matchTeamIds.map((id, i) => ({
  id,
  name: "FC Bayern",
  matchId,
  team: returnTeams[i],
}));

const sampleOdds = [
  {
    id: generateUUIDString(),
    name: "FCB-BVB on Bwin",
    bookmaker: {
      id: generateUUIDString(),
      name: "Bwin",
      tax: "NONE" as keyof typeof TaxTypes,
    },
    bets: [
      {
        id: generateUUIDString(),
        date: 0,
        type: {
          id: generateUUIDString(),
          outcomeNames,
          name: "Winner",
        },
        outcomes: [
          {
            name: "1",
            odd: 1.8,
            date,
          },
          {
            name: "X",
            odd: 3.8,
            date,
          },
          {
            name: "2",
            odd: 2.1,
            date,
          },
        ],
      },
    ],
  },
];

const filledMatchTemplate: ReturnMatch = {
  id: matchId,
  name: "FC Bayern - Borussia Dortmund",
  result: [3, 2],
  status: "FT" as keyof typeof MatchStatus,
  currentMinute: 90,
  startDate: 1607630900000,
  endDate: 1607637620000,
  odds: sampleOdds,
  teams: sampleMatchTeams,
  competition: sampleCompetition
};

const simpleMatchTemplate: ReturnMatch = {
  id: matchId,
  name: "Quarter final",
  result: null,
  status: "NS" as keyof typeof MatchStatus,
  currentMinute: 90,
  startDate: 1607630900000,
  endDate: 1607637620000,
  odds: sampleOdds,
  teams: sampleMatchTeams,
  competition: sampleCompetition
};

describe("ReturnMatch: isReturnMatch", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnMatch,
    filledMatchTemplate,
    [
      "id",
      "name",
      "teams",
      "status",
      "odds",
    ],
    assert,
  );

  assert({
    given: "An unstarted ReturnMatch",
    should: "Return true",
    actual: isReturnMatch(simpleMatchTemplate),
    expected: true,
  });

  assert({
    given: "An unstarted ReturnMatch with invalid startdate",
    should: "Return false",
    actual: isReturnMatch({
      id: generateUUIDString(),
      name: "FC Bayern - Borussia Dortmund",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: "INVALID",
      endDate: 1607637620000,
      odds: [],
      teams: sampleMatchTeams,
    }),
    expected: false,
  });

  assert({
    given: "An unstarted ReturnMatch with invalid enddate",
    should: "Return false",
    actual: isReturnMatch({
      id: generateUUIDString(),
      name: "FC Bayern - Borussia Dortmund",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1607637620000,
      endDate: -1,
      odds: [],
      teams: sampleMatchTeams,
    }),
    expected: false,
  });

  assert({
    given: "An unstarted ReturnMatch with invalid odds",
    should: "Return false",
    actual: isReturnMatch({
      id: generateUUIDString(),
      name: "FC Bayern - Borussia Dortmund",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1607630900000,
      endDate: 1607637620000,
      odds: [{
        bookmaker: null,
      }],
      teams: sampleMatchTeams,
    }),
    expected: false,
  });

  assert({
    given: "An unstarted ReturnMatch with invalid teams",
    should: "Return false",
    actual: isReturnMatch({
      id: generateUUIDString(),
      name: "FC Bayern - Borussia Dortmund",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1607630900000,
      endDate: 1607637620000,
      odds: [],
      teams: [
        { INVALID_PROPERTY: "FC Bayern" },
        { INVALID_PROPERTY: "BVB" },
      ],
    }),
    expected: false,
  });

  assert({
    given: "An ReturnMatch with currentMinute being a string",
    should: "Return false",
    actual: isReturnMatch({
      id: generateUUIDString(),
      name: "FC Bayern - Borussia Dortmund",
      result: [0, 1],
      status: "NS",
      currentMinute: "INVALID MINUTE",
      startDate: 1607630900000,
      endDate: 1607637620000,
      odds: [],
      teams: sampleMatchTeams,
    }),
    expected: false,
  });
});

describe("ReturnMatch: createReturnMatch", async (assert) => {
  assert({
    given: "properties for return match",
    should: "return a ReturnMatch",
    actual: isReturnMatch(await createReturnMatch(filledMatchTemplate)),
    expected: true,
  });

  assert({
    given: "properties for return match with teams",
    should: "return a ReturnMatch with teams",
    actual: (await createReturnMatch(filledMatchTemplate)).teams?.length,
    expected: filledMatchTemplate.teams?.length,
  });

  assert({
    given: "properties for return match with odds",
    should: "return a ReturnMatch with odds",
    actual: (await createReturnMatch(filledMatchTemplate)).odds?.length,
    expected: filledMatchTemplate.odds?.length,
  });
});

describe("ReturnMatch: createReturnMatchFromSaveFormat", async (assert) => {
  const filledDB = await createFilledMockDB();

  const [competition] = await retrieveAllCompetitions(filledDB);

  const teams: TeamSaveFormat[] = await filledDB.retrieveAll("teams");

  await filledDB.insert("matchTeams", {
    id: matchTeamIds[0],
    name: "FC Bayern",
    properties: [],
    teamId: generateUUIDString(),
    parentId: teams[0].id,
  } as MatchTeamSaveFormat);

  await filledDB.insert("matchTeams", {
    id: matchTeamIds[1],
    name: "FC Bayern",
    properties: [],
    teamId: generateUUIDString(),
    parentId: teams[1].id,
  } as MatchTeamSaveFormat);

  const match = (await createReturnMatchFromSaveFormat(filledDB)({})({
    id: matchId,
    name: "FCB - BVB",
    status: "1H",
    startDate: 1607896606000,
    childIds: matchTeamIds,
    oddsIds: [],
    result: [0, 0],
    properties: [],
    competitionId: competition.id
  } as MatchSaveFormat)).result;

  assert({
    given: "properties for return match",
    should: "return a ReturnMatch",
    actual: isReturnMatch(match),
    expected: true,
  });

  assert({
    given: "properties for return match",
    should: "return a ReturnMatch with teams",
    actual: match?.teams?.length,
    expected: matchTeamIds.length,
  });

  assert({
    given: "properties for return match",
    should: "return a ReturnMatch with competition",
    actual: match?.competition?.country?.id,
    expected: competition?.countryId,
  });

  assert({
    given: "properties for return match",
    should: "return a ReturnMatch with sports",
    actual: match?.competition?.sport?.id,
    expected: competition?.sportId,
  });

  const betTypeIds = Array(5).fill(generateUUIDString());
  betTypeIds.forEach((id, i) =>
    filledDB.insert("betTypes", {
      id,
      name: "BET_" + i,
      properties: [],
      outcomeNames: [],
    } as BetTypeSaveFormat)
  );

  const bookmakerId = generateUUIDString();
  await filledDB.insert("bookmakers", {
    id: bookmakerId,
    name: "Bwin",
    tax: "NONE",
    properties: [],
  });

  const oddsIds = await Promise.all(
    createOdds(filledDB, matchId, bookmakerId, betTypeIds, 8),
  );

  const match2 = (await createReturnMatchFromSaveFormat(filledDB)({})({
    id: matchId,
    name: "FCB - BVB",
    status: "1H",
    startDate: 1607896606000,
    childIds: matchTeamIds,
    oddsIds,
    result: [0, 0],
    properties: [],
    competitionId: competition.id
  } as MatchSaveFormat)).result;
  assert({
    given: "properties for return match",
    should: "return a ReturnMatch with odds",
    actual: match2?.odds?.length,
    expected: oddsIds.length,
  });

  const falsyMatchTeamIds = [generateUUIDString(), matchTeamIds[1]];
  const falsyOddsIds = [generateUUIDString(), oddsIds[1]];
  const matchResponse = (await createReturnMatchFromSaveFormat(filledDB)({})({
    id: matchId,
    name: "FCB - BVB",
    status: "1H",
    startDate: 1607896606000,
    childIds: falsyMatchTeamIds,
    oddsIds: falsyOddsIds,
    result: [0, 0],
    properties: [],
    competitionId: competition.id
  } as MatchSaveFormat));

  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "still return a match",
    actual: isReturnMatch(matchResponse.result),
    expected: true,
  });

  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "still return a match but only with one matchTeam",
    actual: matchResponse.result?.teams.length,
    expected: 1,
  });

  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "still return a match but only with one odds object",
    actual: matchResponse.result?.odds.length,
    expected: 1,
  });

  const matchTeamError = matchResponse.messages && matchResponse.messages[0];
  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "also return a matchTeam NotFound error",
    actual: matchTeamError?.code === "E001" &&
      matchTeamError?.params[0] === "matchTeam",
    expected: true,
  });

  const teamError = matchResponse.messages && matchResponse.messages[1];
  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "also return a teams NotFound error",
    actual: teamError?.code === "E001" && teamError?.params[0] === "team",
    expected: true,
  });

  const oddsError = matchResponse.messages && matchResponse.messages[2];
  assert({
    given:
      "a match in save format with one non existant matchTeamId and one non existant oddsId",
    should: "also return a odds NotFound error",
    actual: oddsError?.code === "E001" && oddsError?.params[0] === "odds",
    expected: true,
  });
});

function createOdds(
  db: DB,
  matchId: UUIDString,
  bookmakerId: UUIDString,
  typeIds: UUIDString[],
  length: number = 0,
): Promise<UUIDString>[] {
  return Array.from({ length })
    .map(async (_, i) => {
      const id = generateUUIDString();

      await db.insert("odds", {
        id,
        name: "Odds #" + i,
        bookmakerId,
        childIds: await Promise.all(createBets(db, typeIds)),
        parentId: matchId,
      } as OddsSaveFormat);

      return id;
    });
}

function createBets(db: DB, typeIds: UUIDString[]): Promise<UUIDString>[] {
  const source = "test";

  return typeIds.map(async (typeId: UUIDString) => {
    const id = generateUUIDString();
    await db.insert("bets", {
      id,
      typeId,
      properties: [
        createProperty({
          source,
          date: 0,
        })<OutcomeSaveFormat[]>(
          "outcomes",
          [
            createOutcomeSaveFormat("1", Math.random() + 1),
            createOutcomeSaveFormat("X", Math.random() + 1),
            createOutcomeSaveFormat("2", Math.random() + 1),
          ],
        ),
      ],
    });

    return id;
  });
}
