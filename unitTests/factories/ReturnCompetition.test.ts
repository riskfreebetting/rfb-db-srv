import { createDB } from "../../scripts/db/db.ts";
import { retrieveAllCompetitions } from "../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { retrieveAllCountries } from "../../scripts/db/saveFormats/CountrySaveFormat.ts";
import { retrieveAllSports } from "../../scripts/db/saveFormats/SportSaveFormat.ts";
import {
  createReturnCompetition,
  createReturnCompetitionById,
  createReturnCompetitionFromSaveFormat,
  isReturnCompetition,
} from "../../scripts/factories/ReturnCompetition.ts";
import { createReturnCountry, createReturnCountryFromSaveFormat } from "../../scripts/factories/ReturnCountry.ts";
import { createReturnSport, createReturnSportFromSaveFormat } from "../../scripts/factories/ReturnSport.ts";
import { generateUUIDString, trace } from "../../scripts/utils.ts";
import { createFilledMockDB } from "../db/mockFactories.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

describe("ReturnCompetition: isReturnCompetition", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnCompetition,
    {
      id: generateUUIDString(),
      name: "MY_COMPETITION",
      alternativeNames: {
        [generateUUIDString()]: ["myCompetition"]
      },
      sport: null,
      country: null
    },
    [
      "id",
      "name",
      "alternativeNames",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnCompetition({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: [],
      sport: null,
      country: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isReturnCompetition({
      id: generateUUIDString(),
      name: 0,
      sport: null,
      country: null,
    }),
    expected: false,
  });
  
  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isReturnCompetition({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
      sport: null,
      country: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non-string alternative name",
    should: "return false",
    actual: isReturnCompetition({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["2", 42],
      sport: null,
      country: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non country",
    should: "return false",
    actual: isReturnCompetition({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["2", 42],
      country: {
        x: "this aint a country"
      },
      sport: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non sport",
    should: "return false",
    actual: isReturnCompetition({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["2", 42],
      country: null,
      sport: {
        x: "this aint a sport"
      }
    }),
    expected: false,
  });
});

describe("ReturnCompetition: createReturnCompetition", async (assert) => {
  const db = await createFilledMockDB();

  const [sport] = await retrieveAllSports(db);
  const [country] = await retrieveAllCountries(db);
  
  assert({
    given: "some Competition properties",
    should: "return a Competition",
    actual: isReturnCompetition(createReturnCompetition({
      id: generateUUIDString(),
      name: "name :)",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      },
      sport: createReturnSportFromSaveFormat(sport),
      country: createReturnCountryFromSaveFormat(country)
    })),
    expected: true,
  });
});

describe("ReturnCompetition: createReturnCompetitionFromSaveFormat", async (assert) => {
  const db = await createFilledMockDB();
  const [competition] = await retrieveAllCompetitions(db);

  assert({
    given: "a Competition in save format",
    should: "return a Competition",
    actual: isReturnCompetition((await createReturnCompetitionFromSaveFormat(db)({
      id: generateUUIDString(),
      properties: [],
      name: "name :)",
      alternativeNames: {
        [generateUUIDString()]: ["name :)"]
      },
      sportId: competition.sportId,
      countryId: competition.countryId,
    })).result),
    expected: true,
  });
});

describe("ReturnCompetition: createReturnCompetitionById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const competitionId = generateUUIDString();
  await db.insert("competitions", {
    id: competitionId,
    properties: [],
    name: "testName",
    alternativeNames: {
      [generateUUIDString()]: ["testName"]
    },
    sportId: generateUUIDString(),
    countryId: generateUUIDString(),
  });

  assert({
    given: "a db and an id",
    should: "return a competition",
    actual: isReturnCompetition((await createReturnCompetitionById(db)(competitionId)).result),
    expected: true,
  });

  const res = await createReturnCompetitionById(db)(generateUUIDString());
  assert({
    given: "a match in save format with a non existant id",
    should: "not return a competition",
    actual: res.result,
    expected: null,
  });

  const err = res.messages && res.messages[0];
  assert({
    given: "a match in save format with a non existant id",
    should: "return a competition not found error",
    actual: err?.code === "E001" && err?.params[0] === "competition",
    expected: true,
  });
});
