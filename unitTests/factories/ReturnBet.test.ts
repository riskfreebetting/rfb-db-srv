import { createDB } from "../../scripts/db/db.ts";
import {
  createOutcomeSaveFormat,
  OutcomeSaveFormat,
} from "../../scripts/db/saveFormats/BetSaveFormat.ts";
import {
  BetTypeSaveFormat,
  createOutcomeNameSaveFormat,
} from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import {
  createOutcomeFromSaveFormat,
  createOutcomesFromProperty,
  createReturnBet,
  createReturnBetById,
  createReturnBetFromSaveFormat,
  createReturnOutcome,
  createSeveralOutcomesFromSaveFormat,
  isReturnBet,
  isReturnOutcome,
  ReturnBet,
} from "../../scripts/factories/ReturnBet.ts";
import { createReturnOutcomeName } from "../../scripts/factories/ReturnBetType.ts";
import {
  EntryNotFoundError,
  isErrorWithInfo,
} from "../../scripts/factories/ReturnObject.ts";
import { createProperty, generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

describe("ReturnBet: isReturnOutcome", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnOutcome,
    {
      name: "1",
      odd: 1.4,
      date: 0,
    },
    [
      "name",
      "odd",
      "date",
    ],
    assert,
  );

  assert({
    given: "An object with number string as odd",
    should: "Return false",
    actual: isReturnOutcome({
      name: "test",
      odd: "1.2",
      date: 0,
    }),
    expected: false,
  });

  assert({
    given: "An object with falsy date",
    should: "Return false",
    actual: isReturnOutcome({
      name: "test",
      odd: 1.2,
      date: -2,
    }),
    expected: false,
  });
});

describe("ReturnBet: createReturnOutcome", (assert) => {
  assert({
    given: "outcome properties",
    should: "return an ReturnOutcome",
    actual: isReturnOutcome(createReturnOutcome({
      name: "TEST",
      odd: 2.21,
      date: 0,
    })),
    expected: true,
  });
});

describe("ReturnBet: createOutcomeFromSaveFormat", (assert) => {
  assert({
    given: "a date and a Outcome in save format",
    should: "return a returnoutcome",
    actual: isReturnOutcome(
      createOutcomeFromSaveFormat(0)({
        name: "TEST",
        odd: 2.1,
      }),
    ),
    expected: true,
  });
});

describe("ReturnBet: createOutcomesFromProperty", (assert) => {
  const date = 0;
  const actual = createOutcomesFromProperty(
    createProperty({ date })<OutcomeSaveFormat[]>(
      "outcomes",
      [
        createOutcomeSaveFormat("1", 1.01),
        createOutcomeSaveFormat("X", 10),
        createOutcomeSaveFormat("2", 120),
      ],
    ),
  );

  assert({
    given: "an outcomes property",
    should: "Return an array with same length as inputted",
    actual: actual.length,
    expected: 3,
  });

  assert({
    given: "an outcomes property",
    should: "Return an array of outcomes",
    actual: actual.every(isReturnOutcome),
    expected: true,
  });

  assert({
    given: "an outcomes property",
    should: "add the date of the property to each outcome",
    actual: actual.every((outcome) => outcome.date === date),
    expected: true,
  });
});

describe("ReturnBet: createSeveralOutcomesFromSaveFormat", (assert) => {
  const actual = createSeveralOutcomesFromSaveFormat({})([
    createProperty({
      date: 0,
    })(
      "not it",
      null,
    ),
    createProperty({ date: 0 })<OutcomeSaveFormat[]>(
      "outcomes",
      [
        createOutcomeSaveFormat("1", 3),
        createOutcomeSaveFormat("X", 3),
        createOutcomeSaveFormat("2", 3),
      ],
    ),
  ]);

  assert({
    given: "some properties with one outcomes",
    should: "find the outcomes property and return an array of ReturnOutcome",
    actual: actual?.every(isReturnOutcome),
    expected: true,
  });

  const actualNonExistant = createSeveralOutcomesFromSaveFormat({})([
    createProperty({
      date: 0,
    })(
      "not it",
      ": (",
    ),
    createProperty({
      date: 0,
    })(
      "not it again",
      ": |",
    ),
  ]);

  assert({
    given: "some properties without any outcomes",
    should: "return null",
    actual: actualNonExistant,
    expected: null,
  });
});

const outcomeNames = [
  createReturnOutcomeName({
    name: "1",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "X",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "2",
    id: generateUUIDString(),
  }),
];

const testBet: ReturnBet = {
  id: generateUUIDString(),
  date: 0,
  type: {
    id: generateUUIDString(),
    outcomeNames,
    name: "YAy",
  },
  outcomes: [
    {
      name: "1",
      odd: 1.5,
      date: 0,
    },
    {
      name: "X",
      odd: 2.7,
      date: 0,
    },
    {
      name: "2",
      odd: 5,
      date: 0,
    },
  ],
};

const emptyBet: ReturnBet = {
  id: generateUUIDString(),
  date: 0,
  type: {
    id: generateUUIDString(),
    outcomeNames,
    name: "YAy",
  },
  outcomes: null,
};

describe("ReturnBet: isReturnBet", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnBet,
    testBet,
    [
      "id",
      "type",
    ],
    assert,
  );

  assert({
    given: "An empty Bet",
    should: "Return true",
    actual: isReturnBet(emptyBet),
    expected: true,
  });

  assert({
    given: "An object with falsy id",
    should: "Return false",
    actual: isReturnBet({
      id: "falsy",
      type: {
        name: "YAy",
        outcomeNames,
      },
      outcomes: [],
    }),
    expected: false,
  });

  assert({
    given: "An object with falsy type",
    should: "Return false",
    actual: isReturnBet({
      id: generateUUIDString(),
      type: {
        NO: "ahaha",
      },
      outcomes: [],
    }),
    expected: false,
  });

  assert({
    given: "An object with falsy outcomes",
    should: "Return false",
    actual: isReturnBet({
      id: generateUUIDString(),
      type: {
        name: "ahaha",
        outcomeNames,
      },
      outcomes: [
        {
          FAlsY: true,
        },
      ],
    }),
    expected: false,
  });
});

describe("ReturnBet: createReturnBet", (assert) => {
  assert({
    given: "some bet properties",
    should: "return a Bet",
    actual: isReturnBet(createReturnBet(testBet)),
    expected: true,
  });

  assert({
    given: "only necessary return bet properties",
    should: "return a Bet",
    actual: isReturnBet(createReturnBet({
      id: generateUUIDString(),
        date: 0,
        type: {
        id: generateUUIDString(),
        name: "X",
        outcomeNames,
      },
    })),
    expected: true,
  });
});

describe("ReturnBet: createReturnBetFromSaveFormat", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const betTypeId = generateUUIDString();
  await db.insert("betTypes", {
    id: betTypeId,
    name: "TEST",
    properties: [],
    outcomeNames: [
      createOutcomeNameSaveFormat({
        name: "test",
        id: generateUUIDString(),
      }),
    ],
  } as BetTypeSaveFormat);

  assert({
    given: "a db with a bettype and its id and a bet in save format",
    should: "return a bet",
    actual: isReturnBet(
      (await createReturnBetFromSaveFormat(db)({})({
        id: generateUUIDString(),
        date: 0,
        name: "BVB-FCB from Bwin",
        typeId: betTypeId,
        properties: [],
      })).result,
    ),
    expected: true,
  });

  const bet = (await createReturnBetFromSaveFormat(db)({})({
    id: generateUUIDString(),
    date: 0,
    name: "BVB-FCB from Bwin",
    typeId: betTypeId,
    properties: [
      createProperty({
        source: "src",
        date: 0,
      })<OutcomeSaveFormat[]>(
        "outcomes",
        [
          createOutcomeSaveFormat("1", 2.1),
          createOutcomeSaveFormat("X", 3),
          createOutcomeSaveFormat("2", 2.8),
        ],
      ),
    ],
  })).result;

  assert({
    given:
      "a db with a bettype and its id and a bet in save format with outcomes",
    should: "return a bet",
    actual: isReturnBet(bet),
    expected: true,
  });

  assert({
    given:
      "a db with a bettype and its id and a bet in save format with 3 outcomes",
    should: "return a bet with 3 outcomes",
    actual: bet?.outcomes?.length,
    expected: 3,
  });

  const nonExistantTypeId = generateUUIDString();
  const failed = await createReturnBetFromSaveFormat(db)({})({
    id: generateUUIDString(),
    date: 0,
    name: "BVB-FCB from Bwin",
    typeId: nonExistantTypeId,
    properties: [
      createProperty({
        source: "src",
        date: 0,
      })<OutcomeSaveFormat[]>(
        "outcomes",
        [
          createOutcomeSaveFormat("1", 2.1),
          createOutcomeSaveFormat("X", 3),
          createOutcomeSaveFormat("2", 2.8),
        ],
      ),
    ],
  });

  assert({
    given: "a db and a typeId which does not exist",
    should: "return no result",
    actual: failed.result,
    expected: null,
  });

  const err = failed.messages && failed.messages[0];
  assert({
    given: "a db and a typeId which does not exist",
    should: "return an error",
    actual: err?.code,
    expected: "E001",
  });
  assert({
    given: "a db and a typeId which does not exist",
    should: "return an error",
    actual: err?.msg,
    expected:
      `E001: EntryNotFound: Could not find entry of type 'betType' with id '${nonExistantTypeId}' in database 'testDB'.`,
  });
});

describe("ReturnBet: createReturnBetById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const betTypeId = generateUUIDString();
  await db.insert("betTypes", {
    id: betTypeId,
    name: "TEST",
    properties: [],
    outcomeNames: [
      createOutcomeNameSaveFormat({
        name: "1",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "X",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "2",
        id: generateUUIDString(),
      }),
    ],
  } as BetTypeSaveFormat);

  const betId = generateUUIDString();
  await db.insert("bets", {
    id: betId,
    name: "BVB-FCB from Bwin",
    typeId: betTypeId,
    properties: [
      createProperty({
        source: "src",
        date: 0,
      })<OutcomeSaveFormat[]>(
        "outcomes",
        [
          createOutcomeSaveFormat("1", 2.1),
          createOutcomeSaveFormat("X", 3),
          createOutcomeSaveFormat("2", 2.8),
        ],
      ),
    ],
  });

  const {
    result: bet,
  } = await createReturnBetById(db)({})(betId);
  assert({
    given: "a DB with a bettype and a bet and that bets id",
    should: "return a bet",
    actual: isReturnBet(bet),
    expected: true,
  });

  assert({
    given: "a DB with a bettype and a bet with 3 outcomes and that bets id",
    should: "return a bet with 3 outcomes",
    actual: bet?.outcomes?.length,
    expected: 3,
  });

  const { messages = [] } = await createReturnBetById(db)({})(
    "not a return bet id!",
  );
  const e = messages[0];
  assert({
    given: "a DB with a bettype and a bet and a id which does not exist",
    should: "return an EntryNotFoundError",
    actual: isErrorWithInfo(e) && e?.error instanceof EntryNotFoundError,
    expected: true,
  });

  assert({
    given: "a DB with a bettype and a bet and a id which does not exist",
    should: "return an type",
    actual: e.params[0],
    expected: "bet",
  });
});
