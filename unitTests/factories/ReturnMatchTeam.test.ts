import { createDB } from "../../scripts/db/db.ts";
import { MatchTeamSaveFormat } from "../../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import {
  createReturnMatchTeam,
  createReturnMatchTeamById,
  createReturnMatchTeamFromSaveFormat,
  isReturnMatchTeam,
} from "../../scripts/factories/ReturnMatchTeam.ts";
import { generateUUIDString, UUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";
import { createFilledMockDB } from "../db/mockFactories.ts";

const matchTeamTemplate = {
  id: generateUUIDString(),
  name: "BVB",
  team: {
    id: generateUUIDString(),
    name: "BVB",
    alternativeNames: {}
  },
  matchId: generateUUIDString(),
};

describe("ReturnMatchTeam: isReturnMatchTeam", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnMatchTeam,
    matchTeamTemplate,
    [
      "id",
      "matchId",
      "name",
    ],
    assert,
  );

  assert({
    given: "an object with none uuid id",
    should: "return false",
    actual: isReturnMatchTeam({
      id: "hi",
      name: "test",
      team: matchTeamTemplate.team,
      matchId: generateUUIDString(),
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy team",
    should: "return false",
    actual: isReturnMatchTeam({
      id: generateUUIDString(),
      name: "test",
      team: {
        notaTeam: true
      },
      matchId: generateUUIDString(),
    }),
    expected: false,
  });

  assert({
    given: "an object with non UUID-matchId",
    should: "return false",
    actual: isReturnMatchTeam({
      id: generateUUIDString(),
      name: "test",
      team: matchTeamTemplate.team,
      matchId: "xxxx",
    }),
    expected: false,
  });
});

describe("ReturnMatchTeam: createReturnMatchTeam", async (assert) => {
  assert({
    given: "properties for return match team",
    should: "return a ReturnMatchTeam",
    actual: isReturnMatchTeam(await createReturnMatchTeam(matchTeamTemplate)),
    expected: true,
  });
});

describe("ReturnMatchTeam: createReturnMatchTeamFromSaveFormat", async (assert) => {
  const db = await createFilledMockDB();
  const [team] = await db.retrieveAll("teams");
  assert({
    given: "properties for return match team",
    should: "return a ReturnMatchTeam",
    actual: isReturnMatchTeam((await createReturnMatchTeamFromSaveFormat(db)({
      id: generateUUIDString(),
      name: "BVB",
      properties: [],
      parentId: generateUUIDString(),
      teamId: team.id,
    }))?.result),
    expected: true,
  });
});

describe("ReturnMatchTeam: createReturnMatchTeamById", async (assert) => {
  const filledDB = await createFilledMockDB();

  const [team] = await filledDB.retrieveAll("teams");
  const matchTeamId: UUIDString = generateUUIDString();
  const matchTeamSaveFormat: MatchTeamSaveFormat = {
    id: matchTeamId,
    name: "FC Bayern",
    teamId: team.id,
    properties: [],
    parentId: generateUUIDString(),
  };
  await filledDB.insert("matchTeams", matchTeamSaveFormat);

  const retrievedMatchTeam =
    (await createReturnMatchTeamById(filledDB)(matchTeamId)).result;

  assert({
    given: "a database with one matchteam and the id of that matchteam",
    should: "return a ReturnMatchTeam",
    actual: isReturnMatchTeam(retrievedMatchTeam),
    expected: true,
  });

  assert({
    given: "a database with one matchteam and the id of that matchteam",
    should: "return a ReturnMatchTeam with the id of the matchteam in the DB",
    actual: retrievedMatchTeam?.id,
    expected: matchTeamSaveFormat.id,
  });
});
