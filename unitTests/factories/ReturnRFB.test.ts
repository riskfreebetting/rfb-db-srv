import {
  createReturnRFB,
  createReturnRFBFromSaveFormat,
  isReturnRFB,
} from "../../scripts/factories/ReturnRFB.ts";

import {
  BetTypeSaveFormat,
  createOutcomeNameSaveFormat,
} from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { MatchSaveFormat } from "../../scripts/db/saveFormats/MatchSaveFormat.ts";
import { MatchTeamSaveFormat } from "../../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import { BookmakerSaveFormat, TaxTypes } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { retrieveAllCompetitions } from "../../scripts/db/saveFormats/CompetitionSaveFormat.ts";

import { UUIDString, generateUUIDString, trace } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";
import { createFilledMockDB } from "../db/mockFactories.ts";

const bookmaker = {
  id: generateUUIDString(),
  name: "test",
  tax: "NONE",
};

describe("ReturnRFB: isReturnRFB", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnRFB,
    {
      id: generateUUIDString(),
      match: null,
      betType: null,
      isAvailable: true,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker: bookmaker,
            odd: 2.5
          }
        }
      }]
    },
    [
      "id",
      "isAvailable",
      "bets"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnRFB({
      id: "FALSY_ID",
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy isAvailable",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: 42,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy match",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: {
        notAMatch: true
      },
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy betType",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: {
        notABetType: true
      },
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bets",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: 22
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, null]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: profit",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, {
        profit: "1022%",
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: outcomes",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: true,
        outcomes: "eh oops"
      }]
    }),
    expected: false,
  });
  
  assert({
    given: "an object with falsy bet: isAvailable",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: "stfu",
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.1
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: outcomes: bookmaker",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker: "AM I a BOOKMAKER?!?",
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: outcomes: odd",
    should: "return false",
    actual: isReturnRFB({
      id: generateUUIDString(),
      isAvailable: true,
      match: null,
      betType: null,
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          outcome1: {
            bookmaker,
            odd: "222"
          }
        }
      }]
    }),
    expected: false,
  });
});

describe("ReturnRFB: createReturnRFB", (assert) => {
  assert({
    given: "some RFB properties",
    should: "return a RFB",
    actual: isReturnRFB(createReturnRFB({
      id: generateUUIDString(),
      bets: [],
      isAvailable: true,
    })),
    expected: true,
  });
});

describe("ReturnRFB: createReturnRFBFromSaveFormat", async (assert) => {
  const db = await createFilledMockDB();

  const [competition] = await retrieveAllCompetitions(db);
  const [team1, team2] = await db.retrieveAll("teams");

  const matchId = generateUUIDString();
  const matchTeamIds = [generateUUIDString(), generateUUIDString()];
  await db.insert("matchTeams", {
    id: matchTeamIds[0],
    name: "FC Bayern",
    properties: [],
    teamId: team1.id,
    parentId: matchId,
  } as MatchTeamSaveFormat);

  await db.insert("matchTeams", {
    id: matchTeamIds[1],
    name: "FC Bayern",
    properties: [],
    teamId: team2.id,
    parentId: matchId,
  } as MatchTeamSaveFormat);

  await db.insert("matches", {
    id: matchId,
    name: "FCB - BVB",
    status: "1H",
    startDate: 1607896606000,
    childIds: matchTeamIds,
    oddsIds: [],
    result: [0, 0],
    properties: [],
    competitionId: competition.id
  } as MatchSaveFormat);
  
  const outcomeId = generateUUIDString();
  const betTypeId = generateUUIDString();
  await db.insert("betTypes", {
    id: betTypeId,
    properties: [],
    name: "testName",
    outcomeNames: [
      createOutcomeNameSaveFormat({
        id: outcomeId,
        name: "outcome1",
      }),
    ],
  } as BetTypeSaveFormat);

  const bookmakerId = generateUUIDString();
  await db.insert("bookmakers", {    
    id: bookmakerId,
    name: "test",
    tax: "NONE" as keyof typeof TaxTypes,
  } as BookmakerSaveFormat);


  const rfb = await createReturnRFBFromSaveFormat(db)({})({
    id: generateUUIDString(),
    isAvailable: true,
    matchId,
    betTypeId,
    bets: [{
      profit: 0.05,
      isAvailable: true,
      outcomes: {
        [outcomeId]: {
          odd: 1.5,
          bookmaker: bookmakerId
        }
      }
    }],
  });

  assert({
    given: "a RFB in save format",
    should: "be successful",
    actual: rfb.successful,
    expected: true,
  });

  assert({
    given: "a RFB in save format",
    should: "return a RFB",
    actual: isReturnRFB(rfb.result),
    expected: true,
  });
});