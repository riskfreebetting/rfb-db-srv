import { createDB } from "../../scripts/db/db.ts";
import {
  BetTypeSaveFormat,
  createOutcomeNameSaveFormat,
  OutcomeNameSaveFormat,
} from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import {
  createReturnBetType,
  createReturnBetTypeById,
  createReturnBetTypeFromSaveFormat,
  createReturnOutcomeName,
  createReturnOutcomeNameFromSaveFormat,
  isReturnBetType,
  isReturnOutcomeName,
  ReturnBetType,
} from "../../scripts/factories/ReturnBetType.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testIfIsIsFunctionForObject } from "../testUtils.ts";

const outcomeNames = [
  createReturnOutcomeName({
    name: "1",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "X",
    id: generateUUIDString(),
  }),
  createReturnOutcomeName({
    name: "2",
    id: generateUUIDString(),
  }),
];

describe("ReturnOutcomeName: isReturnOutcomeName", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnOutcomeName,
    {
      id: generateUUIDString(),
      name: "testName",
    },
    [
      "id",
      "name",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnOutcomeName({
      id: "FALSY_ID",
      name: "test",
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isReturnOutcomeName({
      id: generateUUIDString(),
      name: 0,
    }),
    expected: false,
  });
});

describe("ReturnBetType: createReturnOutcomeName", (assert) => {
  assert({
    given: "an id and a name",
    should: "return an ReturnOutcomeName",
    actual: isReturnOutcomeName(createReturnOutcomeName({
      name: "test",
      id: generateUUIDString(),
    })),
    expected: true,
  });
});

describe("ReturnBetType: createReturnOutcomeNameFromSaveFormat", (assert) => {
  assert({
    given: "a OutcomeName in SaveFormat",
    should: "Return an ReturnOutcomeName",
    actual: isReturnOutcomeName(createReturnOutcomeNameFromSaveFormat({
      id: generateUUIDString(),
      name: "test",
    } as OutcomeNameSaveFormat)),
    expected: true,
  });
});

const betType: ReturnBetType = {
  id: generateUUIDString(),
  name: "test",
  outcomeNames,
};

describe("ReturnBetType: isReturnBetType", (assert) => {
  testIfIsIsFunctionForObject(
    isReturnBetType,
    betType,
    [
      "id",
      "name",
      "outcomeNames",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isReturnBetType({
      id: "aass",
      mame: "test",
      outcomeNames,
    }),
    expected: false,
  });

  assert({
    given: "an object with number as name",
    should: "return false",
    actual: isReturnBetType({
      id: generateUUIDString(),
      name: 0,
      outcomeNames,
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy outcomeNames 1",
    should: "return false",
    actual: isReturnBetType({
      id: generateUUIDString(),
      name: "sup",
      outcomeNames: [
        true,
      ],
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy outcomeNames 2",
    should: "return false",
    actual: isReturnBetType({
      id: generateUUIDString(),
      name: 0,
      outcomeNames: {
        "1": generateUUIDString(),
        "X": "whoops",
        "2": generateUUIDString(),
      },
    }),
    expected: false,
  });
});

describe("ReturnBetType: createReturnBetType", (assert) => {
  assert({
    given: "some BetType properties",
    should: "return a BetType",
    actual: isReturnBetType(createReturnBetType(betType)),
    expected: true,
  });
});

describe("ReturnBetType: createReturnBetTypeFromSaveFormat", (assert) => {
  const betType = createReturnBetTypeFromSaveFormat({
    id: generateUUIDString(),
    properties: [],
    name: "testName",
    outcomeNames: [
      createOutcomeNameSaveFormat({
        name: "1",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "X",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "2",
        id: generateUUIDString(),
      }),
    ],
  });

  assert({
    given: "a BetType in save format",
    should: "return a BetType",
    actual: isReturnBetType(betType),
    expected: true,
  });

  assert({
    given: "a BetType in save format",
    should: "return a BetType with 3 outcomeNames",
    actual: betType.outcomeNames.length,
    expected: 3,
  });
});

describe("ReturnBetType: createReturnBetTypeById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const betTypeId = generateUUIDString();
  await db.insert("betTypes", {
    id: betTypeId,
    properties: [],
    name: "testName",
    outcomeNames: [
      createOutcomeNameSaveFormat({
        id: generateUUIDString(),
        name: "Heei",
      }),
    ],
  } as BetTypeSaveFormat);

  assert({
    given: "a db and an id",
    should: "return a BetType",
    actual: isReturnBetType(
      (await createReturnBetTypeById(db)(betTypeId)).result,
    ),
    expected: true,
  });
});
