import { createPostBody, isPostBody } from "../scripts/RequestFormats.ts";
import describe from "./describe.ts";

describe("RequestFormats: isPostBody", (assert) => {
  assert({
    given: "a post body",
    should: "return true",
    actual: isPostBody({
      source: "Hi",
      date: 0,
      inputs: {
        anything: true,
      },
    }),
    expected: true,
  });

  assert({
    given: "an object without source",
    should: "return false",
    actual: isPostBody({
      date: 0,
      inputs: {
        anything: true,
      },
    }),
    expected: false,
  });

  assert({
    given: "an object with wrong source type",
    should: "return false",
    actual: isPostBody({
      source: 2001,
      date: 0,
      inputs: {
        anything: true,
      },
    }),
    expected: false,
  });

  assert({
    given: "an object without date",
    should: "return false",
    actual: isPostBody({
      source: "Hi",
      inputs: {
        anything: true,
      },
    }),
    expected: false,
  });

  assert({
    given: "an object with wrong date type",
    should: "return false",
    actual: isPostBody({
      source: "Hi",
      date: "not a date",
      inputs: {
        anything: true,
      },
    }),
    expected: false,
  });

  assert({
    given: "an object without inputs",
    should: "return false",
    actual: isPostBody({
      source: "Hi",
      date: 0,
    }),
    expected: false,
  });

  assert({
    given: "an object with wrong inputs (string)",
    should: "return false",
    actual: isPostBody({
      source: "Hi",
      date: 0,
      inputs: "not a record",
    }),
    expected: false,
  });

  assert({
    given: "an object with wrong inputs (null)",
    should: "return false",
    actual: isPostBody({
      source: "Hi",
      date: 0,
      inputs: null,
    }),
    expected: false,
  });
});

describe("RequestFormats: createPostBody", (assert) => {
  assert({
    given: "some postbody parameters",
    should: "return a postbody",
    actual: isPostBody(createPostBody({
      source: "source",
      date: 0,
      inputs: {
        anything: "!!!",
      },
    })),
    expected: true,
  });

  assert({
    given: "some inputs",
    should: "return a postbody by setting default values for source and date",
    actual: isPostBody(createPostBody({
      inputs: {
        anything: "!!!",
      },
    })),
    expected: true,
  });
});
