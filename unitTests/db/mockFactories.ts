import { createDB, DB } from "../../scripts/db/db.ts";
import { BetTypeSaveFormat } from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { BookmakerSaveFormat } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { CompetitionSaveFormat } from "../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { CountrySaveFormat } from "../../scripts/db/saveFormats/CountrySaveFormat.ts";
import { SportSaveFormat } from "../../scripts/db/saveFormats/SportSaveFormat.ts";
import { TeamSaveFormat } from "../../scripts/db/saveFormats/TeamSaveFormat.ts";
import { ReturnCountry } from "../../scripts/factories/ReturnCountry.ts";
import { ReturnSport } from "../../scripts/factories/ReturnSport.ts";
import { generateUUIDString, UUIDString } from "../../scripts/utils.ts";

export async function createFilledMockDB(): Promise<DB> {
  const db = await createDB({ id: "mock" });

  await createMockBookmakers(db, ["bwin", "bet365", "bet-at-home"]);
  await createMockBetTypesWithOutcomes(db, {
    "Winner": ["1", "X", "2"],
    "Over (Total Goals)": ["0.5", "1.5", "2.5", "3.5", "4.5", "5.5", "6.5"],
    "Under (Total Goals)": ["0.5", "1.5", "2.5", "3.5", "4.5", "5.5", "6.5"],
  });
  await createMockTeams(db, [
    "FC Bayern",
    "Borussia Dortmund",
    "RB Leipzig",
    "Borussia Mönchengladbach",
  ]);
  const [sport] = await createMockSports(db, ["Fußball"]);
  const [country] = await createMockCountries(db, ["Deutschland"]);
  await createMockCompetitions(db, ["Bundesliga"], sport.id, country.id);

  return db;
}

export async function createMockSports(
  db: DB,
  names: string[],
): Promise<SportSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const sportId = generateUUIDString();

    const sport: SportSaveFormat = {
      id: sportId,
      properties: [],
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
    };

    await db.insert("sports", sport);

    return sport;
  }));
}

export async function createMockCountries(
  db: DB,
  names: string[],
): Promise<CountrySaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const countryId = generateUUIDString();

    const country: CountrySaveFormat = {
      id: countryId,
      properties: [],
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
    };

    await db.insert("countries", country);

    return country;
  }));
}

export async function createMockTeams(
  db: DB,
  names: string[],
): Promise<TeamSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const teamId = generateUUIDString();

    const team: TeamSaveFormat = {
      id: teamId,
      properties: [],
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
    };

    await db.insert("teams", team);

    return team;
  }));
}

export async function createMockBookmakers(
  db: DB,
  names: string[],
): Promise<BookmakerSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const bookmakerId = generateUUIDString();

    const bookmaker: BookmakerSaveFormat = {
      id: bookmakerId,
      tax: "NONE",
      properties: [],
      name,
    };

    await db.insert("bookmakers", bookmaker);

    return bookmaker;
  }));
}

export async function createMockBetTypes(
  db: DB,
  names: string[],
): Promise<BetTypeSaveFormat[]> {
  return createMockBetTypesWithOutcomes(
    db,
    names.reduce((acc, curr) => ({
      ...acc,
      [curr]: ["A", "B", "C"],
    }), {}),
  );
}

export async function createMockBetTypesWithOutcomes(
  db: DB,
  names: Record<string, string[]>,
): Promise<BetTypeSaveFormat[]> {
  return Promise.all(
    Object.keys(names).map(async (name: string) => {
      const betType: BetTypeSaveFormat = {
        id: generateUUIDString(),
        properties: [],
        name,
        outcomeNames: names[name].map((name) => ({
          name,
          id: generateUUIDString(),
        })),
      };

      await db.insert("betTypes", betType);

      return betType;
    }),
  );
}

export async function createMockCompetitions(
  db: DB, 
  names: string[], 
  sportId: UUIDString, 
  countryId: UUIDString
  ): Promise<CompetitionSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const id = generateUUIDString();
    const competition = {
      id,
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
      properties: [],
      sportId,
      countryId
    };
    await db.insert("competitions", competition);

    return competition;
  }));
}