import {
  createDB,
  DBDeletedError,
  isDB,
  NoGivenIdError,
  NotFoundError,
} from "../../scripts/db/db.ts";
import describe from "../describe.ts";

describe("db: isDB", async (assert) => {
  const noop = () => {};
  assert({
    given: "a db object",
    should: "return true",
    actual: isDB({
      id: "testDB",
      retrieve: noop,
      retrieveAll: noop,
      insert: noop,
      delete: noop,
      deleteDB: noop,
    }),
    expected: true,
  });

  assert({
    given: "an db object without functions",
    should: "return false",
    actual: isDB({
      id: "testDB",
    }),
    expected: false,
  });

  assert({
    given: "an db object without id",
    should: "return false",
    actual: isDB({
      retrieve: noop,
      retrieveAll: noop,
      insert: noop,
      delete: noop,
      deleteDB: noop,
    }),
    expected: false,
  });

  assert({
    given: "an empty object",
    should: "return false",
    actual: isDB({}),
    expected: false,
  });

  assert({
    given: "null",
    should: "return false",
    actual: isDB(null),
    expected: false,
  });
});

describe("db: createDB", async (assert) => {
  const testDB = await createDB({ id: "testDB" });

  assert({
    given: "no parameters",
    should: "return an DB Object with name 'testDB'",
    actual: isDB(testDB) && testDB.id === "testDB",
    expected: true,
  });

  const myDB = await createDB({ id: "myDB" });

  assert({
    given: "the id 'myDB'",
    should: "return an DB Object with name 'myDB'",
    actual: isDB(myDB) && myDB.id === "myDB",
    expected: true,
  });
});

describe("db: insert and retrieve", async (assert) => {
  const testDB = await createDB({ id: "testDB" });
  await testDB.insert("matches", { id: "testMatchId", test: "test" });

  const retrieved = await testDB.retrieve("matches", "testMatchId");
  assert({
    given: "an empty id where a match was inserted with id: 'testMatchId'",
    should: "be able to retrieve this inputted match",
    actual: retrieved.id === "testMatchId" && retrieved.test === "test",
    expected: true,
  });
});

describe("db: retrieve (errors)", async (assert) => {
  const testDB = await createDB({ id: "testDB" });

  assert({
    given: "an empty database",
    should: "not be able to retrieve a match",
    actual: await testDB.retrieve("matches", "testMatchId"),
    expected: null,
  });

  await testDB.insert("matches", { id: "not" });

  assert({
    given: "a database with one entry",
    should: "not be able to retrieve a match with a different id",
    actual: await testDB.retrieve("matches", "testMatchId"),
    expected: null,
  });
});

describe("db: retrieveAll", async (assert) => {
  const testDB = await createDB({ id: "testDB" });
  await testDB.insert("matches", { id: "testMatchId0" });
  await testDB.insert("matches", { id: "testMatchId1" });
  await testDB.insert("matches", { id: "testMatchId2" });
  await testDB.insert("matches", { id: "testMatchId3" });
  await testDB.insert("teams", { id: "testTeam0" });

  const retrieved = await testDB.retrieveAll("matches");

  assert({
    given: "a database with 4 added matches and 1 added team",
    should: "be able to retrieve 4 elements",
    actual: retrieved?.length === 4,
    expected: true,
  });

  assert({
    given: "a database with 4 added matches and 1 added team",
    should: "be able to retrieve only match objects",
    actual: retrieved?.every((m: any) => m.id.includes("testMatchId")),
    expected: true,
  });

  assert({
    given: "a database with 4 added matches and 1 added team",
    should: "be able to retrieve 4 elements",
    actual: (await testDB.retrieveAll("teams"))?.length === 1,
    expected: true,
  });

  assert({
    given: "a database and a non-existant type",
    should: "return an empty array",
    actual: await testDB.retrieveAll("noneType"),
    expected: [],
  });
});

describe("db: insert (overwrite)", async (assert) => {
  const testDB = await createDB({ id: "testDB" });

  await testDB.insert("matches", { id: "test1", name: "FIRST_NAME" });
  await testDB.insert("matches", { id: "test1", name: "OVERWRITTEN_NAME" });

  assert({
    given: "a database where an match was overwritten",
    should: "return the name of the second added match",
    actual: (await testDB.retrieve("matches", "test1")).name,
    expected: "OVERWRITTEN_NAME",
  });
});

describe("db: insert (errors)", async (assert) => {
  const testDB = await createDB({ id: "testDB" });

  await assert.shouldThrow({
    given: "a database where an match without id is inputted",
    should: "throw an 'NoGivenIdError'",
    call: () => testDB.insert("matches", { notId: "not" }),
    expectedError: (err) => err instanceof NoGivenIdError,
  });
});

describe("db: delete", async (assert) => {
  const testDB = await createDB({ id: "testDB" });
  await testDB.insert("matches", { id: "id" });
  await testDB.delete("matches", "id");

  assert({
    given: "a database where an element was inputted and deleted",
    should: "not be able to find that element",
    actual: await testDB.retrieve("matches", "id"),
    expected: null,
  });

  await assert.shouldThrow({
    given:
      "an empty database where an non-existant match is tried to be deleted",
    should: "throw an 'NotFoundError'",
    call: () => testDB.delete("matches", "x"),
    expectedError: (err) => err instanceof NotFoundError,
  });

  testDB.insert("matches", { id: "test2" });

  await assert.shouldThrow({
    given:
      "an database where an match is tried to be deleted but the type is non-existant",
    should: "throw an 'NotFoundError'",
    call: () => testDB.delete("NULL_TYPE", "test2"),
    expectedError: (err) => err instanceof NotFoundError,
  });
});

describe("db: deleteDB", async (assert) => {
  const testDB = await createDB({ id: "testDB" });

  await testDB.insert("matches", { id: "x" });
  await testDB.deleteDB();


  await assert.shouldThrow({
    given: "an deleted database",
    should: "throw an 'DBDeletedError' if you try to retrieve anything",
    call: () => testDB.retrieve("matches", "x"),
    expectedError: (err) => err instanceof DBDeletedError,
  });

  await assert.shouldThrow({
    given: "an deleted database",
    should: "throw an 'DBDeletedError' if you try to insert anything",
    call: () => testDB.insert("matches", { id: "x" }),
    expectedError: (err) => err instanceof DBDeletedError,
  });

  await assert.shouldThrow({
    given: "an deleted database",
    should: "throw an 'DBDeletedError' if you try to delete anything",
    call: () => testDB.delete("matches", "x"),
    expectedError: (err) => err instanceof DBDeletedError,
  });

  await assert.shouldThrow({
    given: "an deleted database",
    should: "throw an 'DBDeletedError' if you try to insert a type",
    call: () => testDB.insertType("matches"),
    expectedError: (err) => err instanceof DBDeletedError,
  });
});
