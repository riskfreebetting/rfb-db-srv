import { createDB } from "../../../scripts/db/db.ts";
import {
  createOutcomeSaveFormat,
  retrieveBetById,
} from "../../../scripts/db/saveFormats/BetSaveFormat.ts";
import { generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";

describe("BetSaveFormat: createOutcomeSaveFormat", (assert) => {
  assert({
    given: "a string and a number",
    should: "return an outcome",
    actual: createOutcomeSaveFormat("testName", 2.8),
    expected: { name: "testName", odd: 2.8 },
  });
});

describe("BetSaveFormat: retrieveBetById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await db.insert("bets", {
    id: generateUUIDString(),
    name: "BMG-FCB from Bwin",
    typeId: generateUUIDString(),
    properties: [],
  });
  const betId = generateUUIDString();
  const savedBet = {
    id: betId,
    name: "BVB-FCB from Bwin",
    typeId: generateUUIDString(),
    properties: [],
  };
  await db.insert("bets", savedBet);

  assert({
    given: "a database with bets and a betId",
    should: "return an bet",
    actual: await retrieveBetById(db)(betId),
    expected: savedBet,
  });
});
