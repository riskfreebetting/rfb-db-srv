import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  BookmakerSaveFormat,
  createBookmakerSaveFormat,
  createBookmakerSaveFormatFromInputs,
  insertBookmaker,
  insertBookmakerFromInputs,
  isBookmakerSaveFormat,
  retrieveAllBookmakers,
  retrieveBookmakerById,
} from "../../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { BookmakerInputs } from "../../../scripts/inputs/BookmakerInputs.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("BookmakerSaveFormat: isBookmakerSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isBookmakerSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_BOOKIE",
      tax: "5% STAKES",
      properties: [],
    },
    [
      "id",
      "name",
      "properties",
      "tax"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isBookmakerSaveFormat({
      id: "FALSY_ID",
      name: "test",
      properties: [],
      tax: "5% STAKES",
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isBookmakerSaveFormat({
      id: generateUUIDString(),
      name: 0,
      properties: [],
      tax: "5% STAKES",
    }),
    expected: false,
  });

  assert({
    given: "an object with valid tax: WINNINGS",
    should: "return true",
    actual: isBookmakerSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      tax: "5% WINNINGS",
    }),
    expected: true,
  });

  assert({
    given: "an object with valid tax: NONE",
    should: "return true",
    actual: isBookmakerSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      tax: "NONE",
    }),
    expected: true,
  });

  assert({
    given: "an object with non string tax",
    should: "return false",
    actual: isBookmakerSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      tax: 0.05,
    }),
    expected: false,
  });
});

describe("BookmakerSaveFormat: createBookmakerSaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a Bookmaker in SaveFormat",
    actual: isBookmakerSaveFormat(createBookmakerSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      tax: "NONE",
      properties: [
        createProperty({
          source: "test",
          date: 0,
        })<string>("test", "xx"),
      ],
    })),
    expected: true,
  });

  assert({
    given: "id and name",
    should:
      "return a Bookmaker in SaveFormat by setting default properties as empty array",
    actual: isBookmakerSaveFormat(createBookmakerSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      tax: "5% STAKES"
    })),
    expected: true,
  });
});

describe("BookmakerSaveFormat: createBookmakerFromInputs", (assert) => {
  const bookmaker1: BookmakerSaveFormat = createBookmakerSaveFormatFromInputs({
    name: "testName",
    tax: "NONE"
  });

  assert({
    given: "Bookmaker inputs",
    should: "return a Bookmaker in SaveFormat",
    actual: isBookmakerSaveFormat(bookmaker1),
    expected: true,
  });

  const bookmaker2: BookmakerSaveFormat = createBookmakerSaveFormatFromInputs({
    name: "testName2",
    tax: "5% WINNINGS"
  });

  assert({
    given: "two created bookmaker save formats",
    should: "create different ids",
    actual: bookmaker1.id !== bookmaker2.id,
    expected: true,
  });
});

describe("BookmakerSaveFormat: insertBookmaker", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bookmaker: BookmakerSaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    tax: "NONE",
    properties: [],
  };

  await insertBookmaker(db)(bookmaker);

  assert({
    given: "Bookmaker inputs and an empty db",
    should: "be able to find the bookmaker with that id",
    actual: await db.retrieve("bookmakers", bookmaker.id) !== null,
    expected: true,
  });
});

describe("BookmakerSaveFormat: insertBookmakerFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const inputs: BookmakerInputs = { 
    name: "testName",
    tax: "NONE",
  };

  const betType = await insertBookmakerFromInputs(db)(inputs);

  const retrieved = await db.retrieve("bookmakers", betType.id);
  assert({
    given: "Bookmaker inputs",
    should: "be able to find a Bookmaker with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "Bookmaker inputs",
    should: "be able to find a Bookmaker with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });
});

describe("BookmakerSaveFormat: retrieveBookmakerById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await db.insert("bookmakers", {
    id: generateUUIDString(),
    name: "Bwin",
    tax: "NONE",
  });
  const bookmakerId = generateUUIDString();
  const savedBookmaker = {
    id: bookmakerId,
    name: "Bet-at-home",
    tax: "NONE",
  };
  await db.insert("bookmakers", savedBookmaker);

  assert({
    given: "a database with bookmakers and an id",
    should: "return the bookmaker with that id",
    actual: await retrieveBookmakerById(db)(bookmakerId),
    expected: savedBookmaker,
  });
});

describe("BookmakerSaveFormat: retrieveAllBookmakers", async (assert) => {
  const db = await createDB({ id: "testDB" });
  const savedBookmakers = await Promise.all([
    createMockBookmaker(db, "bwin"),
    createMockBookmaker(db, "bet365"),
  ]);

  const actual = await retrieveAllBookmakers(db);
  assert({
    given: "a db with bookmakers",
    should: "return an array with the same length as the inputted bookmakers",
    actual: actual.length,
    expected: savedBookmakers.length,
  });

  assert({
    given: "a db with bookmakers",
    should: "return an array with ReturnBookmakers",
    actual: actual.every(isBookmakerSaveFormat),
    expected: true,
  });

  assert({
    given: "a db with bookmakers",
    should: "get Bookmakers with same ids as inputted",
    actual: actual.every((b) => savedBookmakers.some((b2) => b2.id === b.id)),
    expected: true,
  });
});

async function createMockBookmaker(
  db: DB,
  name: string,
): Promise<BookmakerSaveFormat> {
  const bookmakerId = generateUUIDString();

  const bookmaker: BookmakerSaveFormat = {
    id: bookmakerId,
    tax: "NONE",
    properties: [],
    name,
  };

  await db.insert("bookmakers", bookmaker);

  return bookmaker;
}
