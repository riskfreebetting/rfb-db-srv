import { isSaveFormat } from "../../../scripts/db/saveFormats/SaveFormat.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("SaveFormat: isSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_NAME",
      properties: [
        createProperty({
          source: "src",
          date: 0,
        })<string>(
          "NAME",
          "abc",
        ),
      ],
      childIds: [generateUUIDString()],
      parentId: generateUUIDString(),
    },
    [
      "id",
      "name",
      "properties",
    ],
    assert,
  );

  assert({
    given: "a SaveFormat without child and parentIds",
    should: "return true",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: "MY_NAME",
      properties: [],
    }),
    expected: true,
  });

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isSaveFormat({
      id: "FALSY_ID",
      name: "test",
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: 0,
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy properties",
    should: "return false",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [
        {
          FALSY: true,
        },
      ],
    }),
    expected: false,
  });

  assert({
    given: "an object with a falsy parentId",
    should: "return false",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      parentId: "MY FALSY ID",
    }),
    expected: false,
  });

  assert({
    given: "an object with one falsy childId",
    should: "return false",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      childIds: [generateUUIDString(), "MY FALSY ID"],
    }),
    expected: false,
  });

  assert({
    given: "an object with a falsy childId",
    should: "return false",
    actual: isSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      childIds: null,
    }),
    expected: false,
  });
});
