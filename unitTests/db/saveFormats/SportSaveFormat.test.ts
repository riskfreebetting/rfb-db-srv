import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createSportSaveFormat,
  createSportSaveFormatFromInputs,
  insertSport,
  insertSportFromInputs,
  isSportSaveFormat,
  retrieveAllSports,
  retrieveSportById,
  SportSaveFormat,
} from "../../../scripts/db/saveFormats/SportSaveFormat.ts";
import { SportInputs } from "../../../scripts/inputs/SportInputs.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("SportSaveFormat: isSportSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isSportSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_SPORT",
      properties: [],
      alternativeNames: {
        [generateUUIDString()]: ["mySport"]
      }
    },
    [
      "id",
      "name",
      "alternativeNames",
      "properties",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isSportSaveFormat({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isSportSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isSportSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string alternative name",
    should: "return false",
    actual: isSportSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
    }),
    expected: false,
  });
});

describe("SportSaveFormat: createSportSaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a Sport in SaveFormat",
    actual: isSportSaveFormat(createSportSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
      properties: [
        createProperty({
          source: "test",
          date: 0,
        })<string>("hi", "xx"),
      ],
    })),
    expected: true,
  });
});

describe("SportSaveFormat: createSportFromInputs", (assert) => {
  const bmId = generateUUIDString();
  const sport1: SportSaveFormat = createSportSaveFormatFromInputs({
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  assert({
    given: "Sport inputs",
    should: "return a Sport in SaveFormat",
    actual: isSportSaveFormat(sport1),
    expected: true,
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: sport1.alternativeNames,
    expected: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  const sport2: SportSaveFormat = createSportSaveFormatFromInputs({
    name: "testName2",
    alternativeNames: {
      [generateUUIDString()]: ["testName2"]
    }
  });

  assert({
    given: "a name and the same alternativeName",
    should: "have only one alternative name",
    actual: Object.keys(sport2.alternativeNames).length,
    expected: 1,
  });

  assert({
    given: "two created sport save formats",
    should: "create different ids",
    actual: sport1.id !== sport2.id,
    expected: true,
  });
});

describe("SportSaveFormat: insertSport", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const sport: SportSaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    alternativeNames: {},
    properties: [],
  };

  await insertSport(db)(sport);

  assert({
    given: "Sport inputs and an empty db",
    should: "be able to find the sport with that id",
    actual: await db.retrieve("sports", sport.id) !== null,
    expected: true,
  });
});

describe("SportSaveFormat: insertSportFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bmId = generateUUIDString();
  const inputs: SportInputs = { 
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName"]
    }
  };

  const betType = await insertSportFromInputs(db)(inputs);

  const retrieved = await db.retrieve("sports", betType.id);
  assert({
    given: "Sport inputs",
    should: "be able to find a Sport with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "Sport inputs",
    should: "be able to find a Sport with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });

  assert({
    given: "Sport inputs",
    should: "be able to find a Sport with the given name as alternative name",
    actual: retrieved.alternativeNames,
    expected: {
      [bmId]: [inputs.name]
    },
  });
});

describe("SportSaveFormat: retrieveSportById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const [_, sport] = await createMockSports(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with sports and an id",
    should: "return the sport with that id",
    actual: await retrieveSportById(db)(sport.id),
    expected: sport,
  });
});

describe("SportSaveFormat: retrieveAllSports", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const savedSports = await createMockSports(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with sports",
    should: "return an array of length of number of saved sports",
    actual: (await retrieveAllSports(db)).length,
    expected: savedSports.length,
  });

  assert({
    given: "a database with sports",
    should: "return an array of sports in save formats",
    actual: savedSports.every(isSportSaveFormat),
    expected: true,
  });

  assert({
    given: "a database with sports",
    should: "return an array of sports with the same as the inputted ids",
    actual: savedSports.map((s) => s.id),
    expected: savedSports.map((s) => s.id),
  });
});

function createMockSports(db: DB, names: string[]): Promise<SportSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const id = generateUUIDString();
    const sport = {
      id,
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
      properties: [],
    };
    await db.insert("sports", sport);

    return sport;
  }));
}
