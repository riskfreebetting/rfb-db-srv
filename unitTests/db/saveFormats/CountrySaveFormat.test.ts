import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createCountrySaveFormat,
  createCountrySaveFormatFromInputs,
  insertCountry,
  insertCountryFromInputs,
  isCountrySaveFormat,
  retrieveAllCountries,
  retrieveCountryById,
  CountrySaveFormat,
} from "../../../scripts/db/saveFormats/CountrySaveFormat.ts";
import { CountryInputs } from "../../../scripts/inputs/CountryInputs.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("CountrySaveFormat: isCountrySaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isCountrySaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_COUNTRY",
      properties: [],
      alternativeNames: {
        [generateUUIDString()]: ["myCountry"]
      }
    },
    [
      "id",
      "name",
      "alternativeNames",
      "properties",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isCountrySaveFormat({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isCountrySaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isCountrySaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string alternative name",
    should: "return false",
    actual: isCountrySaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
    }),
    expected: false,
  });
});

describe("CountrySaveFormat: createCountrySaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a Country in SaveFormat",
    actual: isCountrySaveFormat(createCountrySaveFormat({
      id: generateUUIDString(),
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
      properties: [
        createProperty({
          source: "test",
          date: 0,
        })<string>("hi", "xx"),
      ],
    })),
    expected: true,
  });
});

describe("CountrySaveFormat: createCountryFromInputs", (assert) => {
  const bmId = generateUUIDString();
  const country1: CountrySaveFormat = createCountrySaveFormatFromInputs({
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  assert({
    given: "Country inputs",
    should: "return a Country in SaveFormat",
    actual: isCountrySaveFormat(country1),
    expected: true,
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: country1.alternativeNames,
    expected: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  const country2: CountrySaveFormat = createCountrySaveFormatFromInputs({
    name: "testName2",
    alternativeNames: {
      [generateUUIDString()]: ["testName2"]
    }
  });

  assert({
    given: "a name and the same alternativeName",
    should: "have only one alternative name",
    actual: Object.keys(country2.alternativeNames).length,
    expected: 1,
  });

  assert({
    given: "two created country save formats",
    should: "create different ids",
    actual: country1.id !== country2.id,
    expected: true,
  });
});

describe("CountrySaveFormat: insertCountry", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const country: CountrySaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    alternativeNames: {},
    properties: [],
  };

  await insertCountry(db)(country);

  assert({
    given: "Country inputs and an empty db",
    should: "be able to find the country with that id",
    actual: await db.retrieve("countries", country.id) !== null,
    expected: true,
  });
});

describe("CountrySaveFormat: insertCountryFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bmId = generateUUIDString();
  const inputs: CountryInputs = { 
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName"]
    }
  };

  const betType = await insertCountryFromInputs(db)(inputs);

  const retrieved = await db.retrieve("countries", betType.id);
  assert({
    given: "Country inputs",
    should: "be able to find a Country with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "Country inputs",
    should: "be able to find a Country with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });

  assert({
    given: "Country inputs",
    should: "be able to find a Country with the given name as alternative name",
    actual: retrieved.alternativeNames,
    expected: {
      [bmId]: [inputs.name]
    },
  });
});

describe("CountrySaveFormat: retrieveCountryById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const [_, country] = await createMockCountries(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with countries and an id",
    should: "return the country with that id",
    actual: await retrieveCountryById(db)(country.id),
    expected: country,
  });
});

describe("CountrySaveFormat: retrieveAllCountries", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const savedCountries = await createMockCountries(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with countries",
    should: "return an array of length of number of saved countries",
    actual: (await retrieveAllCountries(db)).length,
    expected: savedCountries.length,
  });

  assert({
    given: "a database with countries",
    should: "return an array of countries in save formats",
    actual: savedCountries.every(isCountrySaveFormat),
    expected: true,
  });

  assert({
    given: "a database with countries",
    should: "return an array of countries with the same as the inputted ids",
    actual: savedCountries.map((t) => t.id),
    expected: savedCountries.map((t) => t.id),
  });
});

function createMockCountries(db: DB, names: string[]): Promise<CountrySaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const id = generateUUIDString();
    const country = {
      id,
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
      properties: [],
    };
    await db.insert("countries", country);

    return country;
  }));
}
