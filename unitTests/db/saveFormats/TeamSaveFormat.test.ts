import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createTeamSaveFormat,
  createTeamSaveFormatFromInputs,
  insertTeam,
  insertTeamFromInputs,
  isTeamSaveFormat,
  retrieveAllTeams,
  retrieveTeamById,
  TeamSaveFormat,
} from "../../../scripts/db/saveFormats/TeamSaveFormat.ts";
import { TeamInputs } from "../../../scripts/inputs/TeamInputs.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("TeamSaveFormat: isTeamSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isTeamSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_TEAM",
      properties: [],
      alternativeNames: {
        [generateUUIDString()]: ["myTeam"]
      }
    },
    [
      "id",
      "name",
      "alternativeNames",
      "properties",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isTeamSaveFormat({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isTeamSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: [],
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isTeamSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
      properties: [],
    }),
    expected: false,
  });

  assert({
    given: "an object with non string alternative name",
    should: "return false",
    actual: isTeamSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
    }),
    expected: false,
  });
});

describe("TeamSaveFormat: createTeamSaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a Team in SaveFormat",
    actual: isTeamSaveFormat(createTeamSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
      properties: [
        createProperty({
          source: "test",
          date: 0,
        })<string>("hi", "xx"),
      ],
    })),
    expected: true,
  });
});

describe("TeamSaveFormat: createTeamFromInputs", (assert) => {
  const bmId = generateUUIDString();
  const team1: TeamSaveFormat = createTeamSaveFormatFromInputs({
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  assert({
    given: "Team inputs",
    should: "return a Team in SaveFormat",
    actual: isTeamSaveFormat(team1),
    expected: true,
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: team1.alternativeNames,
    expected: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  const team2: TeamSaveFormat = createTeamSaveFormatFromInputs({
    name: "testName2",
    alternativeNames: {
      [generateUUIDString()]: ["testName2"]
    }
  });

  assert({
    given: "a name and the same alternativeName",
    should: "have only one alternative name",
    actual: Object.keys(team2.alternativeNames).length,
    expected: 1,
  });

  assert({
    given: "two created team save formats",
    should: "create different ids",
    actual: team1.id !== team2.id,
    expected: true,
  });
});

describe("TeamSaveFormat: insertTeam", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const team: TeamSaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    alternativeNames: {},
    properties: [],
  };

  await insertTeam(db)(team);

  assert({
    given: "Team inputs and an empty db",
    should: "be able to find the team with that id",
    actual: await db.retrieve("teams", team.id) !== null,
    expected: true,
  });
});

describe("TeamSaveFormat: insertTeamFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bmId = generateUUIDString();
  const inputs: TeamInputs = { 
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName"]
    }
  };

  const team = await insertTeamFromInputs(db)(inputs);

  const retrieved = await db.retrieve("teams", team.id);
  assert({
    given: "Team inputs",
    should: "be able to find a Team with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "Team inputs",
    should: "be able to find a Team with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });

  assert({
    given: "Team inputs",
    should: "be able to find a Team with the given name as alternative name",
    actual: retrieved.alternativeNames,
    expected: {
      [bmId]: [inputs.name]
    },
  });
});

describe("TeamSaveFormat: retrieveTeamById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const [_, team] = await createMockTeams(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with teams and an id",
    should: "return the team with that id",
    actual: await retrieveTeamById(db)(team.id),
    expected: team,
  });
});

describe("TeamSaveFormat: retrieveAllTeams", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const savedTeams = await createMockTeams(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
  );

  assert({
    given: "a database with teams",
    should: "return an array of length of number of saved teams",
    actual: (await retrieveAllTeams(db)).length,
    expected: savedTeams.length,
  });

  assert({
    given: "a database with teams",
    should: "return an array of teams in save formats",
    actual: savedTeams.every(isTeamSaveFormat),
    expected: true,
  });

  assert({
    given: "a database with teams",
    should: "return an array of teams with the same as the inputted ids",
    actual: savedTeams.map((t) => t.id),
    expected: savedTeams.map((t) => t.id),
  });
});

function createMockTeams(db: DB, names: string[]): Promise<TeamSaveFormat[]> {
  return Promise.all(names.map(async (name: string) => {
    const id = generateUUIDString();
    const team = {
      id,
      name,
      alternativeNames: {
        [generateUUIDString()]: [name]
      },
      properties: [],
    };
    await db.insert("teams", team);

    return team;
  }));
}
