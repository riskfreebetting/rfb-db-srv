import { createDB } from "../../../scripts/db/db.ts";
import {
  MatchTeamSaveFormat,
  retrieveMatchTeamById,
} from "../../../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import describe from "../../describe.ts";

describe("MatchTeamSaveFormat: retrieveMatchTeamById", async (assert) => {
  const testMatchTeam: MatchTeamSaveFormat = {
    id: "testId",
    name: "BVB",
    teamId: "team_0",
    properties: [],
    parentId: "matchId",
  };
  const testDB = await createDB({ id: "testDB" });
  await testDB.insert("matchTeams", testMatchTeam);

  assert({
    given: "a test database with one match-entry",
    should: "return one DB-match-entry",
    actual: await retrieveMatchTeamById(testDB)("testId"),
    expected: testMatchTeam,
  });
});
