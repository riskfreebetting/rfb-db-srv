import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  BetTypeSaveFormat,
  createBetTypeSaveFormat,
  createBetTypeSaveFormatFromInputs,
  createOutcomeNameSaveFormat,
  createOutcomeNamesObjectFromInputs,
  insertBetType,
  insertBetTypeFromInputs,
  isBetTypeSaveFormat,
  isOutcomeNameSaveFormat,
  OutcomeNameSaveFormat,
  retrieveAllBetTypes,
  retrieveBetTypeById,
} from "../../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { BetTypeInputs } from "../../../scripts/inputs/BetTypeInputs.ts";
import { createProperty, generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

describe("BetTypeSaveFormat: isOutcomeNameSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isOutcomeNameSaveFormat,
    {
      id: generateUUIDString(),
      name: "testName",
    },
    [
      "id",
      "name",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isOutcomeNameSaveFormat({
      id: "FALSY_ID",
      name: "test",
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isOutcomeNameSaveFormat({
      id: generateUUIDString(),
      name: 0,
    }),
    expected: false,
  });
});

describe("BetTypeSaveFormat: createOutcomeNameSaveFormat", (assert) => {
  assert({
    given: "an id and a name",
    should: "return an ReturnOutcomeName",
    actual: isOutcomeNameSaveFormat(createOutcomeNameSaveFormat({
      name: "test",
      id: generateUUIDString(),
    })),
    expected: true,
  });
});

describe("BetTypeSaveFormat: createOutcomeNamesObjectFromInputs", (assert) => {
  const outcomeNames: OutcomeNameSaveFormat[] =
    createOutcomeNamesObjectFromInputs([
      "a",
      "b",
      "c",
      "d",
    ]);

  assert({
    given: "Several names",
    should: "return a new object for each one",
    actual: outcomeNames.length,
    expected: 4,
  });

  assert({
    given: "Several names",
    should: "return a outcomeNameSaveFormat for each one",
    actual: outcomeNames.every(isOutcomeNameSaveFormat),
    expected: true,
  });

  assert({
    given: "Several names",
    should: "return ooutcomeNameSaveFormats with different ids",
    actual: outcomeNames[0].id !== outcomeNames[1].id,
    expected: true,
  });
});

describe("BetTypeSaveFormat: isBetTypeSaveFormat", (assert) => {
  const outcomeNames = [
    createOutcomeNameSaveFormat({
      name: "1",
      id: generateUUIDString(),
    }),
  ];

  testIfIsIsFunctionForObject(
    isBetTypeSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_BET_TYPE",
      properties: [],
      outcomeNames,
    },
    [
      "id",
      "name",
      "outcomeNames",
      "properties",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isBetTypeSaveFormat({
      id: "FALSY_ID",
      name: "test",
      properties: [],
      outcomeNames,
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isBetTypeSaveFormat({
      id: generateUUIDString(),
      name: 0,
      properties: [],
      outcomeNames,
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy properties",
    should: "return false",
    actual: isBetTypeSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [
        false,
      ],
      outcomeNames,
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy outcomeNames",
    should: "return false",
    actual: isBetTypeSaveFormat({
      id: generateUUIDString(),
      name: "name",
      properties: [],
      outcomeNames: [false],
    }),
    expected: false,
  });
});

describe("BetTypeSaveFormat: createBetTypeSaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a BetType in SaveFormat",
    actual: isBetTypeSaveFormat(createBetTypeSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      properties: [
        createProperty({
          source: "test",
          date: 0,
        })<string>("hi", "xx"),
      ],
    })),
    expected: true,
  });

  assert({
    given: "id and name",
    should:
      "return a BetType in SaveFormat by setting default properties as empty array",
    actual: isBetTypeSaveFormat(createBetTypeSaveFormat({
      id: generateUUIDString(),
      name: "testName",
    })),
    expected: true,
  });
});

describe("BetTypeSaveFormat: createBetTypeSaveFormatFromInputs", (assert) => {
  const betType1: BetTypeSaveFormat = createBetTypeSaveFormatFromInputs({
    name: "testName",
    outcomeNames: ["1", "X", "2"],
  });

  assert({
    given: "BetType inputs",
    should: "return a BetType in SaveFormat",
    actual: isBetTypeSaveFormat(betType1),
    expected: true,
  });

  assert({
    given: "BetType inputs",
    should: "return a BetType with several outcome names",
    actual: Object.keys(betType1.outcomeNames).length,
    expected: 3,
  });

  const betType2: BetTypeSaveFormat = createBetTypeSaveFormatFromInputs({
    name: "testName2",
    outcomeNames: [],
  });

  assert({
    given: "two created bet type save formats",
    should: "create different ids",
    actual: betType1.id !== betType2.id,
    expected: true,
  });
});

describe("BetTypeSaveFormat: insertBetType", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const betType: BetTypeSaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    outcomeNames: [],
    properties: [],
  };

  await insertBetType(db)(betType);

  assert({
    given: "BetType inputs and an empty db",
    should: "be able to find the bettype with that id",
    actual: await db.retrieve("betTypes", betType.id) !== null,
    expected: true,
  });
});

describe("BetTypeSaveFormat: insertBetTypeFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const inputs: BetTypeInputs = {
    name: "testName",
    outcomeNames: ["1", "X", "2"],
  };

  const betType = await insertBetTypeFromInputs(db)(inputs);

  const retrieved = await db.retrieve("betTypes", betType.id);
  assert({
    given: "BetType inputs",
    should: "be able to find a bettype with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "BetType inputs",
    should: "be able to find a bettype with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });
});

describe("BetTypeSaveFormat: retrieveBetTypeById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await db.insert("betTypes", {
    id: generateUUIDString(),
    name: "Winner",
    properties: [],
    outcomeNames: [
      createOutcomeNameSaveFormat({
        name: "hi",
        id: generateUUIDString(),
      }),
    ],
  } as BetTypeSaveFormat);
  const savedBetType = await createMockBetType(db, "Winner");

  assert({
    given: "a database with bettypes and an id",
    should: "return the bettype with that id",
    actual: await retrieveBetTypeById(db)(savedBetType.id),
    expected: savedBetType,
  });
});

describe("BetTypeSaveFormat: retrieveAllBetTypes", async (assert) => {
  const db = await createDB({ id: "testDB" });
  const savedBetTypes = await Promise.all([
    createMockBetType(db, "Winner"),
    createMockBetType(db, "Over"),
  ]);

  const actual = await retrieveAllBetTypes(db);
  assert({
    given: "a db with bettypes",
    should: "return an array with the same length as the inputted bettypes",
    actual: actual.length,
    expected: savedBetTypes.length,
  });

  assert({
    given: "a db with bettypes",
    should: "return an array with ReturnBetTypes",
    actual: actual.every(isBetTypeSaveFormat),
    expected: true,
  });

  assert({
    given: "a db with bettypes",
    should: "get BetTypes with same ids as inputted",
    actual: actual.every((b) => savedBetTypes.some((b2) => b2.id === b.id)),
    expected: true,
  });
});

async function createMockBetType(
  db: DB,
  name: string,
): Promise<BetTypeSaveFormat> {
  const betType: BetTypeSaveFormat = {
    id: generateUUIDString(),
    name,
    outcomeNames: [
      createOutcomeNameSaveFormat({
        name: "1",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "X",
        id: generateUUIDString(),
      }),
      createOutcomeNameSaveFormat({
        name: "2",
        id: generateUUIDString(),
      }),
    ],
    properties: [],
  };

  await db.insert("betTypes", betType);

  return betType;
}
