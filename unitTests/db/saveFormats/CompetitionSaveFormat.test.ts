import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createCompetitionSaveFormat,
  createCompetitionSaveFormatFromInputs,
  insertCompetition,
  insertCompetitionFromInputs,
  isCompetitionSaveFormat,
  retrieveAllCompetitions,
  retrieveCompetitionById,
  CompetitionSaveFormat,
} from "../../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { CompetitionInputs } from "../../../scripts/inputs/CompetitionInputs.ts";
import { createProperty, generateUUIDString, UUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";
import { createMockCompetitions } from "../mockFactories.ts";

describe("CompetitionSaveFormat: isCompetitionSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isCompetitionSaveFormat,
    {
      id: generateUUIDString(),
      name: "MY_COMPETITION",
      properties: [],
      alternativeNames: {
        [generateUUIDString()]: ["myCompetition"]
      },
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    },
    [
      "id",
      "name",
      "alternativeNames",
      "sportId",
      "countryId",
      "properties",
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: "FALSY_ID",
      name: "test",
      alternativeNames: [],
      properties: [],
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: [],
      properties: [],
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with null alternative name",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: null,
      properties: [],
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non string alternative name",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non string sportId",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
      sportId: null,
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non UUIDstring sportId",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
      sportId: "not an id",
      countryId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non string countryId",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
      sportId: generateUUIDString(),
      countryId: null,
    }),
    expected: false,
  });

  assert({
    given: "an object with non UUIDstring countryId",
    should: "return false",
    actual: isCompetitionSaveFormat({
      id: generateUUIDString(),
      name: 0,
      alternativeNames: ["Name 1", 22],
      properties: [],
      sportId: generateUUIDString(),
      countryId: "not an id",
    }),
    expected: false,
  });
});

describe("CompetitionSaveFormat: createCompetitionSaveFormat", (assert) => {
  assert({
    given: "id, name and properties",
    should: "return a Competition in SaveFormat",
    actual: isCompetitionSaveFormat(createCompetitionSaveFormat({
      id: generateUUIDString(),
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
      sportId: generateUUIDString(),
      countryId: generateUUIDString(),
      properties: [],
    })),
    expected: true,
  });
});

describe("CompetitionSaveFormat: createCompetitionFromInputs", (assert) => {
  const bmId = generateUUIDString();
  const sportId = generateUUIDString();
  const countryId = generateUUIDString();

  const competition1: CompetitionSaveFormat = createCompetitionSaveFormatFromInputs({
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName", "TEST_NAME"]
    },
    sportId,
    countryId,  
  });

  assert({
    given: "Competition inputs",
    should: "return a Competition in SaveFormat",
    actual: isCompetitionSaveFormat(competition1),
    expected: true,
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: competition1.alternativeNames,
    expected: {
      [bmId]: ["testName", "TEST_NAME"]
    },
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: competition1.sportId,
    expected: sportId,
  });

  assert({
    given: "a name and one different alternativeName",
    should: "add name to alternativeNames",
    actual: competition1.countryId,
    expected: countryId,
  });

  const competition2: CompetitionSaveFormat = createCompetitionSaveFormatFromInputs({
    name: "testName2",
    alternativeNames: {
      [generateUUIDString()]: ["testName2"]
    },
    sportId: generateUUIDString(),
    countryId: generateUUIDString(),  
  });

  assert({
    given: "a name and the same alternativeName",
    should: "have only one alternative name",
    actual: Object.keys(competition2.alternativeNames).length,
    expected: 1,
  });

  assert({
    given: "two created competition save formats",
    should: "create different ids",
    actual: competition1.id !== competition2.id,
    expected: true,
  });
});

describe("CompetitionSaveFormat: insertCompetition", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const competition: CompetitionSaveFormat = {
    id: generateUUIDString(),
    name: "testName",
    alternativeNames: {},
    properties: [],
    countryId: generateUUIDString(),
    sportId: generateUUIDString()
  };

  await insertCompetition(db)(competition);

  assert({
    given: "Competition inputs and an empty db",
    should: "be able to find the competition with that id",
    actual: await db.retrieve("competitions", competition.id) !== null,
    expected: true,
  });
});

describe("CompetitionSaveFormat: insertCompetitionFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bmId = generateUUIDString();
  const inputs: CompetitionInputs = { 
    name: "testName",
    alternativeNames: {
      [bmId]: ["testName"]
    },
    countryId: generateUUIDString(),
    sportId: generateUUIDString()
  };

  const betType = await insertCompetitionFromInputs(db)(inputs);

  const retrieved = await db.retrieve("competitions", betType.id);
  assert({
    given: "Competition inputs",
    should: "be able to find a Competition with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "Competition inputs",
    should: "be able to find a Competition with the given name",
    actual: retrieved.name,
    expected: inputs.name,
  });

  assert({
    given: "Competition inputs",
    should: "be able to find a Competition with the given name as alternative name",
    actual: retrieved.alternativeNames,
    expected: {
      [bmId]: [inputs.name]
    },
  });
});

describe("CompetitionSaveFormat: retrieveCompetitionById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const [_, competition] = await createMockCompetitions(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
    generateUUIDString(),
    generateUUIDString()
  );

  assert({
    given: "a database with competitions and an id",
    should: "return the competition with that id",
    actual: await retrieveCompetitionById(db)(competition.id),
    expected: competition,
  });
});

describe("CompetitionSaveFormat: retrieveAllCompetitions", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const savedCompetitions = await createMockCompetitions(
    db,
    ["FC Bayern", "Bayer Leverkusen"],
    generateUUIDString(),
    generateUUIDString()
  );

  assert({
    given: "a database with competitions",
    should: "return an array of length of number of saved competitions",
    actual: (await retrieveAllCompetitions(db)).length,
    expected: savedCompetitions.length,
  });

  assert({
    given: "a database with competitions",
    should: "return an array of competitions in save formats",
    actual: savedCompetitions.every(isCompetitionSaveFormat),
    expected: true,
  });

  assert({
    given: "a database with competitions",
    should: "return an array of competitions with the same as the inputted ids",
    actual: savedCompetitions.map((t) => t.id),
    expected: savedCompetitions.map((t) => t.id),
  });
});
