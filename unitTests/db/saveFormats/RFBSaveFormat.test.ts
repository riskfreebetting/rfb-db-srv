import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createRFBSaveFormat,
  createRFBFromInputs,
  insertRFB,
  insertRFBFromInputs,
  isRFBSaveFormat,
  retrieveAllRFBs,
  retrieveRFBById,
  RFBSaveFormat,
  updateRFB
} from "../../../scripts/db/saveFormats/RFBSaveFormat.ts";
import { RFBInputs } from "../../../scripts/inputs/RFBInputs.ts";
import { generateUUIDString, UUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";

import {
  createReturnRFBFromSaveFormat,
} from "../../../scripts/factories/ReturnRFB.ts";
import { MatchSaveFormat } from "../../../scripts/db/saveFormats/MatchSaveFormat.ts";

import {
  BetTypeSaveFormat,
  createOutcomeNameSaveFormat,
} from "../../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { MatchTeamSaveFormat } from "../../../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import { BookmakerSaveFormat, TaxTypes } from "../../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { retrieveAllCompetitions } from "../../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { createFilledMockDB } from "../../db/mockFactories.ts";

describe("RFBSaveFormat: isRFBSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isRFBSaveFormat,
    {
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    },
    [
      "id",
      "isAvailable",
      "matchId",
      "betTypeId",
      "bets"
    ],
    assert,
  );

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isRFBSaveFormat({
      id: "FALSY_ID",
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: "idk?",
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy match",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      match: {
        notAMatch: true
      },
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy betType",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betType: {
        notABetType: true
      },
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bets",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: 22
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }, null]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: profit",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }, {
        profit: "1022%",
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: isAvailable",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: "tadataaaaaa",
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: outcomes: bookmaker",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: "Aint an Id",
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: false,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }]
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy bet: outcomes: odd",
    should: "return false",
    actual: isRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: [{
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: 2.5
          }
        }
      }, {
        profit: 0.02,
        isAvailable: true,
        outcomes: {
          [generateUUIDString()]: {
            bookmaker: generateUUIDString(),
            odd: "222"
          }
        }
      }]
    }),
    expected: false,
  });
});

describe("RFBSaveFormat: createRFBSaveFormat", (assert) => {
  assert({
    given: "rfb save format inputs",
    should: "return a RFB in SaveFormat",
    actual: isRFBSaveFormat(createRFBSaveFormat({
      id: generateUUIDString(),
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: []
    })),
    expected: true,
  });
});

describe("RFBSaveFormat: createRFBFromInputs", async (assert) => {
  const rfb1: RFBSaveFormat = await createRFBFromInputs({
    id: generateUUIDString(),
    rfb: {
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: []
    }
  });

  assert({
    given: "RFB inputs",
    should: "return a RFB in SaveFormat",
    actual: isRFBSaveFormat(rfb1),
    expected: true,
  });

  const rfb2: RFBSaveFormat = createRFBFromInputs({
    id: generateUUIDString(),
    rfb: {
      isAvailable: true,
      matchId: generateUUIDString(),
      betTypeId: generateUUIDString(),
      bets: []
    }
  });

  assert({
    given: "two created rfb save formats",
    should: "create different ids",
    actual: rfb1.id !== rfb2.id,
    expected: true,
  });
});

describe("RFBSaveFormat: insertRFB", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const rfb: RFBSaveFormat = {
    id: generateUUIDString(),
    isAvailable: true,
    matchId: generateUUIDString(),
    betTypeId: generateUUIDString(),
    bets: []
  };

  await insertRFB(db)(rfb);

  assert({
    given: "RFB inputs and an empty db",
    should: "be able to find the rfb with that id",
    actual: await db.retrieve("rfbs", rfb.id) !== null,
    expected: true,
  });
});

describe("RFBSaveFormat: insertRFBFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const inputs: RFBInputs = { 
    matchId: generateUUIDString(),
    betTypeId: generateUUIDString(),
    bets: []
  };

  const rfb = await insertRFBFromInputs(db)(inputs);

  const retrieved = await db.retrieve("rfbs", rfb.id);
  assert({
    given: "RFB inputs",
    should: "be able to find a RFB with the newly created id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "RFB inputs",
    should: "be able to find a RFB with the given matchId",
    actual: retrieved.matchId,
    expected: inputs.matchId,
  });

  assert({
    given: "RFB inputs",
    should: "be able to find a RFB with the given betTypeId",
    actual: retrieved.betTypeId,
    expected: inputs.betTypeId,
  });

  assert({
    given: "RFB inputs",
    should: "be able to find a RFB and it is available",
    actual: retrieved.isAvailable,
    expected: true,
  });
});

describe("RFBSaveFormat: retrieveRFBById", async (assert) => {
  const db = await createDB({ id: "testDB" });
  
  const id = generateUUIDString();
  await db.insert("rfbs", {
    id,
    isAvailable: true,
    matchId: generateUUIDString(),
    betTypeId: generateUUIDString(),
    bets: []
  } as RFBSaveFormat);

  assert({
    given: "a database with rfbs and an id",
    should: "return the rfb with that id",
    actual: (await retrieveRFBById(db)(id)).id,
    expected: id,
  });
});

describe("RFBSaveFormat: retrieveAllRFBs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const ids = [generateUUIDString(), generateUUIDString()];

  await db.insert("rfbs", {
    id: ids[0],
    isAvailable: true,
    matchId: generateUUIDString(),
    betTypeId: generateUUIDString(),
    bets: []
  } as RFBSaveFormat);
  await db.insert("rfbs", {
    id: ids[1],
    isAvailable: true,
    matchId: generateUUIDString(),
    betTypeId: generateUUIDString(),
    bets: []
  } as RFBSaveFormat);

  const savedRFBs = await retrieveAllRFBs(db);

  assert({
    given: "a database with rfbs",
    should: "return an array of length of number of saved rfbs",
    actual: savedRFBs.length,
    expected: 2,
  });

  assert({
    given: "a database with rfbs",
    should: "return an array of rfbs in save formats",
    actual: savedRFBs.every(isRFBSaveFormat),
    expected: true,
  });

  assert({
    given: "a database with rfbs",
    should: "return an array of rfbs with the same as the inputted ids",
    actual: savedRFBs.map((rfb) => rfb.id),
    expected: ids,
  });
});

describe("RFBSaveFormat: updateRFB", async (assert) => {
  const db = await createFilledMockDB();

  const [competition] = await retrieveAllCompetitions(db);

  const matchId = generateUUIDString();
  const matchTeamIds = [generateUUIDString(), generateUUIDString()];
  await db.insert("matchTeams", {
    id: matchTeamIds[0],
    name: "FC Bayern",
    properties: [],
    teamId: generateUUIDString(),
    parentId: matchId,
  } as MatchTeamSaveFormat);

  await db.insert("matchTeams", {
    id: matchTeamIds[1],
    name: "FC Bayern",
    properties: [],
    teamId: generateUUIDString(),
    parentId: matchId,
  } as MatchTeamSaveFormat);

  await db.insert("matches", {
    id: matchId,
    name: "FCB - BVB",
    status: "1H",
    startDate: 1607896606000,
    childIds: matchTeamIds,
    oddsIds: [],
    result: [0, 0],
    properties: [],
    competitionId: competition.id
  } as MatchSaveFormat);
  
  const outcomeId = generateUUIDString();
  const betTypeId = generateUUIDString();
  await db.insert("betTypes", {
    id: betTypeId,
    properties: [],
    name: "testName",
    outcomeNames: [
      createOutcomeNameSaveFormat({
        id: outcomeId,
        name: "outcome1",
      }),
    ],
  } as BetTypeSaveFormat);

  const bookmakerId = generateUUIDString();
  await db.insert("bookmakers", {    
    id: bookmakerId,
    name: "test",
    tax: "NONE" as keyof typeof TaxTypes,
  } as BookmakerSaveFormat);


  const rfb = await insertRFBFromInputs(db)({
    matchId,
    betTypeId,
    bets: [{
      profit: 0.05,
      outcomes: {
        [outcomeId]: {
          odd: 1.5,
          bookmaker: bookmakerId
        }
      }
    }],
  });

  const updatedRFB = await updateRFB(db)(rfb?.id as UUIDString, {
    matchId: rfb?.matchId as UUIDString,
    betTypeId: rfb?.betTypeId as UUIDString,
    bets: []
  });

  assert({
    given: "an created rfb where matchId is not being updated",
    should: "not change matchId",
    actual: updatedRFB.matchId,
    expected: rfb?.matchId
  });

  assert({
    given: "an created rfb where betTypeId is not being updated",
    should: "not change betTypeId",
    actual: updatedRFB.betTypeId,
    expected: rfb?.betTypeId
  });

  assert({
    given: "an created rfb where bet is being added, other one unavailable",
    should: "set rfb to unavailable",
    actual: updatedRFB.isAvailable,
    expected: false
  });

  assert({
    given: "an created rfb where bet is being added, other one unavailable",
    should: "set all bets to unavailable",
    actual: updatedRFB.bets.every(bet => bet.isAvailable === false),
    expected: true
  });

  const updatedRFB2 = await updateRFB(db)(rfb?.id as UUIDString, {
    matchId: rfb?.matchId as UUIDString,
    betTypeId: rfb?.betTypeId as UUIDString,
    bets: [{
      profit: 0.1,
      outcomes: {
        [outcomeId]: {
          odd: 2,
          bookmaker: bookmakerId
        }
      }
    }]
  });

  assert({
    given: "an created rfb where one bet is being added, other one is still unavailable",
    should: "set rfb to available",
    actual: updatedRFB2.isAvailable,
    expected: true
  });

  assert({
    given: "an created rfb where bet is being added, other one is still unavailable",
    should: "add bet",
    actual: updatedRFB2.bets.length,
    expected: 2
  });

  assert({
    given: "an created rfb where bet is being added, other one is still unavailable",
    should: "leave original bet as unavailable",
    actual: updatedRFB2.bets[0].isAvailable,
    expected: false
  });

  assert({
    given: "an created rfb where bet is being added, other one is still unavailable",
    should: "set new bet as available",
    actual: updatedRFB2.bets[1].isAvailable,
    expected: true
  });

  assert({
    given: "an created rfb where bet is being added, other one is still unavailable",
    should: "set odd of new bet as inputted",
    actual: updatedRFB2.bets[1].outcomes[outcomeId].odd,
    expected: 2
  });

  const otherBookmakerId = generateUUIDString();
  const updatedRFB3 = await updateRFB(db)(rfb?.id as UUIDString, {
    matchId: rfb?.matchId as UUIDString,
    betTypeId: rfb?.betTypeId as UUIDString,
    bets: [{
      profit: 0.05,
      outcomes: {
        [outcomeId]: {
          odd: 1.5,
          bookmaker: bookmakerId
        }
      }
    },
    {
      profit: 0.05,
      outcomes: {
        [outcomeId]: {
          odd: 1.5,
          bookmaker: otherBookmakerId
        }
      }
    }]
  });
  
  assert({
    given: "an created rfb where bets are being added and updated",
    should: "add bet",
    actual: updatedRFB3.bets.length,
    expected: 3
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "set updated bet as available again",
    actual: updatedRFB3.bets[0].isAvailable,
    expected: true
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "leave odd of updated bet as original",
    actual: updatedRFB3.bets[0].outcomes[outcomeId].odd,
    expected: 1.5
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "set not added bet as unavailable",
    actual: updatedRFB3.bets[1].isAvailable,
    expected: false
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "not change odd of not updated bet",
    actual: updatedRFB3.bets[1].outcomes[outcomeId].odd,
    expected: 2
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "set added bet as available",
    actual: updatedRFB3.bets[2].isAvailable,
    expected: true
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "set bet as inputted",
    actual: updatedRFB3.bets[2].outcomes[outcomeId].odd,
    expected: 1.5
  });

  assert({
    given: "an created rfb where bets are being added and updated",
    should: "set bookmaker as inputted",
    actual: updatedRFB3.bets[2].outcomes[outcomeId].bookmaker,
    expected: otherBookmakerId
  });
});