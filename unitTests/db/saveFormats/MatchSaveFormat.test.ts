import { createDB, DB } from "../../../scripts/db/db.ts";
import {
  createMatchFromInputs,
  createMatchSaveFormat,
  insertMatch,
  insertMatchFromInputs,
  isMatchSaveFormat,
  MatchSaveFormat,
  retrieveAllMatches,
  updateMatch,
} from "../../../scripts/db/saveFormats/MatchSaveFormat.ts";
import {
  createProperty,
  DateNumber,
  generateUUIDString,
  isDateNumber,
  Property,
  trace,
  UUIDString,
} from "../../../scripts/utils.ts";
import {
  MatchInputs,
} from "../../../scripts/inputs/MatchInputs.ts";
import describe from "../../describe.ts";

import {
  createMockBetTypes,
  createMockBookmakers,
  createMockTeams,
} from "../mockFactories.ts";
import { testIfIsIsFunctionForObject } from "../../testUtils.ts";
import { OddsSaveFormat } from "../../../scripts/db/saveFormats/OddsSaveFormat.ts";
import { BetSaveFormat, OutcomeSaveFormat } from "../../../scripts/db/saveFormats/BetSaveFormat.ts";

const createPropertyOfSource = createProperty({
  source: "source",
  date: 0,
});

const filledProperties: Property<any>[] = [
  createPropertyOfSource<DateNumber>(
    "endDate",
    0,
  ),
  createPropertyOfSource<number>(
    "currentMinute",
    21,
  ),
];

const matchSaveFormat: MatchSaveFormat = {
  id: generateUUIDString(),
  name: "FCB - BVB",
  properties: filledProperties,
  status: "1H",
  result: [0, 0],
  startDate: 0,
  childIds: [generateUUIDString(), generateUUIDString()],
  oddsIds: [generateUUIDString(), generateUUIDString()],
  competitionId: generateUUIDString()
};

describe("MatchSaveFormat: isMatchSaveFormat", (assert) => {
  testIfIsIsFunctionForObject(
    isMatchSaveFormat,
    matchSaveFormat,
    [
      "id",
      "name",
      "oddsIds",
      "childIds",
      "properties",
      "status",
      "competitionId"
    ],
    assert,
  );

  assert({
    given: "a not started Match in SaveFormat",
    should: "return true",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: [],
      status: "TBD",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: true,
  });

  assert({
    given: "an object with falsy id",
    should: "return false",
    actual: isMatchSaveFormat({
      id: "FALSY",
      name: "FCB - BVB",
      properties: [],
      status: "TBD",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non string name",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: null,
      properties: [],
      status: "TBD",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy properties",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: [
        { wow: true },
      ],
      status: "TBD",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy status",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "FFFFFFF",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy result",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "TBD",
      result: 0,
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with non number result",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "TBD",
      result: ["0", "0"],
      startDate: null,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy startDate",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "TBD",
      result: null,
      startDate: "AAAAAAAAAAAAAAAa",
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy childIds",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "TBD",
      result: null,
      startDate: null,
      childIds: ["not an id :)"],
      oddsIds: [],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });

  assert({
    given: "an object with falsy oddsIds",
    should: "return false",
    actual: isMatchSaveFormat({
      id: generateUUIDString(),
      name: "FCB - BVB",
      properties: null,
      status: "TBD",
      result: null,
      startDate: null,
      childIds: [],
      oddsIds: ["not an Id"],
      competitionId: generateUUIDString()
    }),
    expected: false,
  });
});

describe("MatchSaveFormat: createMatchSaveFormat", (assert) => {
  assert({
    given: "match save format properties",
    should: "return a match in save format",
    actual: isMatchSaveFormat(createMatchSaveFormat({
      id: generateUUIDString(),
      name: "TEST",
      properties: [],
      status: "1H",
      result: [1, 2],
      startDate: 0,
      childIds: [],
      oddsIds: [],
      competitionId: generateUUIDString()
    })),
    expected: true,
  });

  assert({
    given: "only the necessary match save format properties",
    should: "return a match in save format by setting default values",
    actual: isMatchSaveFormat(createMatchSaveFormat({
      id: generateUUIDString(),
      name: "TEST",
      startDate: null,
      status: "TBD",
      result: null,
      competitionId: generateUUIDString()
    })),
    expected: true,
  });
});

describe("MatchSaveFormat: createMatchFromInputs", (assert) => {
  const matchTeamIds = [generateUUIDString(), generateUUIDString()];
  const matchTeams = matchTeamIds.map(id => ({
    id,
    teamId: generateUUIDString(),
    properties: [],
    name: "test",
    parentId: generateUUIDString(),
  }));
  const oddsIds = [
    generateUUIDString(),
    generateUUIDString(),
    generateUUIDString(),
  ];

  const actualMatch = createMatchFromInputs({
    source: "test",
    date: 0,
  })({
    id: generateUUIDString(),
    match: {
      result: [1, 2],
      status: "1H",
      currentMinute: 29,
      startDate: 0,
      endDate: 0,
      competitionId: generateUUIDString()
    },
    matchTeams,
    oddsIds,
  });

  assert({
    given: "match inputs",
    should: "return a match in save format",
    actual: isMatchSaveFormat(actualMatch),
    expected: true,
  });

  assert({
    given: "match inputs",
    should: "return a match with several teamIds",
    actual: actualMatch.childIds,
    expected: matchTeamIds,
  });

  assert({
    given: "match inputs",
    should: "return a match with a name",
    actual: actualMatch.name,
    expected: "test - test",
  });

  assert({
    given: "match inputs",
    should: "return a match with several results",
    actual: actualMatch.result?.length,
    expected: 2,
  });

  assert({
    given: "match inputs",
    should: "return a match with currentMinute in properties",
    actual: actualMatch.properties.some((p) =>
      p.name === "currentMinute" && p.value === 29
    ),
    expected: true,
  });

  assert({
    given: "match inputs",
    should: "return a match with endDate in properties",
    actual: actualMatch.properties.some((p) =>
      p.name === "endDate" && isDateNumber(p.value)
    ),
    expected: true,
  });

  assert({
    given: "match inputs",
    should: "return a match with oddsIds",
    actual: actualMatch.oddsIds.length,
    expected: 3,
  });
});

describe("MatchSaveFormat: insertMatch", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await insertMatch(db)(matchSaveFormat);

  assert({
    given: "Match in save format and an empty db",
    should: "be able to find the match by id in the database",
    actual: await db.retrieve("matches", matchSaveFormat.id) !== null,
    expected: true,
  });
});

describe("MatchSaveFormat: insertMatchFromInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const teams = await createMockTeams(db, ["FCB", "BVB"]);
  const teamIds = teams.map((team) => team.id);
  const bookmakers = await createMockBookmakers(db, ["Bwin", "bet365"]);
  const betTypes = await createMockBetTypes(db, ["Winner", "Doppelte Chance"]);

  const {
    match: actualMatch,
    odds: actualOdds,
    bets: actualBets,
    matchTeams: actualMatchTeams,
  } = await insertMatchFromInputs(db)({
    date: 0,
    source: "src",
  })({
    teams: teamIds.map((teamId) => ({ teamId })),
    competitionId: generateUUIDString(),
    result: [1, 2],
    status: "1H",
    currentMinute: 29,
    startDate: 0,
    endDate: 0,
    odds: [
      {
        bookmakerId: bookmakers[0].id,
        bets: [
          {
            typeId: betTypes[0].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[0].outcomeNames[0].id,
                odd: 3.0,
              },
              {
                name: betTypes[0].outcomeNames[1].id,
                odd: 3.4,
              },
              {
                name: betTypes[0].outcomeNames[2].id,
                odd: 2.1,
              },
            ],
          },
          {
            typeId: betTypes[0].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[1].outcomeNames[0].id,
                odd: 2.0,
              },
              {
                name: betTypes[1].outcomeNames[1].id,
                odd: 2.2,
              },
              {
                name: betTypes[1].outcomeNames[2].id,
                odd: 1.8,
              },
            ],
          },
        ],
      },
      {
        bookmakerId: bookmakers[1].id,
        bets: [
          {
            typeId: betTypes[1].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[0].outcomeNames[0].id,
                odd: 3.3,
              },
              {
                name: betTypes[0].outcomeNames[1].id,
                odd: 3.0,
              },
              {
                name: betTypes[0].outcomeNames[2].id,
                odd: 1.9,
              },
            ],
          },
          {
            typeId: betTypes[1].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[1].outcomeNames[0].id,
                odd: 1.8,
              },
              {
                name: betTypes[1].outcomeNames[1].id,
                odd: 2.4,
              },
              {
                name: betTypes[1].outcomeNames[2].id,
                odd: 1.9,
              },
            ],
          },
        ],
      },
    ],
  } as MatchInputs);

  assert({
    given: "a database, propertyData nad a match in input format",
    should: "return a match in save format",
    actual: isMatchSaveFormat(actualMatch),
    expected: true,
  });

  assert({
    given: "a database, propertyData nad a match in input format",
    should: "insert that match into the db",
    actual: await db.retrieve("matches", actualMatch.id) !== null,
    expected: true,
  });

  assert({
    given: "a database, propertyData nad a match in input format",
    should: "insert the odds into the db",
    actual: (await Promise.all(
      actualOdds.map((odds) => db.retrieve("odds", odds.id) !== null),
    )).every((o) => o !== null),
    expected: true,
  });

  assert({
    given: "a database, propertyData nad a match in input format",
    should: "insert the bets into the db",
    actual: (await Promise.all(
      actualBets.map(bet => db.retrieve("bets", bet.id) !== null),
    )).every(o => o !== null),
    expected: true,
  });

  assert({
    given: "a database, propertyData nad a match in input format",
    should: "insert the matchTeams into the db",
    actual: (await Promise.all(
      actualMatchTeams.map((matchTeam) =>
        db.retrieve("matchTeams", matchTeam.id) !== null
      ),
    )).every((o) => o !== null),
    expected: true,
  });
});

describe("MatchSaveFormat: retrieveAllMatches", async (assert) => {
  const testMatch: MatchSaveFormat = {
    id: "testId",
    name: "FCB - BVB",
    status: "NS",
    result: null,
    startDate: 1607855007000,
    properties: [],
    childIds: ["matchTeam1Id", "matchTeam2Id"],
    oddsIds: [],
    competitionId: generateUUIDString()
  };
  const testDB = await createDB({ id: "testDB" });
  await testDB.insert("matches", testMatch);

  assert({
    given: "a test database with one match-entry",
    should: "return one DB-match-entry",
    actual: await retrieveAllMatches(testDB),
    expected: [testMatch],
  });
});

describe("MatchSaveFormat: updateMatch", async assert => {
  const db = await createDB({ id: "testDB" });
  
  const teams = await createMockTeams(db, ["FCB", "BVB", "RBL"]);
  const teamIds = teams.map((team) => team.id);
  const bookmakers = await createMockBookmakers(db, ["Bwin", "bet365", "tipico"]);
  const betTypes = await createMockBetTypes(db, ["Winner", "Doppelte Chance"]);

  const originalMatch = await insertMatchFromInputs(db)({
    date: 0,
    source: "src1"
  })({
    teams: [
      {
        teamId: teamIds[0]
      },
      {
        teamId: teamIds[1]
      },
    ],
    competitionId: generateUUIDString(),
    startDate: 1609414200000,
    endDate: 1609419600000,
    status: "1H",
    result: [0, 0],
    currentMinute: 2,
    odds: [{
      bookmakerId: bookmakers[0].id,
      bets: [
        {
          typeId: betTypes[0].id,
          date: 0,
          outcomes: [
            {
              name: betTypes[0].outcomeNames[0].id,
              odd: 4.3
            },
            {
              name: betTypes[0].outcomeNames[1].id,
              odd: 3.8
            },
            {
              name: betTypes[0].outcomeNames[2].id,
              odd: 2
            },
          ]
        }
      ],
    },
    {
      bookmakerId: bookmakers[1].id,
      bets: []
    }
  ]});

  const res = await updateMatch(db)({
    date: 0,
    source: "src2",
  })(
    originalMatch.match.id, 
    {
      teams: [
        {
          teamId: teamIds[2]
        },
        {
          teamId: teamIds[1]
        },
      ],
      result: [1, 2],
      status: "2H",
      currentMinute: 79,
      odds: [{
        bookmakerId: bookmakers[0].id,
        bets: [
          {
            typeId: betTypes[0].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[0].outcomeNames[0].id,
                odd: 4.2
              },
              {
                name: betTypes[0].outcomeNames[1].id,
                odd: 3.9
              },
              {
                name: betTypes[0].outcomeNames[2].id,
                odd: 1.9
              },
            ]
          },
          {
            typeId: betTypes[1].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[1].outcomeNames[0].id,
                odd: 2
              },
              {
                name: betTypes[1].outcomeNames[1].id,
                odd: 3.5
              },
              {
                name: betTypes[1].outcomeNames[2].id,
                odd: 4.6
              },
            ]
          }
        ]
      },
      {
        bookmakerId: bookmakers[2].id,
        bets: [
          {
            typeId: betTypes[0].id,
            date: 0,
            outcomes: [
              {
                name: betTypes[0].outcomeNames[0].id,
                odd: 4
              },
              {
                name: betTypes[0].outcomeNames[1].id,
                odd: 4.5
              },
              {
                name: betTypes[0].outcomeNames[2].id,
                odd: 2.1
              },
            ]
          }
        ]
      }
    ],
    competitionId: generateUUIDString()
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return a matchSaveFormat",
    actual: isMatchSaveFormat(res.match),
    expected: true
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the updated result",
    actual: res.match.result,
    expected: [1, 2]
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the updated status",
    actual: res.match.status,
    expected: "2H"
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the updated and the original currentMinute in properties",
    actual: res.match.properties.filter((p: Property<any>) => p.name === "currentMinute").length,
    expected: 2
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return only the original currentMinute in properties because no new endDate was given",
    actual: res.match.properties.filter((p: Property<any>) => p.name === "endDate").length,
    expected: 1
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return 2 new matchTeams",
    actual: res.matchTeams.length,
    expected: 2
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return 2 new matchTeams",
    actual: res.matchTeams[0].teamId,
    expected: teamIds[2]
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return 2 new matchTeams",
    actual: res.matchTeams[1].teamId,
    expected: teamIds[1]
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the 1 updated odd, 1 existing odd and 1 new odd",
    actual: res.odds.length,
    expected: 3
  });

  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the correct bookmaker ids",
    actual: res.odds.map((o: OddsSaveFormat) => o.bookmakerId),
    expected: bookmakers.map(b => b.id)
  });

  const findBet = findBetInBets(res.bets);
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the updated bet of the updated bookmaker",
    actual: findBet(res.odds[0].childIds[0])?.properties.filter(p => p.name === "outcomes").length,
    expected: 2
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the inserted bet of the updated bookmaker",
    actual: findBet(res.odds[0].childIds[1])?.typeId,
    expected: betTypes[1].id
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the originally inserted odds",
    actual: res.odds[1].childIds.length,
    expected: 0
  });
  
  assert({
    given: "a db with an existing match and some updates for that match",
    should: "return the inserted bookmaker",
    actual: res.odds[2]?.bookmakerId,
    expected: bookmakers[2]?.id
  });

  function findBetInBets(bets: BetSaveFormat[]) {
    return (id: UUIDString) => bets.find(b => b.id === id);
  }
});