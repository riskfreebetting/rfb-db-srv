import { createDB } from "../../../scripts/db/db.ts";
import { retrieveOddsById } from "../../../scripts/db/saveFormats/OddsSaveFormat.ts";
import { generateUUIDString } from "../../../scripts/utils.ts";
import describe from "../../describe.ts";

describe("OddsSaveFormat: retrieveOddsById", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await db.insert("odds", {
    id: generateUUIDString(),
    name: "1",
    properties: [],
    bookmakerId: generateUUIDString(),
    childIds: [],
    parentId: generateUUIDString(),
  });

  const oddsId = generateUUIDString();
  const odds = {
    id: oddsId,
    name: "2",
    properties: [],
    bookmakerId: generateUUIDString(),
    childIds: [],
    parentId: generateUUIDString(),
  };
  await db.insert("odds", odds);

  assert({
    given: "a db with odds and an id",
    should: "return the odds object with the id",
    actual: await retrieveOddsById(db)(oddsId),
    expected: odds,
  });
});
