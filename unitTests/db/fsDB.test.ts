import { ensureDir, exists } from "https://deno.land/std@0.93.0/fs/mod.ts";

import {
  DBDeletedError,
  isDB,
  NoGivenIdError,
  NotFoundError,
} from "../../scripts/db/db.ts";
import { createAllFsDBs, createFsDB, getPathOfDatabase } from "../../scripts/db/fsDB.ts";
import describe from "../describe.ts";

describe("fsDB: deleteDB", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });

  try {
    await testDB.insert("matches", { id: "x" });
    await testDB.deleteDB();

    await assert.shouldThrow({
      given: "an deleted database",
      should: "throw an 'DBDeletedError' if you try to retrieve anything",
      call: () => testDB.retrieve("matches", "x"),
      expectedError: (err) => err instanceof DBDeletedError,
    });

    await assert.shouldThrow({
      given: "an deleted database",
      should: "throw an 'DBDeletedError' if you try to insert anything",
      call: () => testDB.insert("matches", { id: "x" }),
      expectedError: (err) => err instanceof DBDeletedError,
    });

    await assert.shouldThrow({
      given: "an deleted database",
      should: "throw an 'DBDeletedError' if you try to delete anything",
      call: () => testDB.delete("matches", "x"),
      expectedError: (err) => err instanceof DBDeletedError,
    });

    await assert.shouldThrow({
      given: "an deleted database",
      should: "throw an 'DBDeletedError' if you try to insert a type",
      call: () => testDB.insertType("matches"),
      expectedError: (err) => err instanceof DBDeletedError,
    });

    assert({
      given: "an deleted database",
      should: "not be able to find the database in the db folder",
      actual: await exists(getPathOfDatabase("testDB")),
      expected: false
    });
  } finally {
    testDB.deleteDB();
  }
});

describe("fsDB: createFsDB", async (assert) => {
  const dir = getPathOfDatabase("myDB").concat("\\matches");
  await ensureDir(dir);
  await Deno.writeTextFile(
    dir.concat("\\match.json"),
    JSON.stringify({
      id: "match",
      value: "FLAG"
    })
  );
  
  const testDB = await createFsDB({ id: "testDB" });
  const myDB = await createFsDB({ id: "myDB" });

  try {
    assert({
      given: "no parameters",
      should: "return an DB Object with name 'testDB'",
      actual: isDB(testDB) && testDB.id === "testDB",
      expected: true,
    });
    
    assert({
      given: "no parameters",
      should: "be able to find the database in the db folder",
      actual: await exists(getPathOfDatabase("myDB")),
      expected: true
    });

    assert({
      given: "the id 'myDB' and a db-folder which already exists",
      should: "return an DB Object with name 'myDB'",
      actual: isDB(myDB) && myDB.id === "myDB",
      expected: true,
    });
    
    assert({
      given: "the id 'myDB' and a db-folder which already exists",
      should: "have loaded the existing DB",
      actual: (await myDB.retrieve("matches", "match"))?.value,
      expected: "FLAG"
    });
  } finally {
    await myDB.deleteDB();    
    await testDB.deleteDB();
  }
});

describe("fsDB: insert and retrieve", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });
  try {
    await testDB.insert("matches", { id: "testMatchId", test: "test" });

    assert({
      given: "an empty db where a match was inserted with id: 'testMatchId'",
      should: "be able to find the file in the db folder",
      actual: await exists(getPathOfDatabase("testDB").concat("\\matches\\testMatchId.json")),
      expected: true
    });

    const retrieved = await testDB.retrieve("matches", "testMatchId");
    assert({
      given: "an empty db where a match was inserted with id: 'testMatchId'",
      should: "be able to retrieve this inputted match",
      actual: retrieved.id === "testMatchId" && retrieved.test === "test",
      expected: true,
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: retrieve (errors)", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });

  try {
    assert({
      given: "an empty database",
      should: "not be able to retrieve a match",
      actual: await testDB.retrieve("matches", "testMatchId"),
      expected: null,
    });
  
    await testDB.insert("matches", { id: "not" });
  
    assert({
      given: "a database with one entry",
      should: "not be able to retrieve a match with a different id",
      actual: await testDB.retrieve("matches", "testMatchId"),
      expected: null,
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: retrieveAll", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });
  try {
    await testDB.insert("matches", { id: "testMatchId0" });
    await testDB.insert("matches", { id: "testMatchId1" });
    await testDB.insert("matches", { id: "testMatchId2" });
    await testDB.insert("matches", { id: "testMatchId3" });
    await testDB.insert("teams", { id: "testTeam0" });

    const retrieved = await testDB.retrieveAll("matches");

    assert({
      given: "a database with 4 added matches and 1 added team",
      should: "be able to retrieve 4 elements",
      actual: retrieved?.length === 4,
      expected: true,
    });

    assert({
      given: "a database with 4 added matches and 1 added team",
      should: "be able to retrieve only match objects",
      actual: retrieved?.every((m: any) => m.id.includes("testMatchId")),
      expected: true,
    });

    assert({
      given: "a database with 4 added matches and 1 added team",
      should: "be able to retrieve 4 elements",
      actual: (await testDB.retrieveAll("teams"))?.length === 1,
      expected: true,
    });

    assert({
      given: "a database and a non-existant type",
      should: "return an empty array",
      actual: await testDB.retrieveAll("noneType"),
      expected: [],
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: insert (overwrite)", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });

  try {
    await testDB.insert("matches", { id: "test1", name: "FIRST_NAME" });
    await testDB.insert("matches", { id: "test1", name: "OVERWRITTEN_NAME" });

    assert({
      given: "a database where an match was overwritten",
      should: "return the name of the second added match",
      actual: (await testDB.retrieve("matches", "test1")).name,
      expected: "OVERWRITTEN_NAME",
    });
    
    assert({
      given: "a database where an match was overwritten",
      should: "be able to find the overwritten name in the db file",
      actual: (await Deno.readTextFile(getPathOfDatabase("testDB").concat("\\matches\\test1.json"))).includes("OVERWRITTEN_NAME"),
      expected: true
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: insert (errors)", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });
  
  try {
    await assert.shouldThrow({
      given: "a database where an match without id is inputted",
      should: "throw an 'NoGivenIdError'",
      call: () => testDB.insert("matches", { notId: "not" }),
      expectedError: (err) => err instanceof NoGivenIdError,
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: delete", async (assert) => {
  const testDB = await createFsDB({ id: "testDB" });
  try {
    await testDB.insert("matches", { id: "id" });
    await testDB.delete("matches", "id");
    
    assert({
      given: "a database where an element was inputted and deleted",
      should: "not be able to find the file in the db folder",
      actual: await exists(getPathOfDatabase("testDB").concat("\\matches\\id.json")),
      expected: false
    });

    assert({
      given: "a database where an element was inputted and deleted",
      should: "not be able to find that element",
      actual: await testDB.retrieve("matches", "id"),
      expected: null,
    });

    await assert.shouldThrow({
      given: "an empty database where an non-existant match is tried to be deleted",
      should: "throw an 'NotFoundError'",
      call: () => testDB.delete("matches", "x"),
      expectedError: (err) => err instanceof NotFoundError,
    });

    await testDB.insert("matches", { id: "test2" });

    await assert.shouldThrow({
      given: "an database where an match is tried to be deleted but the type is non-existant",
      should: "throw an 'NotFoundError'",
      call: () => testDB.delete("NULL_TYPE", "test2"),
      expectedError: (err) => err instanceof NotFoundError,
    });
  } finally {
    await testDB.deleteDB();
  }
});

describe("fsDB: createAllFsDBs", async assert => {
  const expectedDBs = await Promise.all([
    createFsDB({ id: "a" }),
    createFsDB({ id: "b" }),
    createFsDB({ id: "c" }),
  ]);

  try {
    const expectedDBIds = expectedDBs.map(db => db.id);
    
    await expectedDBs[0].insert("matches", { id: "id", test: true });

    const actualDBs = await createAllFsDBs();
    const actualDBIds = Object.keys(actualDBs);
    assert({
      given: "several created FS DBs",
      should: "return an object with the ids as keys",
      actual: expectedDBIds.every(id => actualDBIds.includes(id)),
      expected: true
    });
  
    assert({
      given: "several created FS DBs",
      should: "return an object with dbs as values",
      actual: Object.values(actualDBs).every(db => db.insert),
      expected: true
    });
  } finally {
    await Promise.all(expectedDBs.map(db => db.deleteDB()));
  }
});