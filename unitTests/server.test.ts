import describe from "./describe.ts";
import { insertMatchFromInputs, MatchSaveFormat } from "../scripts/db/saveFormats/MatchSaveFormat.ts";
import {
  isReturnMatch,
  ReturnMatch,
} from "../scripts/factories/ReturnMatch.ts";
import {
  handleGetBetTypes,
  handleGetBookmakers,
  handleGetMatches,
  handleGetTeams,
  handleUpdateMatch,
  handlePostBetType,
  handlePostBookmaker,
  handlePostMatch,
  handlePostTeam,
  handleUpdateTeam,
  handlePostCompetition,
  handleGetCompetitions,
  handleUpdateCompetition,
  handlePostSport,
  handleUpdateSport,
  handleGetSports
} from "../scripts/server.ts";
import { createDB, DB } from "../scripts/db/db.ts";
import { MatchTeamSaveFormat } from "../scripts/db/saveFormats/MatchTeamSaveFormat.ts";
import { TeamSaveFormat } from "../scripts/db/saveFormats/TeamSaveFormat.ts";
import { generateUUIDString, Property, trace, UUIDString } from "../scripts/utils.ts";
import { isReturnTeam, ReturnTeam } from "../scripts/factories/ReturnTeam.ts";
import {
  isReturnBookmaker,
  ReturnBookmaker,
} from "../scripts/factories/ReturnBookmaker.ts";
import {
  isReturnBetType,
  ReturnBetType,
} from "../scripts/factories/ReturnBetType.ts";
import { BetTypeInputs } from "../scripts/inputs/BetTypeInputs.ts";
import { createPostBody } from "../scripts/RequestFormats.ts";
import { BookmakerInputs } from "../scripts/inputs/BookmakerInputs.ts";
import { TeamInputs } from "../scripts/inputs/TeamInputs.ts";
import { MatchInputs } from "../scripts/inputs/MatchInputs.ts";
import {
  createMockBetTypes,
  createMockBookmakers,
  createMockCompetitions,
  createMockTeams,
  createMockSports,
  createFilledMockDB
} from "./db/mockFactories.ts";
import { OddsSaveFormat } from "../scripts/db/saveFormats/OddsSaveFormat.ts";
import { BetSaveFormat, OutcomeSaveFormat } from "../scripts/db/saveFormats/BetSaveFormat.ts";
import { isReturnCompetition, ReturnCompetition } from "../scripts/factories/ReturnCompetition.ts";
import { CompetitionInputs } from "../scripts/inputs/CompetitionInputs.ts";
import { SportInputs } from "../scripts/inputs/SportInputs.ts";
import { isReturnSport, ReturnSport } from "../scripts/factories/ReturnSport.ts";
import { CompetitionSaveFormat, retrieveAllCompetitions } from "../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { insertSportFromInputs } from "../scripts/db/saveFormats/SportSaveFormat.ts";
import { insertCountryFromInputs } from "../scripts/db/saveFormats/CountrySaveFormat.ts";

describe("server: handleGetSports", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const sports = await createMockSports(db, ["FCB", "BVB"]);

  const actual = (await handleGetSports(db)()).result;
  assert({
    given: "a test db with sports",
    should: "return an array of same length as sports in DB.",
    actual: actual?.length,
    expected: sports.length,
  });

  assert({
    given: "a test db with sports",
    should: "return an array of ReturnSports",
    actual: actual?.every(isReturnSport),
    expected: true,
  });

  assert({
    given: "a test db with sports",
    should: "get ReturnSports with same ids as inputted",
    actual: actual?.every((s: ReturnSport) =>
      sports.some((s2: ReturnSport) => s2.id === s.id)
    ),
    expected: true,
  });
});

describe("server: handlePostSport", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostSport(db)(createPostBody({
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testSportName"]
      },
    } as SportInputs,
  }))).result as ReturnSport;

  const retrieved = await db.retrieve("sports", saved.id);
  assert({
    given: "a body with a name as inputs and an empty db",
    should: "be able to find the sport with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with a name as inputs and an empty db",
    should: "create an db entry with the given name",
    actual: retrieved.name,
    expected: saved.name,
  });
});

describe("server: handleUpdateSport", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostSport(db)(createPostBody({
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
    } as SportInputs,
  }))).result as ReturnSport;

  const updated = (await handleUpdateSport(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
    } as SportInputs,
  }, {
    id: saved.id
  })).result as ReturnSport;

  const retrieved = await db.retrieve("sports", updated.id);
  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "be able to find the sport with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "find the updated match in the db",
    actual: retrieved.name,
    expected: updated.name,
  });
});

describe("server: handlePostCompetition", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const sport = await insertSportFromInputs(db)({
    name: "Fußball",
    alternativeNames: {}
  });

  const country = await insertCountryFromInputs(db)({
    name: "Deutschland",
    alternativeNames: {}
  });

  const saved = (await handlePostCompetition(db)(createPostBody({
    inputs: {
      name: "testCompetitionName",
      alternativeNames: {
        [generateUUIDString()]: ["testCompetitionName"]
      },
      sportId: sport.id,
      countryId: country.id
    } as CompetitionInputs,
  }))).result as ReturnCompetition;

  const retrieved = await db.retrieve("competitions", saved.id);
  assert({
    given: "a body with name, alternativeNames, sportId and countryId as inputs and an empty db",
    should: "be able to find the competition with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with name, alternativeNames, sportId and countryId as inputs and an empty db",
    should: "create a db entry with the given name",
    actual: retrieved.name,
    expected: saved.name,
  });

  assert({
    given: "a body with name, alternativeNames, sportId and countryId as inputs and an empty db",
    should: "create a db entry with the given sportId",
    actual: retrieved.sportId,
    expected: saved.sport?.id,
  });

  assert({
    given: "a body with name, alternativeNames, sportId and countryId as inputs and an empty db",
    should: "create a db entry with the given countryId",
    actual: retrieved.countryId,
    expected: saved.country?.id,
  });

  assert({
    given: "a body with name, alternativeNames, sportId and countryId as inputs and an empty db",
    should: "create a db entry with the given alternativeNames",
    actual: retrieved.alternativeNames,
    expected: saved.alternativeNames,
  });
});

describe("server: handleGetCompetition", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const competitions = await createMockCompetitions(db, ["Bundesliga"], generateUUIDString(), generateUUIDString());

  const actual = (await handleGetCompetitions(db)()).result;
  assert({
    given: "a test db with competitions",
    should: "return an array of same length as competitions in DB.",
    actual: actual?.length,
    expected: competitions.length,
  });

  assert({
    given: "a test db with competitions",
    should: "return an array of ReturnCompetitions",
    actual: actual?.every(isReturnCompetition),
    expected: true,
  });

  assert({
    given: "a test db with competitions",
    should: "get ReturnCompetitions with same ids as inputted",
    actual: actual?.every((c: ReturnCompetition) =>
      competitions.some((c2: CompetitionSaveFormat) => c2.id === c.id)
    ),
    expected: true,
  });
});

describe("server: handleUpdateCompetition", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostCompetition(db)(createPostBody({
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    } as CompetitionInputs,
  }))).result as ReturnCompetition;

  const updated = (await handleUpdateCompetition(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "testName",
      alternativeNames:  {
        [generateUUIDString()]: ["testName"]
      },
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    } as CompetitionInputs,
  }, {
    id: saved.id
  })).result as ReturnCompetition;

  const retrieved = await db.retrieve("competitions", updated.id);
  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "be able to find the competition with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "find the updated match in the db",
    actual: retrieved.name,
    expected: updated.name,
  });
});

describe("server: handleGetMatches", async (assert) => {
  const db = await createFilledMockDB();

  const [competition] = await retrieveAllCompetitions(db);
  const teamIds = (await db.retrieveAll("teams")).map((team: TeamSaveFormat) => team.id);
  const matches = await Promise.all([
    createMockMatch(db, competition.id, teamIds, true), 
    createMockMatch(db, competition.id, teamIds, false)
  ],);

  const actual = (await handleGetMatches(db)({
    source: ""
  })).result;
  assert({
    given: "a test db with matches and matchteams",
    should: "return an array of same length as matches in DB.",
    actual: actual?.length,
    expected: matches.length,
  });

  assert({
    given: "a test db with matches and matchteams",
    should: "return an array of ReturnMatches",
    actual: actual?.every(isReturnMatch),
    expected: true,
  });

  assert({
    given: "a test db with matches and matchteams",
    should: "get ReturnMatches with same ids as inputted",
    actual: actual?.every((m: ReturnMatch) =>
      matches.some((m2) => m2.id === m.id)
    ),
    expected: true,
  });

  // insert falsy match with random team/odds/competition Ids => 5 errors
  const match: MatchSaveFormat = {
    id: generateUUIDString(),
    name: "FCB - BVB",
    status: "NS",
    result: null,
    startDate: 1607855007000,
    properties: [],
    childIds: [generateUUIDString(), generateUUIDString()],
    oddsIds: [generateUUIDString(), generateUUIDString()],
    competitionId: generateUUIDString()
  };
  await db.insert("matches", match);

  const errorRes = await handleGetMatches(db)({
    source: ""
  });
  assert({
    given: "a database with 2 correct and 1 falsy match",
    should: "return unsuccessful",
    actual: errorRes.successful,
    expected: false,
  });

  assert({
    given: "a database with 2 correct and 1 falsy match",
    should: "return 3 matches",
    actual: errorRes.result?.length,
    expected: 3,
  });

  assert({
    given: "a database with 2 correct and 1 falsy match",
    should: "return four errors",
    actual: errorRes.messages?.length,
    expected: 5,
  });
});

describe("server: handlePostMatch", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const teams = await createMockTeams(db, ["FCB", "BVB"]);
  const teamIds = teams.map((team) => team.id);
  const bookmakers = await createMockBookmakers(db, ["Bwin", "bet365"]);
  const betTypes = await createMockBetTypes(db, ["Winner", "Doppelte Chance"]);

  const returnMatch = (await handlePostMatch(db)(createPostBody({
    inputs: {
      teams: teamIds.map((teamId) => ({ teamId })),
      result: [1, 2],
      status: "1H",
      currentMinute: 29,
      startDate: 0,
      endDate: 0,
      odds: [
        {
          bookmakerId: bookmakers[0].id,
          bets: [
            {
              typeId: betTypes[0].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[0].outcomeNames[0].id,
                  odd: 3.0,
                },
                {
                  name: betTypes[0].outcomeNames[1].id,
                  odd: 3.4,
                },
                {
                  name: betTypes[0].outcomeNames[2].id,
                  odd: 2.1,
                },
              ],
            },
            {
              typeId: betTypes[0].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[1].outcomeNames[0].id,
                  odd: 2.0,
                },
                {
                  name: betTypes[1].outcomeNames[1].id,
                  odd: 2.2,
                },
                {
                  name: betTypes[1].outcomeNames[2].id,
                  odd: 1.8,
                },
              ],
            },
          ],
        },
        {
          bookmakerId: bookmakers[1].id,
          bets: [
            {
              typeId: betTypes[1].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[0].outcomeNames[0].id,
                  odd: 3.3,
                },
                {
                  name: betTypes[0].outcomeNames[1].id,
                  odd: 3.0,
                },
                {
                  name: betTypes[0].outcomeNames[2].id,
                  odd: 1.9,
                },
              ],
            },
            {
              typeId: betTypes[1].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[1].outcomeNames[0].id,
                  odd: 1.8,
                },
                {
                  name: betTypes[1].outcomeNames[1].id,
                  odd: 2.4,
                },
                {
                  name: betTypes[1].outcomeNames[2].id,
                  odd: 1.9,
                },
              ],
            },
          ],
        },
      ],
    } as MatchInputs,
  }))).result;

  assert({
    given: "a post body with a match as inputs and an empty db",
    should: "return a returnMatch",
    actual: isReturnMatch(returnMatch),
    expected: true,
  });

  const retrieved = await db.retrieve("matches", returnMatch?.id as UUIDString);
  assert({
    given: "a body with a match as inputs and an empty db",
    should: "be able to find the match in the db with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with a match as inputs and an empty db",
    should: "return a match with 2 teams",
    actual: returnMatch?.teams?.length,
    expected: teams.length,
  });

  assert({
    given: "a body with a match as inputs and an empty db",
    should: "return a match with 2 odds",
    actual: returnMatch?.odds?.length,
    expected: 2,
  });
});

describe("server: handleUpdateMatch", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const teams = await createMockTeams(db, ["FCB", "BVB"]);
  const teamIds = teams.map((team) => team.id);
  const bookmakers = await createMockBookmakers(db, ["Bwin", "bet365"]);
  const betTypes = await createMockBetTypes(db, ["Winner", "Doppelte Chance"]);

  const res = await insertMatchFromInputs(db)({
    source: "test",
    date: 0
  })({
    teams: [
      {
        teamId: teamIds[0]
      },
      {
        teamId: teamIds[1]
      }
    ],
    startDate: 1609359439000,
    endDate: 1609363039000,
    status: "1H",
    result: [1, 2],
    odds: [
      {
        bookmakerId: bookmakers[0].id,
        bets: []
      }
    ],
    competitionId: generateUUIDString()
  });

  const newCompetitionId = generateUUIDString();
  const updateMatch1 = (await handleUpdateMatch(db)({
    source: "SOURCE2",
    date: 0,
    inputs: {
      startDate: 1609363039000,
      endDate: 1609366639000,        
      odds: [
        {
          bookmakerId: bookmakers[0].id,
          bets: [
            {
              typeId: betTypes[0].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[0].outcomeNames[0].id,
                  odd: 99
                },
                {
                  name: betTypes[0].outcomeNames[1].id,
                  odd: 42
                },
                {
                  name: betTypes[0].outcomeNames[2].id,
                  odd: 1.01
                },
              ]
            }
          ]
        },
      ],
      competitionId: newCompetitionId
    }
  }, {
    id: res.match.id
  })).result;

  const getMatchesResponse = (await handleGetMatches(db)({ source: "SOURCE2" })).result ?? [];
  const match = getMatchesResponse[0];

  assert({
    given: "the get matches response after updating an existing match",
    should: "return only one match",
    actual: getMatchesResponse?.length,
    expected: 1
  });

  assert({
    given: "an overwritten saved match",
    should: "have appended the properties of the match like endDate",
    actual: (await db.retrieve("matches", match.id)).properties.filter((p: Property<any>) => p.name === "endDate").length,
    expected: 2
  });

  assert({
    given: "an overwritten match result",
    should: "take over data that does not exist in the inputs of the updated match like result",
    actual: match?.result,
    expected: [1, 2]
  });

  assert({
    given: "an overwritten saved match",
    should: "still be able to fetch the properties like endDate from the first input",
    actual: ((await handleGetMatches(db)({ source: "test" })).result ?? [])[0].endDate,
    expected: 1609363039000
  });

  assert({
    given: "an overwritten match result",
    should: "have overwritten the normal data like startDate of that match",
    actual: match?.startDate,
    expected: 1609363039000
  });

  await assertOddExists(updateMatch1, 0, 99);

  const updateMatch2 = (await handleUpdateMatch(db)({
    source: "test",
    date: 0,
    inputs: {
      odds: [
        {
          bookmakerId: bookmakers[0].id,
          bets: [
            {
              typeId: betTypes[0].id,
              date: 0,
              outcomes: [
                {
                  name: betTypes[0].outcomeNames[0].id,
                  odd: 42
                },
                {
                  name: betTypes[0].outcomeNames[1].id,
                  odd: 1.01
                },
                {
                  name: betTypes[0].outcomeNames[2].id,
                  odd: 69
                },
              ]
            }
          ]
        }
      ]
    }
  }, {
    id: res.match.id
  })).result;

  await assertOddExists(updateMatch2, 1, 42);

  async function assertOddExists(match: ReturnMatch | null, outcomeIndex: number, expectedOdd: number) {
    assert({
      given: "a post body with a match as inputs and a db with an existing match and its id",
      should: "return a returnMatch",
      actual: isReturnMatch(match),
      expected: true,
    });

    const retrievedMatch: MatchSaveFormat = await db.retrieve("matches", match?.id as UUIDString);
    assert({
      given: "a database with an overwritten match",
      should: "be able to find the match in the db with the returned id",
      actual: retrievedMatch !== null,
      expected: true,
    });

    assert({
      given: "a database with an overwritten match",
      should: "find a match with the overwritten odds",
      actual: retrievedMatch?.oddsIds.length,
      expected: 1,
    });

    const retrievedOdds: OddsSaveFormat = await db.retrieve("odds", retrievedMatch.oddsIds[0]);
    assert({
      given: "a database with an overwritten match",
      should: "be able to find the overwritten odds in the database",
      actual: retrievedOdds !== null,
      expected: true,
    });

    assert({
      given: "a database with an overwritten match",
      should: "find a match with the overwritten bets",
      actual: retrievedOdds?.childIds.length,
      expected: 1,
    });

    const retrievedBet: BetSaveFormat = await db.retrieve("bets", retrievedOdds.childIds[0]);
    assert({
      given: "a database with an overwritten match",
      should: "be able to find the overwritten outcomes",
      actual: retrievedBet.properties[outcomeIndex]?.name,
      expected: "outcomes",
    });

    assert({
      given: "a database with an overwritten match",
      should: "find a match with the overwritten odds",
      actual: retrievedBet.properties[outcomeIndex].value[0].odd,
      expected: expectedOdd,
    });
  }
});

describe("server: handleGetTeams", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const teams = await createMockTeams(db, ["FCB", "BVB"]);

  const actual = (await handleGetTeams(db)()).result;
  assert({
    given: "a test db with teams",
    should: "return an array of same length as teams in DB.",
    actual: actual?.length,
    expected: teams.length,
  });

  assert({
    given: "a test db with teams",
    should: "return an array of ReturnTeams",
    actual: actual?.every(isReturnTeam),
    expected: true,
  });

  assert({
    given: "a test db with teams",
    should: "get ReturnTeams with same ids as inputted",
    actual: actual?.every((t: ReturnTeam) =>
      teams.some((t2) => t2.id === t.id)
    ),
    expected: true,
  });
});

describe("server: handlePostTeam", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostTeam(db)(createPostBody({
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testTeamName"]
      },
    } as TeamInputs,
  }))).result as ReturnTeam;

  const retrieved = await db.retrieve("teams", saved.id);
  assert({
    given: "a body with a name as inputs and an empty db",
    should: "be able to find the team with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with a name as inputs and an empty db",
    should: "create an db entry with the given name",
    actual: retrieved.name,
    expected: saved.name,
  });
});

describe("server: handleUpdateTeam", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostTeam(db)(createPostBody({
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
    } as TeamInputs,
  }))).result as ReturnTeam;

  const updated = (await handleUpdateTeam(db)({
    source: "test",
    date: 0,
    inputs: {
      name: "testName",
      alternativeNames: {
        [generateUUIDString()]: ["testName"]
      },
    } as TeamInputs,
  }, {
    id: saved.id
  })).result as ReturnTeam;

  const retrieved = await db.retrieve("teams", updated.id);
  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "be able to find the team with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a db with one match, the id of that match and a body with a new name as inputs",
    should: "find the updated match in the db",
    actual: retrieved.name,
    expected: updated.name,
  });
});

describe("server: handleGetBookmakers", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bookmakers = await createMockBookmakers(db, ["Bwin", "bet-at-home"]);

  const actual = (await handleGetBookmakers(db)()).result;
  assert({
    given: "a test db with bookmakers",
    should: "return an array of same length as bookmakers in DB.",
    actual: actual?.length,
    expected: bookmakers.length,
  });

  assert({
    given: "a test db with bookmakers",
    should: "return an array of ReturnBookmakers",
    actual: actual?.every(isReturnBookmaker),
    expected: true,
  });

  assert({
    given: "a test db with bookmakers",
    should: "get ReturnBookmakers with same ids as inputted",
    actual: actual?.every((b: ReturnBookmaker) =>
      bookmakers.some((b2) => b2.id === b.id)
    ),
    expected: true,
  });
});

describe("server: handlePostBookmaker", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostBookmaker(db)(createPostBody({
    inputs: {
      name: "testName",
    } as BookmakerInputs,
  }))).result as ReturnBookmaker;

  const retrieved = await db.retrieve("bookmakers", saved.id);
  assert({
    given: "a body with a name as inputs and an empty db",
    should: "be able to find the bookmaker with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with a name as inputs and an empty db",
    should: "create an db entry with the given name",
    actual: retrieved.name,
    expected: saved.name,
  });
});

describe("server: handleGetBetTypes", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const bettypes = await createMockBetTypes(db, ["Winner", "Over"]);

  const actual = (await handleGetBetTypes(db)()).result;
  assert({
    given: "a test db with bettypes",
    should: "return an array of same length as bettypes in DB.",
    actual: actual?.length,
    expected: bettypes.length,
  });

  assert({
    given: "a test db with bettypes",
    should: "return an array of ReturnBetTypes",
    actual: actual?.every(isReturnBetType),
    expected: true,
  });

  assert({
    given: "a test db with bettypes",
    should: "get ReturnBetTypes with same ids as inputted",
    actual: actual?.every((bt: ReturnBetType) =>
      bettypes.some((bt2) => bt2.id === bt.id)
    ),
    expected: true,
  });
});

describe("server: handlePostBetTypes", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const saved = (await handlePostBetType(db)(createPostBody({
    inputs: {
      name: "testName",
      outcomeNames: [
        "1",
        "X",
        "2",
      ],
    } as BetTypeInputs,
  }))).result as ReturnBetType;

  const retrieved = await db.retrieve("betTypes", saved.id);
  assert({
    given: "a body with a name as inputs and an empty db",
    should: "be able to find the bettype with the returned id",
    actual: retrieved !== null,
    expected: true,
  });

  assert({
    given: "a body with a name as inputs and an empty db",
    should: "create an db entry with the given name",
    actual: retrieved.name,
    expected: saved.name,
  });
});

async function createMockMatch(
  db: DB,
  competitionId: UUIDString,
  teamIds: UUIDString[],
  firstHome: boolean,
): Promise<MatchSaveFormat> {
  const matchId = generateUUIDString();

  const matchTeam1: MatchTeamSaveFormat = {
    id: generateUUIDString(),
    name: "FCB",
    teamId: teamIds[firstHome ? 0 : 1],
    properties: [],
    parentId: matchId,
  };
  await db.insert("matchTeams", matchTeam1);

  const matchTeam2: MatchTeamSaveFormat = {
    id: generateUUIDString(),
    name: "BVB",
    teamId: teamIds[firstHome ? 1 : 0],
    properties: [],
    parentId: matchId,
  };
  await db.insert("matchTeams", matchTeam2);

  const match: MatchSaveFormat = {
    id: matchId,
    name: "FCB - BVB",
    status: "NS",
    result: null,
    startDate: 1607855007000,
    properties: [],
    childIds: [matchTeam1.id, matchTeam2.id],
    oddsIds: [],
    competitionId
  };

  await db.insert("matches", match);

  return match;
}
