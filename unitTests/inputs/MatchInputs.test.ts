import { createDB } from "../../scripts/db/db.ts";
import { insertBetTypeFromInputs } from "../../scripts/db/saveFormats/BetTypeSaveFormat.ts";
import { insertBookmakerFromInputs } from "../../scripts/db/saveFormats/BookmakerSaveFormat.ts";
import { retrieveAllCompetitions } from "../../scripts/db/saveFormats/CompetitionSaveFormat.ts";
import { insertTeamFromInputs } from "../../scripts/db/saveFormats/TeamSaveFormat.ts";
import { validateMatchInputs } from "../../scripts/inputs/MatchInputs.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import { createFilledMockDB } from "../db/mockFactories.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";

describe("MatchInputs: validateMatchInputs", async (assert) => {
  const db = await createFilledMockDB();

  const [competition] = await retrieveAllCompetitions(db);
  const competitionId = competition.id;

  const teams = (await Promise.all([
    insertTeamFromInputs(db)({ 
      name: "FCB", 
      alternativeNames: { 
        [generateUUIDString()]: ["FCB"]
      }
    }),
    insertTeamFromInputs(db)({ 
      name: "BVB", 
      alternativeNames: { 
        [generateUUIDString()]: ["BVB"]
      }
    }),
  ])).map(({ id: teamId }) => ({ teamId }));

  const bookmakerId =
    (await insertBookmakerFromInputs(db)({ 
      name: "tipico",
      tax: "NONE"
    })).id;
  const betType = await insertBetTypeFromInputs(db)(
    { name: "Winner", outcomeNames: ["1", "X", "2"] },
  );
  const betTypeId = betType.id;
  const outcomes = [
    {
      name: betType.outcomeNames[0].id,
      odd: 2.1,
    },
    {
      name: betType.outcomeNames[1].id,
      odd: 2.1,
    },
    {
      name: betType.outcomeNames[2].id,
      odd: 2.1,
    },
  ];
  const odds = [
    {
      bookmakerId,
      bets: [
        {
          typeId: betTypeId,
          date: 0,
          outcomes,
        },
      ],
    },
  ];
  await testValidator(db, validateMatchInputs, {
    teams,
    result: [0, 0],
    status: "1H",
    currentMinute: 21,
    startDate: 0,
    endDate: 0,
    odds,
    competitionId
  }, [
    "teams",
    "status",
    "odds",
    "competitionId"
  ], assert);

  assert({
    given: "match inputs of an unstarted match",
    should: "return an successful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: true,
  });

  assert({
    given: "match inputs of an unstarted match 2",
    should: "return an successful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      status: "TBD",
      odds: [],
      competitionId
    })).successful,
    expected: true,
  });

  assert({
    given: "match inputs of an unstarted match with non existant team ids",
    should: "return an successful validation",
    actual: (await validateMatchInputs(db, {
      teams: [
        teams[0],
        {
          teamId: generateUUIDString(),
        },
      ],
      status: "TBD",
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy team",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams: ["a"],
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy team 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams: [{ x: false }],
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy team 3",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams: [{ teamId: "nope" }],
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy result",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: "2:1",
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy result 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: [2, "1"],
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy status",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "NOO",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy currentMinute",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: "21",
      startDate: null,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy startDate",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: -424,
      endDate: null,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy endDate",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: -25,
      odds: [],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy odds",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: "eh..",
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with non-existant bookmakerId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId: generateUUIDString(),
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bookmakerId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId: "nope",
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bookmakerId 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId: null,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bookmakerId 3",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: null,
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: ["false"],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets typeId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: "",
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets typeId 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given:
      "match inputs of an unstarted match with falsy bets: non-existant typeId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: generateUUIDString(),
            date: 0,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets date",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: "not a date",
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy bets date 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            outcomes,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with empty outcomes",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              21,
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: false,
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes (name)",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: "X",
                odd: 2.1,
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given:
      "match inputs of an unstarted match with falsy outcomes (name not in outcomeNames)",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: generateUUIDString(),
                odd: 2.1,
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given:
      "match inputs of an unstarted match with fewer outcomes than bettypes",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes (name) 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                odd: 2.1,
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes (odd)",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: betType.outcomeNames[0].id,
                odd: "2.1",
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              ,
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes (odd) 2",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: betType.outcomeNames[0].id,
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 1.2,
              },
              ,
              {
                name: betType.outcomeNames[2].id,
                odd: 8,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy outcomes (odd < 1)",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: null,
      odds: [{
        bookmakerId,
        bets: [
          {
            typeId: betTypeId,
            date: 0,
            outcomes: [
              {
                name: betType.outcomeNames[0].id,
                odd: 0.8,
              },
              {
                name: betType.outcomeNames[1].id,
                odd: 2.5,
              },
              {
                name: betType.outcomeNames[2].id,
                odd: 1.2,
              },
            ],
          },
        ],
      }],
      competitionId
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with wrong typed competitionId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: 200,
      odds: [],
      competitionId: []
    })).successful,
    expected: false,
  });

  assert({
    given: "match inputs of an unstarted match with falsy competitionId",
    should: "return an unsuccessful validation",
    actual: (await validateMatchInputs(db, {
      teams,
      result: null,
      status: "TBD",
      currentMinute: null,
      startDate: null,
      endDate: 200,
      odds: [],
      competitionId: "sorry but simply not an id"
    })).successful,
    expected: false,
  });
});
