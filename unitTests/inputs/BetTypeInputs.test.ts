import { createDB } from "../../scripts/db/db.ts";
import { validateBetTypeInputs } from "../../scripts/inputs/BetTypeInputs.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";

describe("BetTypeInputs: validateBetTypeInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await testValidator(db, validateBetTypeInputs, {
    name: "Winner",
    outcomeNames: ["one", "two", "three"],
  }, [
    "name",
    "outcomeNames",
  ], assert);

  assert({
    given: "inputs with wrong typed name",
    should: "return an unsuccessful validation",
    actual: (await validateBetTypeInputs(db, {
      name: 21,
      outcomeNames: ["one"],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed outcomeNames",
    should: "return an unsuccessful validation",
    actual: (await validateBetTypeInputs(db, {
      name: "21",
      outcomeNames: true,
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed element of outcomeNames",
    should: "return an unsuccessful validation",
    actual: (await validateBetTypeInputs(db, {
      name: "21",
      outcomeNames: ["hi", true],
    })).successful,
    expected: false,
  });
});
