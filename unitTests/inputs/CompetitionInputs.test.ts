import { createDB } from "../../scripts/db/db.ts";
import { validateCompetitionInputs } from "../../scripts/inputs/CompetitionInputs.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";

describe("CompetitionInputs: validateCompetitionInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await testValidator(db, validateCompetitionInputs, {
    name: "Winner",
    alternativeNames: {
      [generateUUIDString()]: ["Winner"]
    },
    sportId: generateUUIDString(),
    countryId: generateUUIDString()
  }, [
    "name"
  ], assert);

  assert({
    given: "inputs with wrong typed name",
    should: "return an unsuccessful validation",
    actual: (await validateCompetitionInputs(db, {
      name: 21,
      alternativeNames: {
        [generateUUIDString()]: ["Winner"]
      },
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with null alternativeNames",
    should: "return an unsuccessful validation",
    actual: (await validateCompetitionInputs(db, {
      name: "test",
      alternativeNames: null,
      sportId: generateUUIDString(),
      countryId: generateUUIDString()
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed alternativeNames",
    should: "return an unsuccessful validation",
    actual: (await validateCompetitionInputs(db, {
      name: "test",
    alternativeNames: {
      [generateUUIDString()]: ["1", 24]
    },
    sportId: generateUUIDString(),
    countryId: generateUUIDString()
    })).successful,
    expected: false,
  });
  
  assert({
    given: "inputs with wrong typed sportId",
    should: "return an unsuccessful validation",
    actual: (await validateCompetitionInputs(db, {
      name: "test",
      alternativeNames: {
        [generateUUIDString()]: ["1"]
      },
      sportId: {},
      countryId: generateUUIDString()
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed countryId",
    should: "return an unsuccessful validation",
    actual: (await validateCompetitionInputs(db, {
      name: "test",
      alternativeNames: {
        [generateUUIDString()]: ["1"]
      },
      sportId: generateUUIDString(),
      countryId: []
    })).successful,
    expected: false,
  });
});