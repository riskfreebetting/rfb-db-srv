import { createDB } from "../../scripts/db/db.ts";
import { validateBookmakerInputs } from "../../scripts/inputs/BookmakerInputs.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";

describe("BookmakerInputs: validateBookmakerInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await testValidator(db, validateBookmakerInputs, {
    name: "Winner",
    tax: "5% WINNINGS"
  }, [
    "name",
    "tax"
  ], assert);

  assert({
    given: "inputs with wrong typed name",
    should: "return an unsuccessful validation",
    actual: (await validateBookmakerInputs(db, {
      name: 21,
      tax: "NONE"
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed tax",
    should: "return an unsuccussful validation",
    actual: (await validateBookmakerInputs(db, {
      name: "test",
      tax: 0.05
    })).successful,
    expected: false
  })
});
