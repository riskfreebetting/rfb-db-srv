import { createDB } from "../../scripts/db/db.ts";
import { validateSportInputs } from "../../scripts/inputs/SportInputs.ts";
import { generateUUIDString } from "../../scripts/utils.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";

describe("SportInputs: validateSportInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  await testValidator(db, validateSportInputs, {
    name: "Winner",
    alternativeNames: {
      [generateUUIDString()]: ["Winner"]
    }
  }, [
    "name"
  ], assert);

  assert({
    given: "inputs with wrong typed name",
    should: "return an unsuccessful validation",
    actual: (await validateSportInputs(db, {
      name: 21,
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with null alternativeNames",
    should: "return an unsuccessful validation",
    actual: (await validateSportInputs(db, {
      name: "test",
      alternativeNames: null
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed alternativeNames",
    should: "return an unsuccessful validation",
    actual: (await validateSportInputs(db, {
      name: "test",
      alternativeNames: {
        [generateUUIDString()]: ["1", 24]
      }
    })).successful,
    expected: false,
  });
});
