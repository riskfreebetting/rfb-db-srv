import { validateTypes } from "../../scripts/inputs/Validators.ts";
import describe from "../describe.ts";

describe("Validator: validateTypes", (assert) => {
  const validators = {
    a: {
      type: "{ x: boolean }",
      fn: (x: any) => typeof x?.x === "boolean",
    },
    b: {
      type: "string",
      fn: (x: any) => typeof x === "string",
    },
    c: {
      type: "number[]",
      fn: (x: any) =>
        Array.isArray(x) && x.every((e: any) => typeof e === "number"),
    },
  };

  assert({
    given:
      "validators for properties 'a', 'b' and 'c' and a object of a, b and c",
    should: "return an empty array",
    actual: validateTypes("test", {
      a: {
        x: true,
      },
      b: "test",
      c: [1, 2, 3],
    }, validators),
    expected: [],
  });

  const actual = validateTypes("test", {
    a: {
      x: true,
    },
    b: null,
    c: [1, 2, 3],
  }, validators);
  assert({
    given:
      "validators for properties 'a', 'b' and 'c' and a falsy object of a, b and c",
    should: "return an UnalignableTypeError",
    actual: actual[0].code,
    expected: "E002",
  });

  const actual2 = validateTypes("test", {
    a: {
      x: true,
    },
    c: [1, 2, 3],
  }, validators);
  assert({
    given:
      "validators for properties 'a', 'b' and 'c' and a falsy object of a and c",
    should: "return an UnalignableTypeError",
    actual: actual2[0].code,
    expected: "E002",
  });
});
