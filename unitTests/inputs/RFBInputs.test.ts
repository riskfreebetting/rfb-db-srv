import { DB, createDB } from "../../scripts/db/db.ts";
import { validatePostRFBInputs } from "../../scripts/inputs/RFBInputs.ts";
import describe from "../describe.ts";
import { testValidator } from "../testUtils.ts";
import { generateUUIDString } from "../../scripts/utils.ts";

describe("RFBInputs: validatePostRFBInputs", async (assert) => {
  const db = await createDB({ id: "testDB" });

  const matchId = generateUUIDString();
  await db.insert("matches", { 
    id: matchId
  });
  
  const outcomeId = generateUUIDString();
  const betTypeId = generateUUIDString();
  await db.insert("betTypes", { 
    id: betTypeId,
    outcomeNames: [{
      id: outcomeId,
      name: "only bettype existing"
    }]
  });
  
  const bookmakerId = generateUUIDString();
  await db.insert("bookmakers", { 
    id: bookmakerId
  });

  await testValidator(db, validatePostRFBInputs, {
    matchId,
    betTypeId,
    bets: [],
  }, [
    "matchId",
    "betTypeId",
    "bets"
  ], assert);

  assert({
    given: "inputs with wrong typed matchId",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId: 21,
      betTypeId,
      bets: [],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed betTypeId",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId: "ABC",
      bets: [],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bets",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: null,
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed profit)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: "2.1",
        outcomes: {}
      }],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed outcomes)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: 0.05,
        outcomes: {}
      }, {
        profit: "2.1",
        outcomes: "ahahha"
      }],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed outcomes: outcomeId)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: 2
          }
        }
      }, {
        profit: 0.05,
        outcomes: {
          "nope": {
            bookmaker: bookmakerId,
            odd: 2
          }
        }
      }],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed outcomes: bookmaker)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: 2
          }
        }
      }, {
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: "THIs Is not an ID",
            odd: 2
          }
        }
      }],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed outcomes: odd wrong typed)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: 2
          }
        }
      }, {
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: []
          }
        }
      }],
    })).successful,
    expected: false,
  });

  assert({
    given: "inputs with wrong typed bet (wrong typed outcomes: odd >= 1)",
    should: "return an unsuccessful validation",
    actual: (await validatePostRFBInputs(db, {
      matchId,
      betTypeId,
      bets: [{
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: 2
          }
        }
      }, {
        profit: 0.05,
        outcomes: {
          [outcomeId]: {
            bookmaker: bookmakerId,
            odd: 0.05
          }
        }
      }],
    })).successful,
    expected: false,
  });
});
