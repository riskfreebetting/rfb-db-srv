import { DB } from "./db/db.ts";
import { createReturnObject, createUnalignableTypeError, createUnalignableTypeOfValueError, MessageWithInfo, ReturnObject } from "./factories/ReturnObject.ts";
import { DateNumber, isDateNumber } from "./utils.ts";

export interface PostBody {
  source: string;
  date: DateNumber;
  inputs: any;
}

export const isPostBody = (o: any): o is PostBody => (
  typeof o?.source === "string" &&
  isDateNumber(o?.date) &&
  o?.inputs !== null && typeof o?.inputs === "object"
);

export const createPostBody = ({
  source = "",
  date = 0,
  inputs,
}: {
  source?: string;
  date?: DateNumber;
  inputs: any;
}): PostBody => ({
  source,
  date,
  inputs,
});

export const validatePostBody = (body: any): ReturnObject<PostBody> => {
  const messages = [];

  if (body?.source != undefined && typeof body?.source !== "string")
    messages.push(createUnalignableTypeError({
      identifier: "body",
      propertyName: "source",
      actualType: typeof body?.source,
      expectedType: "string"
    }));

  if (body?.date != undefined && !isDateNumber(body?.date))
    messages.push(createUnalignableTypeError({
      identifier: "body",
      propertyName: "date",
      actualType: typeof body?.date,
      expectedType: "Date in ms"
    }));

  if (body.inputs == null)
    messages.push(createUnalignableTypeError({
      identifier: "body",
      propertyName: "inputs",
      actualType: typeof body?.inputs,
      expectedType: "not undefined or null"
    }));  

  return createReturnObject({
    result: body as PostBody,
    messages
  });
};

export interface PropertyFilter {
  source?: string;
  beforeDate?: DateNumber;
  afterDate?: DateNumber;
}

export const validateGetParams = (
  params: Record<string, string>
) => {
  const messages = [];

  if (params.beforeDate != undefined && Number.isNaN(parseInt(params.beforeDate)))
    messages.push(createUnalignableTypeOfValueError({
      identifier: "params",
      propertyName: "beforeDate",
      value: params?.beforeDate,
      expectedType: "integer"
    }));
      
  if (params.afterDate != undefined && Number.isNaN(parseInt(params.afterDate)))
    messages.push(createUnalignableTypeOfValueError({
      identifier: "params",
      propertyName: "afterDate",
      value: params?.beforeDate,
      expectedType: "integer"
    }));

  // No need to test source because it will always be a string.

  return createReturnObject({
    result: params,
    messages
  });
};