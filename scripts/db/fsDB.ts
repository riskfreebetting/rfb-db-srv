import {
  emptyDir,
  ensureDir,
  exists,
  walk,
  WalkEntry,
} from "https://deno.land/std@0.93.0/fs/mod.ts";
import { CONFIG } from "../config.ts"
import { asyncPipe, pipe, trace } from "../utils.ts";

import { createDB, DB, DBCreationInputs } from "./db.ts";

export interface FsDB extends DB {
  path: string;
}

const PATH_DELIMITER = CONFIG.SYS === "WINDOWS" ? "\\" : "/";

export const createAllFsDBs = async (): Promise<Record<string, DB>> => {
  const ids = await findAllDBIds();
  const dbArray = await Promise.all(ids.map(id => createFsDB({ id })));
  
  return dbArray.reduce((accDB, db) => ({
    ...accDB,
    [db.id]: db
  }), {});
};

async function findAllDBIds(): Promise<string[]> {
  const path = CONFIG.DB_PATH;

  const entryNames: string[] = [];
  let i = 0;
  for await (const entry of walk(path, { maxDepth: 1 })) {
    if (i > 0 && entry.isDirectory)
      entryNames.push(entry.name);

    i++;
  }

  return entryNames;
}

export const createFsDB = async (
  params: DBCreationInputs = {},
): Promise<FsDB> => {
  const db = await createDB(params);  

  const path = getPathOfDatabase(db.id); 
  await loadDB(db)(path);

  const that: FsDB = {
    ...db,
    path,
    retrieve(type, id) {
      return db.retrieve(type, id);
    },
    retrieveAll(type) {
      return db.retrieveAll(type);
    },
    async insert(type: string, o: any) {
      if (!db.isDeleted && typeof o.id === "string") {
        await that.insertType(type);
        await Deno.writeTextFile(
          `${path}/${type}/${o.id}.json`,
          JSON.stringify(o),
        );
      }

      await db.insert(type, o);
      
      return that;
    },
    async insertType(type: string) {
      if (!db.isDeleted) {
        await ensureDir(`${path}/${type}`);
      }

      await db.insertType(type);

      return that;
    },
    async delete(type: string, id: string) {
      const entryPath = `${path}/${type}/${id}.json`;
      if (!db.isDeleted && await exists(entryPath)) {
        await Deno.remove(entryPath);
      }

      return db.delete(type, id);
    },
    async deleteDB() {
      if (!db.isDeleted) {
        await emptyDir(path);
        await Deno.remove(path);
      }

      return db.deleteDB();
    },
  };

  return that;
};

export const getPathOfDatabase = (id="testDB") => CONFIG.DB_PATH.concat("/" + id );

function loadDB(db: DB) {
  return asyncPipe<DB, string>(
    ensureDirectory,
    findFilesInDB,
    loadExistingFilesIntoDB(db),
  );
}

async function ensureDirectory(path: string) {
  await ensureDir(path);

  return path;
}

type DBEntry = {type: string, content: object};
async function findFilesInDB(path: string) {
  let files: DBEntry[] = [];

  for await (const entry of walk(path)) {
    if (entry.isFile) {
      files.push({
        type: getType(entry),
        content: await getContent(entry)
      });
    }
  }

  return files;

  function getType(entry: WalkEntry) {
    return pipe<string, string>(
      navigateUp(),
      getFolderNameFromPath
    )(entry.path);
  }

  function getFolderNameFromPath(path: string) {
    const arr = path.split(PATH_DELIMITER);
    return arr[arr.length - 1];
  }

  async function getContent(entry: WalkEntry) {
    try {
      return await asyncPipe<object, string>(
        Deno.readTextFile,
        JSON.parse
      )(entry.path);
    } catch (e) {
      console.log("Falsy db file: ", entry.path);
      console.error(e);

      return {};
    }
  }
}

function loadExistingFilesIntoDB(db: DB) {
  const loadFileIntoDB = (entry: DBEntry) => db.insert(entry.type, entry.content);
  
  return (files: DBEntry[]) => Promise.all(
    files.map(loadFileIntoDB)
  );
}

function navigateUp(i=0) {
  return (path="") => path
    .split(PATH_DELIMITER)
    .slice(0, -i - 1)
    .join(PATH_DELIMITER);
}