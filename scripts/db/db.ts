import { CONFIG } from "../config.ts"

export interface DB {
  id: string;
  isDeleted: boolean;

  retrieve: (type: string, id: string) => Promise<any>;
  retrieveAll: (type: string) => Promise<any>;
  insert: (type: string, o: any) => Promise<DB>;
  insertType: (type: string) => Promise<DB>;
  delete: (type: string, id: string) => Promise<DB>;
  deleteDB: () => Promise<void>;

  log: () => Promise<DB>;
}

export const isDB = (o: any): o is DB => (
  typeof o?.id === "string" &&
  typeof o?.retrieve === "function" &&
  typeof o?.retrieveAll === "function" &&
  typeof o?.insert === "function" &&
  typeof o?.delete === "function" &&
  typeof o?.deleteDB === "function"
);

export interface DBCreationInputs {
  id?: string;
}

export const createDB = async (
  { id = CONFIG.DB_NAME }: DBCreationInputs = {},
): Promise<DB> => {
  const data: Record<string, Record<string, any>> = {};

  let isDeleted: boolean = false;
  const that: DB = {
    get id(): string {
      return id;
    },
    get isDeleted(): boolean {
      return isDeleted;
    },
    async retrieve<T>(type: string, id: string): Promise<T | null> {
      if (isDeleted) {
        throw new DBDeletedError(id);
      }

      if (data[type] && data[type][id] != null) {
        return data[type][id];
      } else {
        return null;
      }
    },
    async retrieveAll<T>(type: string): Promise<T[]> {
      const entriesOfType = data[type];
      if (entriesOfType) {
        return Object.values(entriesOfType).reduce(
          (acc: any[], curr: any) => acc.concat(curr),
          [],
        );
      } else {
        return [];
      }
    },
    async insert(type: string, o: any): Promise<DB> {
      if (isDeleted) {
        throw new DBDeletedError(id);
      }

      if (!data[type]) {
        await this.insertType(type);
      }

      if (typeof o.id === "string") {
        data[type][o.id] = JSON.parse(JSON.stringify(o));
      } else {
        throw new NoGivenIdError();
      }

      return this;
    },
    async insertType(type: string): Promise<DB> {
      if (isDeleted) {
        throw new DBDeletedError(id);
      }

      if (!data[type]) {
        data[type] = {};
      }

      return this;
    },
    async delete(type: string, id: string): Promise<DB> {
      if (isDeleted) {
        throw new DBDeletedError(id);
      }

      if (data[type] && data[type][id] != null) {
        delete data[type][id];
      } else {
        throw new NotFoundError(type, id);
      }
      return this;
    },
    async deleteDB(): Promise<void> {
      isDeleted = true;

      Object.keys(data).forEach((type) => delete data[type]);
    },
    async log(): Promise<DB> {
      console.log(id, JSON.stringify(data));

      return this;
    },
  };

  return that;
};

export class NoGivenIdError extends Error {
  constructor() {
    super(
      "NoGivenIdError: Tried to input an element without an string-id into the database.",
    );
  }
}

export class NotFoundError extends Error {
  constructor(type: string, id: string) {
    super(
      `NotFoundError: Could not find element of type ${type} with id ${id}.`,
    );
  }
}

export class DBDeletedError extends Error {
  constructor(id: string) {
    super(
      `DBDeletedError: The DB with the id ${id} you are trying to access was deleted.`,
    );
  }
}
