import { BetTypeInputs } from "../../inputs/BetTypeInputs.ts";
import {
  generateUUIDString,
  isUUIDString,
  Property,
  UUIDString,
} from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "betTypes";

export interface BetTypeSaveFormat extends SaveFormat {
  outcomeNames: OutcomeNameSaveFormat[];
}

export interface OutcomeNameSaveFormat {
  id: UUIDString;
  name: string;
}

export const isBetTypeSaveFormat = (o: any): o is BetTypeSaveFormat => (
  Array.isArray(o?.outcomeNames) &&
  o?.outcomeNames.every(isOutcomeNameSaveFormat) &&
  isSaveFormat(o)
);

export const isOutcomeNameSaveFormat = (o: any): o is OutcomeNameSaveFormat => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string"
);

export const insertBetTypeFromInputs = (db: DB) =>
  async (inputs: BetTypeInputs): Promise<BetTypeSaveFormat> => {
    const betTypeSaveFormat = createBetTypeSaveFormatFromInputs(inputs);

    await insertBetType(db)(betTypeSaveFormat);

    return betTypeSaveFormat;
  };

export const createBetTypeSaveFormatFromInputs = (
  inputs: BetTypeInputs,
): BetTypeSaveFormat => {
  const outcomeNames = createOutcomeNamesObjectFromInputs(inputs.outcomeNames);

  return createBetTypeSaveFormat({
    id: generateUUIDString(),
    name: inputs.name,
    outcomeNames,
  });
};

export const createOutcomeNamesObjectFromInputs = (names: string[]) =>
  names.map(
    (name) =>
      createOutcomeNameSaveFormat({
        name,
        id: generateUUIDString(),
      }),
  );

export const createOutcomeNameSaveFormat = ({
  name,
  id,
}: {
  name: string;
  id: UUIDString;
}): OutcomeNameSaveFormat => ({
  name,
  id,
});

export const createBetTypeSaveFormat = ({
  id,
  name,
  outcomeNames = [],
  properties = [],
}: {
  id: UUIDString;
  name: string;
  outcomeNames?: OutcomeNameSaveFormat[];
  properties?: Property<any>[];
}): BetTypeSaveFormat => ({
  id,
  name,
  outcomeNames,
  properties,
});

export const insertBetType = (db: DB) =>
  (betType: BetTypeSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, betType)
  );

export const retrieveBetTypeById = (db: DB) =>
  (id: UUIDString): Promise<BetTypeSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
export const retrieveAllBetTypes = (db: DB): Promise<BetTypeSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);
