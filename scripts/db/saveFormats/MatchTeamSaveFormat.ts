import { MatchTeamInputs } from "../../inputs/MatchInputs.ts";
import { generateUUIDString, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { SaveFormat } from "./SaveFormat.ts";
import { retrieveTeamById } from "./TeamSaveFormat.ts";

const DB_TYPE = "matchTeams";

export interface MatchTeamSaveFormat extends SaveFormat {
  teamId: UUIDString;
  parentId: UUIDString;
}

export const insertMatchTeamsNotCreatedYet = (db: DB) => 
  async (
    teamInputs: MatchTeamInputs[],
    existingMatchTeamIds: UUIDString[],
    matchId: UUIDString
  ): Promise<MatchTeamSaveFormat[]> => {
    const existingMatchTeams = await retrieveSeveralMatchTeamsById(db)(existingMatchTeamIds);
  
    return teamInputs
      ? insertMatchTeamsNotCreatedYet(db, teamInputs, existingMatchTeams, matchId)
      : existingMatchTeams;

    async function insertMatchTeamsNotCreatedYet(
      db: DB, 
      teamInputs: MatchTeamInputs[], 
      existingMatchTeams: MatchTeamSaveFormat[], 
      matchId: UUIDString
    ) {
      const doMatchTeamsNeedToBeCreated = teamInputs.map(doesMatchTeamExist(existingMatchTeams));

      const newMatchTeamInputs = doMatchTeamsNeedToBeCreated.reduce(
        addTeamInputsIfNeedsToBeCreated(teamInputs), []
      );

      const createdMatchTeamsSaveFormat = await insertMatchTeamsFromInputs(db)(newMatchTeamInputs, matchId);

      return doMatchTeamsNeedToBeCreated.map((isNewlyCreated, idx) => (
        isNewlyCreated
          ? createdMatchTeamsSaveFormat[idx]
          : existingMatchTeams[idx]
      ));
    }

    function doesMatchTeamExist(existingMatchTeams: MatchTeamSaveFormat[]) {
      return (team: MatchTeamInputs, idx: number): boolean => (
        team.teamId !== existingMatchTeams[idx].teamId
      );
    }

    function addTeamInputsIfNeedsToBeCreated(teamInputs: MatchTeamInputs[]) {
      return (acc: MatchTeamInputs[], needsToBeCreated: boolean, idx: number): MatchTeamInputs[] => (
        needsToBeCreated
          ? acc.concat(teamInputs[idx])
          : acc
      );
    }
  };

export const insertMatchTeamsFromInputs = (db: DB) =>
  async (
    matchTeams: MatchTeamInputs[],
    matchId: UUIDString,
  ): Promise<MatchTeamSaveFormat[]> => (
    Promise.all(matchTeams.map(insertMatchTeamFromInputs(db, matchId)))
  );

export const insertMatchTeamFromInputs = (db: DB, matchId: UUIDString) =>
  async (matchTeam: MatchTeamInputs): Promise<MatchTeamSaveFormat> => {
    // TODO: What if team is null?
    const team = await retrieveTeamById(db)(matchTeam.teamId);

    const savedMatchTeam: MatchTeamSaveFormat = {
      id: generateUUIDString(),
      name: team.name,
      parentId: matchId,
      teamId: matchTeam.teamId,
      properties: [],
    };

    await insertMatchTeam(db)(savedMatchTeam);

    return savedMatchTeam;
  };

export const insertMatchTeam = (db: DB) =>
  async (matchTeam: MatchTeamSaveFormat) => (
    db.insert(DB_TYPE, matchTeam)
  );

export const retrieveSeveralMatchTeamsById = (db: DB) => 
  async (ids: UUIDString[]): Promise<MatchTeamSaveFormat[]> => Promise.all(
    ids.map(retrieveMatchTeamById(db))
  );

export const retrieveMatchTeamById = (db: DB) =>
  async (id: UUIDString): Promise<MatchTeamSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
