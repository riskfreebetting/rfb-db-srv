import { RFBInputs, RFBBet as RFBInputBet } from "../../inputs/RFBInputs.ts";
import {
  generateUUIDString,
  isUUIDString,
  UUIDString,
  splitIntoArrays
} from "../../utils.ts";
import { DB } from "../db.ts";

const DB_TYPE = "rfbs";

export interface RFBSaveFormat {
  id: UUIDString;
  isAvailable: boolean;
  matchId: UUIDString;
  betTypeId: UUIDString;
  bets: RFBBetSaveFormat[];
}

export interface RFBBetSaveFormat {
  profit: number;
  isAvailable: boolean;
  outcomes: Record<UUIDString, RFBOutcome>;
}

interface RFBOutcome {
  bookmaker: UUIDString;
  odd: number;
}

export const isRFBSaveFormat = (o: any): o is RFBSaveFormat => (
  isUUIDString(o?.id) &&
  isUUIDString(o?.matchId) &&
  isUUIDString(o?.betTypeId) &&
  typeof o?.isAvailable === "boolean" &&
  Array.isArray(o?.bets) && o?.bets?.every((bet: any) => (
    typeof bet?.profit === "number" &&
    typeof bet?.isAvailable === "boolean" &&
    typeof bet?.outcomes === "object" &&
    Object.keys(bet?.outcomes).every(isUUIDString) &&
    Object.values(bet?.outcomes).every((oc: any) => (
      isUUIDString(oc?.bookmaker) &&
      typeof oc?.odd === "number"
    ))
  ))
);

export const updateRFB = (db: DB) => 
  async (id: UUIDString, inputs: RFBInputs): Promise<RFBSaveFormat> => {
    const existingRFB = await retrieveRFBById(db)(id);

    const betUpdates = updateBets(existingRFB.bets, inputs?.bets);

    const isRFBAvailable = betUpdates.some(isBetAvailable);

    const updatedRFB = createRFBFromInputs({
      id,
      rfb: {
        matchId: existingRFB.matchId,
        betTypeId: existingRFB.betTypeId,
        isAvailable: isRFBAvailable,
        bets: betUpdates
      }
    });

    const availableBefore = existingRFB.bets.filter(isBetAvailable).length;
    const availableNow = existingRFB.bets.filter(isBetAvailable).length;
    console.log(`Updated RFB: Before ${availableBefore}; Now: ${availableNow}`);

    await insertRFB(db)(updatedRFB);

    return updatedRFB;
  };

function updateBets(existingBets: RFBBetSaveFormat[], updates: RFBInputBet[]=[]) {
  const [betUpdates, newBets] = splitIntoArrays(doesBetExist(existingBets), updates);

  return updateAvailabilityOfExistingBets(existingBets, betUpdates)
    .concat(newBets.map(setAvailable(true)));
}


function updateAvailabilityOfExistingBets(existingBets: RFBBetSaveFormat[], betUpdates: RFBInputBet[]) {
  return existingBets.map(bet => {
    if (doesBetExist(betUpdates)(bet))
      return setAvailable(true)(bet);
    else
      return setAvailable(false)(bet);
  });
}

function doesBetExist(existingBets: (RFBBetSaveFormat | RFBInputBet)[]) {
  return (betUpdate: (RFBInputBet | RFBBetSaveFormat)) => {
    const updateOutcomeIds = Object.keys(betUpdate.outcomes);
    
    return existingBets.some(existingBet => {
      const outcomeIds = Object.keys(existingBet.outcomes);
      
      return outcomeIds.every((id, i) => updateOutcomeIds[i] === id) && (
        Object.keys(existingBet.outcomes).every(outcomeId => {
          const outcome = existingBet.outcomes[outcomeId];
          const outcomeUpdate = betUpdate.outcomes[outcomeId];

          return (
            outcome.bookmaker === outcomeUpdate?.bookmaker &&
            outcome.odd === outcomeUpdate?.odd
          );
        })
      );
    });
  };
}

function isBetAvailable(bet: RFBBetSaveFormat) {
  return bet.isAvailable;
}

export const insertRFBFromInputs = (db: DB) =>
  async (rfbInputs: RFBInputs) => {
    const id = generateUUIDString();

    return insertRFBFromInputsAndGivenId(db)(
      id,
      rfbInputs,
    );
  };

export const insertRFBFromInputsAndGivenId = (db: DB) => 
  async (id: UUIDString, rfbInputs: RFBInputs): Promise<RFBSaveFormat> => {
    const rfb = createRFBFromInputs({
      id,
      rfb: setRFBAvailable(true)(rfbInputs),
    });

    await insertRFB(db)(rfb);

    return rfb;
  };

function setRFBAvailable(isAvailable: boolean) {
  return (rfb: RFBInputs) => ({
    ...rfb,
    isAvailable,
    bets: rfb.bets.map(setAvailable(isAvailable))
  });
}

function setAvailable(isAvailable: boolean) {
  return (rfb: RFBInputBet) => ({
    ...rfb,
    isAvailable
  })
}


export const createRFBFromInputs = ({
    id,
    rfb,
  }: {
    id: UUIDString;
    rfb: {
      matchId: UUIDString;
      isAvailable: boolean;
      betTypeId: UUIDString;
      bets: {
        profit: number;
        isAvailable: boolean;
        outcomes: Record<UUIDString, {
          bookmaker: UUIDString;
          odd: number;
        }>
      }[]
    };
  }): RFBSaveFormat => {

    return createRFBSaveFormat({
      id,
      isAvailable: rfb.isAvailable,
      matchId: rfb.matchId,
      betTypeId: rfb.betTypeId,
      bets: rfb.bets
    });
  };

export const createRFBSaveFormat = ({
  id,
  isAvailable,
  matchId,
  betTypeId,
  bets=[]
}: {
  id: UUIDString;
  isAvailable: boolean;
  matchId: UUIDString;
  betTypeId: UUIDString;
  bets?: {
    profit: number;
    isAvailable: boolean;
    outcomes: Record<UUIDString, {
      bookmaker: UUIDString;
      odd: number;
    }>
  }[]
}): RFBSaveFormat => ({
  id,
  matchId,
  isAvailable,
  betTypeId,
  bets
});

export const insertRFB = (db: DB) =>
  (rfb: RFBSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, rfb)
  );

export const retrieveRFBById = (db: DB) =>
  (id: UUIDString): Promise<RFBSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );

export const retrieveAllRFBs = (db: DB): Promise<RFBSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);