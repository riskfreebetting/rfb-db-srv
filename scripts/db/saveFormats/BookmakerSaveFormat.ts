import { BookmakerInputs } from "../../inputs/BookmakerInputs.ts";
import { generateUUIDString, Property, UUIDString, trace } from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "bookmakers";

export interface BookmakerSaveFormat extends SaveFormat {
  tax: keyof typeof TaxTypes;
}

export const isBookmakerSaveFormat = (o: any): o is BookmakerSaveFormat => (
  o?.tax in TaxTypes &&
  isSaveFormat(o)
);

export const insertBookmakerFromInputs = (db: DB) =>
  async (inputs: BookmakerInputs): Promise<BookmakerSaveFormat> => {
    const bookmakerSaveFormat = createBookmakerSaveFormatFromInputs(inputs);

    await insertBookmaker(db)(bookmakerSaveFormat);

    return bookmakerSaveFormat;
  };

export const createBookmakerSaveFormatFromInputs = (
  inputs: BookmakerInputs,
): BookmakerSaveFormat => {
  return createBookmakerSaveFormat({
    id: generateUUIDString(),
    name: inputs.name,
    tax: inputs.tax
  });
};

export const createBookmakerSaveFormat = ({
  id,
  name,
  tax,
  properties = [],
}: {
  id: UUIDString;
  name: string;
  tax: keyof typeof TaxTypes;
  properties?: Property<any>[];
}): BookmakerSaveFormat => ({
  id,
  name,
  tax,
  properties,
});

export const insertBookmaker = (db: DB) =>
  (bookmaker: BookmakerSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, bookmaker)
  );

export const retrieveBookmakerById = (db: DB) =>
  (id: UUIDString): Promise<BookmakerSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
export const retrieveAllBookmakers = (
  db: DB,
): Promise<BookmakerSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);

export enum TaxTypes {
  "NONE",
  "5% STAKES",
  "5% WINNINGS",
};