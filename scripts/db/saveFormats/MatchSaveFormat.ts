import { MatchInputs } from "../../inputs/MatchInputs.ts";
import {
  createProperty,
  DateNumber,
  generateUUIDString,
  isDateNumber,
  isUUIDString,
  Property,
  UUIDString,
  getDefaultValue,
  trace
} from "../../utils.ts";
import { DB } from "../db.ts";
import { BetSaveFormat } from "./BetSaveFormat.ts";
import {
  insertMatchTeamsFromInputs,
  insertMatchTeamsNotCreatedYet,
  MatchTeamSaveFormat,
} from "./MatchTeamSaveFormat.ts";
import {
  insertOddsNotCreatedYet,
  insertSeveralOddsFromInputs,
  OddsSaveFormat,
} from "./OddsSaveFormat.ts";
import { isSaveFormat, PropertyData, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "matches";

export type PropertyTypes = DateNumber | number; //endDate, currentMinute

export interface MatchSaveFormat extends SaveFormat {
  status: keyof typeof SaveMatchStatus;
  result: number[] | null;
  startDate: DateNumber | null;
  childIds: UUIDString[];
  oddsIds: UUIDString[];
  competitionId: UUIDString;

  properties: Property<PropertyTypes>[];
}

export const isMatchSaveFormat = (o: any): o is SaveFormat => (
  o?.status in SaveMatchStatus &&
  (
    Array.isArray(o.result) && o.result.every((e: any) => typeof e === "number") || 
    o.result === null
  ) &&
  (isDateNumber(o?.startDate) || o?.startDate === null) &&
  Array.isArray(o?.childIds) && o?.childIds.every(isUUIDString) &&
  Array.isArray(o?.oddsIds) && o?.oddsIds.every(isUUIDString) &&
  isUUIDString(o?.competitionId) &&
  isSaveFormat(o)
);

export const insertMatchFromInputs = (db: DB) =>
  (propertyData: PropertyData) =>
    async (matchInputs: MatchInputs) => {
      const id = generateUUIDString();

      return insertMatchFromInputsAndGivenId(db)(propertyData)(
        id,
        matchInputs,
      );
    };

export const updateMatch = (db: DB) => 
  (propertyData: PropertyData) => 
    async (id: UUIDString, matchInputs: MatchInputs): Promise<{
      match: MatchSaveFormat;
      matchTeams: MatchTeamSaveFormat[];
      odds: OddsSaveFormat[];
      bets: BetSaveFormat[];
    }> => {
      const existingMatch = await retrieveMatchById(db)(id);
      
      const newMatchTeams = await insertMatchTeamsNotCreatedYet(db)(matchInputs.teams, existingMatch.childIds, id);
      const newOddsAndBets = await insertOddsNotCreatedYet(db)(propertyData)(matchInputs.odds, existingMatch.oddsIds, id);

      const updates = createMatchFromInputs(propertyData)({
        id,
        match: {
          result: getDefaultValue(matchInputs.result, existingMatch.result),
          status: getDefaultValue(matchInputs.status, existingMatch.status),
          startDate: getDefaultValue(matchInputs.startDate, existingMatch.startDate),
          endDate: matchInputs.endDate,
          currentMinute: matchInputs.currentMinute,
          competitionId: getDefaultValue(matchInputs.competitionId, existingMatch.competitionId)
        },
        matchTeams: newMatchTeams,
        oddsIds: newOddsAndBets.map(({ odds }) => odds.id),
      });

      const newMatch = {
        ...updates, 
        properties: existingMatch.properties.concat(updates.properties)
      };
      await insertMatch(db)(newMatch);

      return {
        match: newMatch,
        matchTeams: newMatchTeams,
        odds: newOddsAndBets.map(({ odds }) => odds),
        bets: newOddsAndBets.flatMap(({ bets }) => bets),
      };
    };

export const insertMatchFromInputsAndGivenId = (db: DB) => {
  return (propertyData: PropertyData) =>
    async (id: UUIDString, matchInputs: MatchInputs): Promise<{
      match: MatchSaveFormat;
      matchTeams: MatchTeamSaveFormat[];
      odds: OddsSaveFormat[];
      bets: BetSaveFormat[];
    }> => {
      const matchTeams = await insertMatchTeamsFromInputs(db)(
        matchInputs.teams,
        id,
      );
      const res = await insertSeveralOddsFromInputs(db)(propertyData)(
        matchInputs.odds,
        id,
      );
      const odds = res.map(res => res.odds);
      const bets = res.flatMap(res => res.bets);

      const match = createMatchFromInputs(propertyData)({
        id,
        match: matchInputs,
        matchTeams,
        oddsIds: odds.map((odds) => odds.id),
      });

      await insertMatch(db)(match);

      return {
        match,
        matchTeams,
        odds,
        bets
      };
    };
};

export const createMatchFromInputs = ({
  source,
  date,
}: {
  source: string;
  date: DateNumber;
}) =>
  ({
    id,
    match,
    matchTeams,
    oddsIds,
  }: {
    id: UUIDString;
    match: {
      result?: number[] | null;
      status: keyof typeof SaveMatchStatus;
      currentMinute?: number | null;
      startDate?: DateNumber | null;
      endDate?: DateNumber | null;
      competitionId: UUIDString;
    };
    matchTeams: MatchTeamSaveFormat[];
    oddsIds: UUIDString[];
  }): MatchSaveFormat => {
    const name = matchTeams.map(t => t.name).join(" - ");
    const childIds = matchTeams.map(({ id }) => id);
    const result = match.result ? [...match.result] : null;

    const createPropertyOfSource = createProperty({ source, date });
    const properties: Property<PropertyTypes>[] = [];
    if (match.currentMinute != null) {
      properties.push(
        createPropertyOfSource<number>("currentMinute", match.currentMinute),
      );
    }
    if (match.endDate != null) {
      properties.push(
        createPropertyOfSource<DateNumber>("endDate", match.endDate),
      );
    }

    return createMatchSaveFormat({
      id,
      name,
      properties,
      status: match.status,
      result,
      startDate: match.startDate ?? null,
      childIds,
      oddsIds,
      competitionId: match.competitionId
    });
  };

export const createMatchSaveFormat = ({
  id,
  name,
  properties = [],
  status,
  result,
  startDate,
  childIds = [],
  oddsIds = [],
  competitionId
}: {
  id: UUIDString;
  name: string;
  properties?: Property<PropertyTypes>[];
  status: keyof typeof SaveMatchStatus;
  result: number[] | null;
  startDate: DateNumber | null;
  childIds?: UUIDString[];
  oddsIds?: UUIDString[];
  competitionId: UUIDString;
}): MatchSaveFormat => ({
  id,
  name,
  properties,
  status,
  result,
  startDate,
  childIds,
  oddsIds,
  competitionId
});

export const insertMatch = (db: DB) =>
  (match: MatchSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, match)
  );

export const retrieveMatchById = (db: DB) =>
  (id: UUIDString): Promise<MatchSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );

export const retrieveAllMatches = (db: DB): Promise<MatchSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);

export enum SaveMatchStatus {
  "TBD" = "Time To Be Defined",
  "NS" = "Not Started",
  "1H" = "First Half, Kick Off",
  "HT" = "Halftime",
  "2H" = "Second Half, 2nd Half Started",
  "ET" = "Extra Time",
  "P" = "Penalty In Progress",
  "FT" = "Match Finished",
  "AET" = "Match Finished After Extra Time",
  "PEN" = "Match Finished After Penalty",
  "BT" = "Break Time (in Extra Time)",
  "SUSP" = "Match Suspended",
  "INT" = "Match Interrupted",
  "PST" = "Match Postponed",
  "CANC" = "Match Cancelled",
  "ABD" = "Match Abandoned",
  "AWD" = "Technical Loss",
  "WO" = "WalkOver",
}

function isMatchStatus(s: any): s is SaveMatchStatus {
  return s in SaveMatchStatus;
}
