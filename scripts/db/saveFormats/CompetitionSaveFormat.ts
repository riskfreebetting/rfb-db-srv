import { CompetitionInputs } from "../../inputs/CompetitionInputs.ts";
import { generateUUIDString, getDefaultValue, isUUIDString, Property, trace, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "competitions";

export interface CompetitionSaveFormat extends SaveFormat {
  alternativeNames: Record<UUIDString, string[]>;
  sportId: UUIDString;
  countryId: UUIDString;
}

export const isCompetitionSaveFormat = (o: any): o is CompetitionSaveFormat => (
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" && 
  Object.keys(o?.alternativeNames ?? {}).every((k: string) => isUUIDString(k)) && 
  Object.values(o?.alternativeNames ?? {}).every((x: any) => (
    Array.isArray(x) && x.every(x => typeof x === "string")
  )) &&
  isUUIDString(o?.countryId) &&
  isUUIDString(o?.sportId) &&
  isSaveFormat(o)
);

export const insertCompetitionFromInputs = (db: DB) =>
  async (inputs: CompetitionInputs): Promise<CompetitionSaveFormat> => {
    const competitionSaveFormat = createCompetitionSaveFormatFromInputs(inputs);

    await insertCompetition(db)(competitionSaveFormat);

    return competitionSaveFormat;
  };

export const updateCompetition = (db: DB) => 
  async (id: UUIDString, inputs: CompetitionInputs): Promise<CompetitionSaveFormat> => {
    const existingCompetition = await retrieveCompetitionById(db)(id);
      
    const updatedCompetition = {
      ...createCompetitionFromInputs({
        id,
        inputs: {
          name: existingCompetition.name,
          alternativeNames: Object.keys(existingCompetition.alternativeNames)
            .concat(
              Object.keys(inputs.alternativeNames ?? {})
                .filter(s => !Object.keys(existingCompetition.alternativeNames).includes(s))
            )
            .reduce((acc, id) => ({
              ...acc,
              [id]: existingCompetition.alternativeNames[id] 
                ? existingCompetition.alternativeNames[id].concat(
                  (inputs.alternativeNames[id] ?? []).filter(s => !existingCompetition.alternativeNames[id].includes(s)) ?? []
                ) 
                : inputs.alternativeNames[id],
            }), {}),  
          sportId: getDefaultValue(inputs.sportId, existingCompetition.sportId),
          countryId: getDefaultValue(inputs.countryId, existingCompetition.countryId)
        }
      }),
      properties: existingCompetition.properties
    };

    await insertCompetition(db)(updatedCompetition);

    return updatedCompetition;
  };

export const createCompetitionSaveFormatFromInputs = (
  inputs: CompetitionInputs,
): CompetitionSaveFormat => createCompetitionFromInputs({
  id: generateUUIDString(),
  inputs
});

export const createCompetitionFromInputs = ({
  id,
  inputs: {
    name,
    alternativeNames,
    sportId,
    countryId
  }
}: {
  id: UUIDString,
  inputs: CompetitionInputs
}) => createCompetitionSaveFormat({
  id,
  name,
  alternativeNames,
  sportId,
  countryId,
  properties: []
});


export const createCompetitionSaveFormat = ({
  id,
  name,
  alternativeNames = {},
  sportId,
  countryId,
  properties = [],
}: {
  id: UUIDString;
  name: string;
  alternativeNames?: Record<UUIDString, string[]>;
  sportId: UUIDString;
  countryId: UUIDString;
  properties?: Property<any>[];
}): CompetitionSaveFormat => ({
  id,
  name,
  alternativeNames,
  sportId,
  countryId,
  properties,
});

export const insertCompetition = (db: DB) =>
  (competition: CompetitionSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, competition)
  );

export const retrieveAllCompetitions = (db: DB): Promise<CompetitionSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);
export const retrieveCompetitionById = (db: DB) =>
  (id: string): Promise<CompetitionSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
