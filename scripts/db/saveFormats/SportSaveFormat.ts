import { SportInputs } from "../../inputs/SportInputs.ts";
import { generateUUIDString, isUUIDString, Property, trace, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "sports";

export interface SportSaveFormat extends SaveFormat {
  alternativeNames: Record<UUIDString, string[]>;
}

export const isSportSaveFormat = (o: any): o is SportSaveFormat => (
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" && 
  Object.keys(o?.alternativeNames ?? {}).every((k: string) => isUUIDString(k)) && 
  Object.values(o?.alternativeNames ?? {}).every((x: any) => (
    Array.isArray(x) && x.every(x => typeof x === "string")
  )) &&
  isSaveFormat(o)
);

export const insertSportFromInputs = (db: DB) =>
  async (inputs: SportInputs): Promise<SportSaveFormat> => {
    const sportSaveFormat = createSportSaveFormatFromInputs(inputs);

    await insertSport(db)(sportSaveFormat);

    return sportSaveFormat;
  };

export const updateSport = (db: DB) => 
  async (id: UUIDString, inputs: SportInputs): Promise<SportSaveFormat> => {
    const existingSport = await retrieveSportById(db)(id);

    const updatedSport = {
      ...createSportFromInputs({
        id,
        inputs: {
          name: existingSport.name,
          alternativeNames: Object.keys(existingSport.alternativeNames)
            .concat(
              Object.keys(inputs.alternativeNames ?? {})
                .filter(s => !Object.keys(existingSport.alternativeNames).includes(s))
            )
            .reduce((acc, id) => ({
              ...acc,
              [id]: existingSport.alternativeNames[id] 
                ? existingSport.alternativeNames[id].concat(
                  (inputs.alternativeNames[id] ?? []).filter(s => !existingSport.alternativeNames[id].includes(s)) ?? []
                ) 
                : inputs.alternativeNames[id],
            }), {})
        }
      }),
      properties: existingSport.properties
    };

    await insertSport(db)(updatedSport);

    return updatedSport;
  };

export const createSportSaveFormatFromInputs = (
  inputs: SportInputs,
): SportSaveFormat => createSportFromInputs({
  id: generateUUIDString(),
  inputs
});

export const createSportFromInputs = ({
  id,
  inputs: {
    name,
    alternativeNames
  }
}: {
  id: UUIDString,
  inputs: SportInputs
}) => createSportSaveFormat({
  id,
  name,
  alternativeNames,
  properties: []
});


export const createSportSaveFormat = ({
  id,
  name,
  alternativeNames = {},
  properties = [],
}: {
  id: UUIDString;
  name: string;
  alternativeNames?: Record<UUIDString, string[]>;
  properties?: Property<any>[];
}): SportSaveFormat => ({
  id,
  name,
  alternativeNames,
  properties,
});

export const insertSport = (db: DB) =>
  (sport: SportSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, sport)
  );

export const retrieveAllSports = (db: DB): Promise<SportSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);
export const retrieveSportById = (db: DB) =>
  (id: string): Promise<SportSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
