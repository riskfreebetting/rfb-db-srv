import { BetInputs, OutcomeInputs } from "../../inputs/MatchInputs.ts";
import {
  createProperty,
  DateNumber,
  findProperty,
  generateUUIDString,
  getDefaultValue,
  Property,
  trace,
  UUIDString,
} from "../../utils.ts";
import { DB } from "../db.ts";
import { PropertyData, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "bets";

export interface BetSaveFormat extends SaveFormat {
  typeId: UUIDString;
  date: DateNumber;
  properties: Property<OutcomeSaveFormat[]>[];

  // outcomes in properties
}

export interface OutcomeSaveFormat {
  name: string;
  odd: number;
}

export const createOutcomeSaveFormat = (
  name: string = "X",
  odd: number = 1.00,
): OutcomeSaveFormat => ({
  name,
  odd,
});

export const insertBetsNotCreatedYet = async (
  db: DB, propertyData: PropertyData, 
  existingBets: BetSaveFormat[], betInputs: BetInputs[]
): Promise<BetSaveFormat[]> => {
  const updatesForExistingBets = existingBets.map(findUpdateForBets(betInputs));
  const updatedBets = await updateOrKeepBets(db, propertyData, existingBets, updatesForExistingBets);
  
  const inputsForNewBets = betInputs.filter(doOddsNotExistYet(existingBets));
  const newBets = await insertBetsFromInputs(db)(propertyData)(inputsForNewBets);

  return updatedBets.concat(newBets);

  function findUpdateForBets(betInputs: BetInputs[]) {
    return (bet: BetSaveFormat) => betInputs.find(betInput => (
      betInput.typeId === bet.typeId
    )) ?? null;
  }

  function updateOrKeepBets(
    db: DB, propertyData: PropertyData, 
    allExistingBets: BetSaveFormat[], updates: (BetInputs|null)[]
  ) {
    return Promise.all(updates.map((betsUpdate, idx) => {
      const existingBet = allExistingBets[idx];

      return betsUpdate
        ? updateBets(db, propertyData, existingBet, betsUpdate)
        : existingBet;
    }));
  }

  async function updateBets(
    db: DB, propertyData: PropertyData, 
    existingBet: BetSaveFormat, 
    betsUpdate: BetInputs
  ): Promise<BetSaveFormat> {
    const bet = createBetSaveFormatFromInput(propertyData)({
      id: existingBet.id,
      bet: {
        typeId: betsUpdate.typeId,
        date: getDefaultValue(betsUpdate.date, existingBet.date),
        outcomes: betsUpdate.outcomes
      }
    });

    const newBet = { 
      ...bet,
      properties: existingBet.properties.concat(bet.properties)
    };

    await insertBet(db)(newBet);

    return newBet;
  }

  function doOddsNotExistYet(existingBets: BetSaveFormat[]) {
    return (inputs: BetInputs) => existingBets.every(bet => 
      inputs.typeId !== bet.typeId
    );
  }
};

export const insertBetsFromInputs = (db: DB) =>
  (propertyData: PropertyData) =>
    (bets: BetInputs[]): Promise<BetSaveFormat[]> => (
      Promise.all(bets.map(insertBetFromInputs(db)(propertyData)))
    );

export const insertBetFromInputs = (db: DB) =>
  (propertyData: PropertyData) =>
    async (bet: BetInputs): Promise<BetSaveFormat> => {
      const savedBet = createBetSaveFormatFromInput(propertyData)({
        id: generateUUIDString(),
        bet
      });

      await insertBet(db)(savedBet);

      return savedBet;
    };

export const createBetSaveFormatFromInput = (propertyData: PropertyData) => ({
  id,
  bet: {
    outcomes=[],
    typeId,
    date
  }}: {
    id: UUIDString,
    bet: {
      typeId: UUIDString,
      date: DateNumber,
      outcomes?: OutcomeInputs[]
    }
  }) => {
    const outcomesProperty = outcomes.map((outcome) =>
      createOutcomeSaveFormat(outcome.name, outcome.odd)
    );

    return createBetSaveFormat({
      id,
      name: `Bet ${id}`,
      date,
      typeId,
      properties: [
        createProperty(propertyData)<OutcomeSaveFormat[]>("outcomes", outcomesProperty),
      ],
    });
  };

export const createBetSaveFormat = ({
  id,
  name,
  date,
  properties = [],
  typeId,
}: {
  id: UUIDString;
  name: string;
  date: DateNumber;
  properties?: Property<any>[];
  typeId: UUIDString;
}): BetSaveFormat => ({
  id,
  name,
  date,
  properties,
  typeId,
});

export const insertBet = (db: DB) =>
  (bet: BetSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, bet)
  );

export const retrieveBetById = (db: DB) =>
  (id: UUIDString): Promise<BetSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
