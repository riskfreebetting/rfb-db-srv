import { TeamInputs } from "../../inputs/TeamInputs.ts";
import { generateUUIDString, isUUIDString, Property, trace, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "teams";

export interface TeamSaveFormat extends SaveFormat {
  alternativeNames: Record<UUIDString, string[]>;
}

export const isTeamSaveFormat = (o: any): o is TeamSaveFormat => (
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" && 
  Object.keys(o?.alternativeNames ?? {}).every((k: string) => isUUIDString(k)) && 
  Object.values(o?.alternativeNames ?? {}).every((x: any) => (
    Array.isArray(x) && x.every(x => typeof x === "string")
  )) &&
  isSaveFormat(o)
);

export const insertTeamFromInputs = (db: DB) =>
  async (inputs: TeamInputs): Promise<TeamSaveFormat> => {
    const teamSaveFormat = createTeamSaveFormatFromInputs(inputs);

    await insertTeam(db)(teamSaveFormat);

    return teamSaveFormat;
  };

export const updateTeam = (db: DB) => 
  async (id: UUIDString, inputs: TeamInputs): Promise<TeamSaveFormat> => {
    const existingTeam = await retrieveTeamById(db)(id);

    const updatedTeam = {
      ...createTeamFromInputs({
        id,
        inputs: {
          name: existingTeam.name,
          alternativeNames: Object.keys(existingTeam.alternativeNames)
            .concat(
              Object.keys(inputs.alternativeNames ?? {})
                .filter(s => !Object.keys(existingTeam.alternativeNames).includes(s))
            )
            .reduce((acc, id) => ({
              ...acc,
              [id]: existingTeam.alternativeNames[id] 
                ? existingTeam.alternativeNames[id].concat(
                  (inputs.alternativeNames[id] ?? []).filter(s => !existingTeam.alternativeNames[id].includes(s)) ?? []
                ) 
                : inputs.alternativeNames[id],
            }), {})
        }
      }),
      properties: existingTeam.properties
    };

    await insertTeam(db)(updatedTeam);

    return updatedTeam;
  };

export const createTeamSaveFormatFromInputs = (
  inputs: TeamInputs,
): TeamSaveFormat => createTeamFromInputs({
  id: generateUUIDString(),
  inputs
});

export const createTeamFromInputs = ({
  id,
  inputs: {
    name,
    alternativeNames
  }
}: {
  id: UUIDString,
  inputs: TeamInputs
}) => createTeamSaveFormat({
  id,
  name,
  alternativeNames,
  properties: []
});


export const createTeamSaveFormat = ({
  id,
  name,
  alternativeNames = {},
  properties = [],
}: {
  id: UUIDString;
  name: string;
  alternativeNames?: Record<UUIDString, string[]>;
  properties?: Property<any>[];
}): TeamSaveFormat => ({
  id,
  name,
  alternativeNames,
  properties,
});

export const insertTeam = (db: DB) =>
  (team: TeamSaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, team)
  );

export const retrieveAllTeams = (db: DB): Promise<TeamSaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);
export const retrieveTeamById = (db: DB) =>
  (id: string): Promise<TeamSaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
