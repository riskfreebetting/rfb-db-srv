import {
  DateNumber,
  isProperty,
  isUUIDString,
  Property,
  UUIDString,
} from "../../utils.ts";

export interface SaveFormat {
  id: UUIDString;
  name: string;
  properties: Property<any>[];
  childIds?: UUIDString[];
  parentId?: UUIDString;
}

export const isSaveFormat = (o: any): o is SaveFormat => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  Array.isArray(o?.properties) && o?.properties.every(isProperty) &&
  ((Array.isArray(o?.childIds) && o?.childIds.every(isUUIDString)) ||
    o?.childIds === undefined) &&
  (isUUIDString(o?.parentId) || o?.parentId === undefined)
);

export interface PropertyData {
  source: string;
  date: DateNumber;
}
