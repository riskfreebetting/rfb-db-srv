import { CountryInputs } from "../../inputs/CountryInputs.ts";
import { generateUUIDString, isUUIDString, Property, trace, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { isSaveFormat, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "countries";

export interface CountrySaveFormat extends SaveFormat {
  alternativeNames: Record<UUIDString, string[]>;
}

export const isCountrySaveFormat = (o: any): o is CountrySaveFormat => (
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" && 
  Object.keys(o?.alternativeNames ?? {}).every((k: string) => isUUIDString(k)) && 
  Object.values(o?.alternativeNames ?? {}).every((x: any) => (
    Array.isArray(x) && x.every(x => typeof x === "string")
  )) &&
  isSaveFormat(o)
);

export const insertCountryFromInputs = (db: DB) =>
  async (inputs: CountryInputs): Promise<CountrySaveFormat> => {
    const countrySaveFormat = createCountrySaveFormatFromInputs(inputs);

    await insertCountry(db)(countrySaveFormat);

    return countrySaveFormat;
  };

export const updateCountry = (db: DB) => 
  async (id: UUIDString, inputs: CountryInputs): Promise<CountrySaveFormat> => {
    const existingCountry = await retrieveCountryById(db)(id);

    const updatedCountry = {
      ...createCountryFromInputs({
        id,
        inputs: {
          name: existingCountry.name,
          alternativeNames: Object.keys(existingCountry.alternativeNames)
            .concat(
              Object.keys(inputs.alternativeNames ?? {})
                .filter(s => !Object.keys(existingCountry.alternativeNames).includes(s))
            )
            .reduce((acc, id) => ({
              ...acc,
              [id]: existingCountry.alternativeNames[id] 
                ? existingCountry.alternativeNames[id].concat(
                  (inputs.alternativeNames[id] ?? []).filter(s => !existingCountry.alternativeNames[id].includes(s)) ?? []
                ) 
                : inputs.alternativeNames[id],
            }), {})
        }
      }),
      properties: existingCountry.properties
    };

    await insertCountry(db)(updatedCountry);

    return updatedCountry;
  };

export const createCountrySaveFormatFromInputs = (
  inputs: CountryInputs,
): CountrySaveFormat => createCountryFromInputs({
  id: generateUUIDString(),
  inputs
});

export const createCountryFromInputs = ({
  id,
  inputs: {
    name,
    alternativeNames
  }
}: {
  id: UUIDString,
  inputs: CountryInputs
}) => createCountrySaveFormat({
  id,
  name,
  alternativeNames,
  properties: []
});


export const createCountrySaveFormat = ({
  id,
  name,
  alternativeNames = {},
  properties = [],
}: {
  id: UUIDString;
  name: string;
  alternativeNames?: Record<UUIDString, string[]>;
  properties?: Property<any>[];
}): CountrySaveFormat => ({
  id,
  name,
  alternativeNames,
  properties,
});

export const insertCountry = (db: DB) =>
  (country: CountrySaveFormat): Promise<DB> => (
    db.insert(DB_TYPE, country)
  );

export const retrieveAllCountries = (db: DB): Promise<CountrySaveFormat[]> => (
  db.retrieveAll(DB_TYPE)
);
export const retrieveCountryById = (db: DB) =>
  (id: string): Promise<CountrySaveFormat> => (
    db.retrieve(DB_TYPE, id)
  );
