import { MatchStatusInputs, OddsInputs } from "../../inputs/MatchInputs.ts";
import { generateUUIDString, Property, UUIDString } from "../../utils.ts";
import { DB } from "../db.ts";
import { insertBetsFromInputs, BetSaveFormat, retrieveBetById, insertBetsNotCreatedYet } from "./BetSaveFormat.ts";
import { PropertyData, SaveFormat } from "./SaveFormat.ts";

const DB_TYPE = "odds";

export interface OddsSaveFormat extends SaveFormat {
  bookmakerId: UUIDString;
  childIds: UUIDString[];
  parentId: UUIDString;
}

type OddsAndBets = { bets: BetSaveFormat[], odds: OddsSaveFormat };

export const insertOddsNotCreatedYet = (db: DB) => 
  (propertyData: PropertyData) => 
    async (
      oddsInputs: OddsInputs[]=[],
      existingOddsIds: UUIDString[],
      parentId: UUIDString
    ): Promise<OddsAndBets[]> => {
      const existingOdds = await createOddsWithBets(db, existingOddsIds);
      return insertOrUpdateOdds(db, propertyData, oddsInputs, existingOdds, parentId);

      async function createOddsWithBets(db: DB, existingOddsIds: UUIDString[]): Promise<OddsAndBets[]> {
        const existingOdds = await Promise.all(existingOddsIds.map(retrieveOddsById(db)));

        return Promise.all(existingOdds.map(async (odds: OddsSaveFormat) => ({
          odds,
          bets: await Promise.all(odds.childIds.map(retrieveBetById(db)))
        })));
      }

      async function insertOrUpdateOdds(
        db: DB, propertyData: PropertyData, 
        oddsInputs: OddsInputs[], existingOdds: OddsAndBets[], 
        parentId: UUIDString
      ): Promise<OddsAndBets[]> {
        const updatesForExistingOdds = existingOdds.map(findUpdateForOdds(oddsInputs));
        const updatedOdds = await updateOrKeepOdds(db, propertyData, existingOdds, updatesForExistingOdds, parentId);
        
        const inputsForNewOdds = oddsInputs.filter(doOddsNotExistYet(existingOdds));
        const newOdds = await insertSeveralOddsFromInputs(db)(propertyData)(inputsForNewOdds, parentId);

        return updatedOdds.concat(newOdds);

        function findUpdateForOdds(oddsInputs: OddsInputs[]) {
          return ({ odds }: OddsAndBets) => oddsInputs.find(oddsInput => (
            oddsInput.bookmakerId === odds.bookmakerId
          )) || null;
        }

        function updateOrKeepOdds(
          db: DB, propertyData: PropertyData, 
          allExistingOdds: OddsAndBets[], updates: (OddsInputs|null)[],
          parentId: UUIDString
        ) {
          return Promise.all(updates.map((oddsUpdate, idx) => {
            const existingOdds = allExistingOdds[idx];

            return oddsUpdate
              ? updateOdds(db, propertyData, existingOdds, oddsUpdate, parentId)
              : existingOdds;
          }));
        }

        async function updateOdds(
          db: DB, propertyData: PropertyData, 
          existingOdds: OddsAndBets, 
          oddsUpdate: OddsInputs,
          parentId: UUIDString
        ): Promise<OddsAndBets> {
          const bets = await insertBetsNotCreatedYet(db, propertyData, existingOdds.bets, oddsUpdate.bets);
          
          const odds = createOddsSaveFormatFromInputs({
            id: existingOdds.odds.id,
            odds: {
              bookmakerId: oddsUpdate.bookmakerId
            }, 
            parentId,
            betIds: bets.map(({ id }) => id)
          });

          await insertOdds(db)(odds);

          return {
            odds,
            bets
          };
        }

        function doOddsNotExistYet(existingOdds: OddsAndBets[]) {
          return (inputs: OddsInputs) => existingOdds.every(({ odds }) => 
            inputs.bookmakerId !== odds.bookmakerId
          );
        }
      }
    };

export const insertSeveralOddsFromInputs = (db: DB) =>
  (propertyData: PropertyData) =>
    async (
      odds: OddsInputs[],
      parentId: UUIDString,
    ): Promise<OddsAndBets[]> => (
      Promise.all(odds.map(insertOddsFromInputs(db, parentId)(propertyData)))
    );

export const insertOddsFromInputs = (db: DB, parentId: UUIDString) =>
  (propertyData: PropertyData) =>
    async (inputs: OddsInputs): Promise<OddsAndBets> => {
      const bets = await insertBetsFromInputs(db)(propertyData)(inputs.bets);
      const savedOdds = createOddsSaveFormatFromInputs({
        id: generateUUIDString(),
        odds: inputs,
        parentId,
        betIds: bets.map((bet) => bet.id),
      });

      await insertOdds(db)(savedOdds);

      return {
        odds: savedOdds,
        bets
      };
    };

export const createOddsSaveFormatFromInputs = ({
  id,
  odds,
  parentId,
  betIds,
}: {
  id: UUIDString,
  odds: {
    bookmakerId: UUIDString
  },
  parentId: UUIDString;
  betIds: UUIDString[];
}): OddsSaveFormat => createOddsSaveFormat({
    id,
    name: `Odds ${id}`,
    bookmakerId: odds.bookmakerId,
    childIds: betIds,
    parentId,
  });

export const createOddsSaveFormat = ({
  id,
  name,
  bookmakerId,
  childIds = [],
  parentId,
  properties = [],
}: {
  id: UUIDString;
  name: string;
  bookmakerId: UUIDString;
  childIds?: UUIDString[];
  parentId: UUIDString;
  properties?: Property<any>[];
}): OddsSaveFormat => ({
  id,
  name,
  bookmakerId,
  childIds,
  parentId,
  properties,
});

export const insertOdds = (db: DB) =>
  (odds: OddsSaveFormat) => (
    db.insert(DB_TYPE, odds)
  );

export const retrieveOddsById = (db: DB) =>
  (id: UUIDString) => (
    db.retrieve(DB_TYPE, id)
  );
