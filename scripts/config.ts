import { config } from "https://deno.land/x/dotenv/mod.ts";
import { trace } from "./utils.ts";

interface Config {
  DB_HOST: string;
  DB_PORT: number;
  DB_NAME: string;
  DB_PATH: string;
  ENV: string;
  SYS: string;
};

const {
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_PATH,
  ENV,
  SYS
} = trace<Record<string, string>>("config")(config({ path: "../.env" }));

export const CONFIG = {
  DB_HOST,
  DB_PORT: parseInt(DB_PORT),
  DB_NAME,
  DB_PATH,
  SYS,
  ENV
} as Config;