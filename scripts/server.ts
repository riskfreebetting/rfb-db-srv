import { CONFIG } from "./config.ts"

import {
  Application,
  Context,
  HttpException,
  InternalServerErrorException,
  NotImplementedException,
} from "https://deno.land/x/abc@v1.3.1/mod.ts";

import { createDB, DB } from "./db/db.ts";
import { createAllFsDBs, createFsDB } from "./db/fsDB.ts";
import {
  insertMatchFromInputs,
  retrieveAllMatches,
  updateMatch,
} from "./db/saveFormats/MatchSaveFormat.ts";
import {
  createReturnMatchFromSaveFormat,
  ReturnMatch,
} from "./factories/ReturnMatch.ts";
import {
  createReturnTeamFromSaveFormat,
  ReturnTeam,
} from "./factories/ReturnTeam.ts";
import {
  insertTeamFromInputs,
  retrieveAllTeams,
  updateTeam
} from "./db/saveFormats/TeamSaveFormat.ts";
import {
  createReturnBookmakerFromSaveFormat,
  ReturnBookmaker,
} from "./factories/ReturnBookmaker.ts";
import {
  insertBookmakerFromInputs,
  retrieveAllBookmakers,
} from "./db/saveFormats/BookmakerSaveFormat.ts";
import {
  BookmakerInputs,
  validatePostBookmakerRequest,
} from "./inputs/BookmakerInputs.ts";
import {
  createReturnBetTypeFromSaveFormat,
  ReturnBetType,
} from "./factories/ReturnBetType.ts";
import {
  insertBetTypeFromInputs,
  retrieveAllBetTypes,
} from "./db/saveFormats/BetTypeSaveFormat.ts";
import {
  BetTypeInputs,
  validatePostBetTypeRequest,
} from "./inputs/BetTypeInputs.ts";
import {
  createReturnRFBFromSaveFormat,
  ReturnRFB,
} from "./factories/ReturnRFB.ts";
import {
  insertRFBFromInputs,
  retrieveAllRFBs,
  updateRFB
} from "./db/saveFormats/RFBSaveFormat.ts";
import {
  RFBInputs,
  validatePostRFBRequest,
  validateUpdateRFBRequest
} from "./inputs/RFBInputs.ts";
import { PostBody, validateGetParams, validatePostBody } from "./RequestFormats.ts";

import { TeamInputs, validatePostTeamRequest, validateUpdateTeamRequest } from "./inputs/TeamInputs.ts";
import { MatchInputs, validatePostMatchRequest, validateUpdateMatchRequest } from "./inputs/MatchInputs.ts";
import { createNotFoundDBWarning, createNotFoundDBError, createReturnObject, handleSeveralSettledObjects, handleSeveralSettledPromises, ReturnObject } from "./factories/ReturnObject.ts";
import { CompetitionInputs, validatePostCompetitionRequest, validateUpdateCompetitionRequest } from "./inputs/CompetitionInputs.ts";
import { createReturnCompetitionFromSaveFormat, ReturnCompetition } from "./factories/ReturnCompetition.ts";
import { insertCompetitionFromInputs, retrieveAllCompetitions, updateCompetition } from "./db/saveFormats/CompetitionSaveFormat.ts";
import { insertSportFromInputs, retrieveAllSports, updateSport } from "./db/saveFormats/SportSaveFormat.ts";
import { SportInputs, validatePostSportRequest, validateUpdateSportRequest } from "./inputs/SportInputs.ts";
import { createReturnSportFromSaveFormat, ReturnSport } from "./factories/ReturnSport.ts";
import { insertCountryFromInputs, retrieveAllCountries, updateCountry } from "./db/saveFormats/CountrySaveFormat.ts";
import { createReturnCountryFromSaveFormat, ReturnCountry } from "./factories/ReturnCountry.ts";
import { CountryInputs, validatePostCountryRequest, validateUpdateCountryRequest } from "./inputs/CountryInputs.ts";

const handleDeleteDB = (dbs: Record<string, DB>) => async (ctx: Context) => {
  const id = ctx.queryParams.db;
  const db = dbs[id];

  if (db) {
    console.log(`Deleting DB with id '${id}'`);
    
    await db.deleteDB();

    delete dbs[id];

    return createReturnObject();
  } else {
    return createReturnObject({
      messages: [createNotFoundDBError(id)]
    })
  }
};

export const handleGetCountries = (db: DB) =>
  async (): Promise<ReturnObject<ReturnCountry[]>> => {
    const dbCountries = await retrieveAllCountries(db);
    const countries = await Promise.allSettled(
      dbCountries.map(createReturnSportFromSaveFormat),
    );

    return handleSeveralSettledPromises(dbCountries, countries);
  };

export const handlePostCountry = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnCountry>> => {
    const country = await insertCountryFromInputs(db)(body.inputs as CountryInputs);
    
    console.log("Inserted country: ", country.name);

    return createReturnObject({
      result: createReturnCountryFromSaveFormat(country),
    });
  };

export const handleUpdateCountry = (db: DB) => async (
  { 
    source, 
    date, 
    inputs
  }: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnCountry>> => {
  const country = await updateCountry(db)(params.id, inputs as CountryInputs);

  console.log("Updated country: ", country.name);

  return createReturnObject({
    result: createReturnSportFromSaveFormat(country)
  });
};

export const handleGetSports = (db: DB) =>
  async (): Promise<ReturnObject<ReturnSport[]>> => {
    const dbSports = await retrieveAllSports(db);
    const sports = await Promise.allSettled(
      dbSports.map(createReturnSportFromSaveFormat),
    );

    return handleSeveralSettledPromises(dbSports, sports);
  };

export const handlePostSport = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnSport>> => {
    const sport = await insertSportFromInputs(db)(body.inputs as SportInputs);
    
    console.log("Inserted sport: ", sport.name);

    return createReturnObject({
      result: createReturnSportFromSaveFormat(sport),
    });
  };

export const handleUpdateSport = (db: DB) => async (
  { 
    source, 
    date, 
    inputs
  }: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnSport>> => {
  const sport = await updateSport(db)(params.id, inputs as SportInputs);

  console.log("Updated sport: ", sport.name);

  return createReturnObject({
    result: createReturnSportFromSaveFormat(sport)
  });
};

export const handleGetCompetitions = (db: DB) =>
  async (): Promise<ReturnObject<ReturnCompetition[]>> => {
    const dbCompetitions = await retrieveAllCompetitions(db);
    const competitions = await Promise.allSettled(
      dbCompetitions.map(createReturnCompetitionFromSaveFormat(db)),
    );

    return handleSeveralSettledObjects(dbCompetitions, competitions);
  };

export const handlePostCompetition = (db: DB) => 
  async (body: PostBody): Promise<ReturnObject<ReturnCompetition>> => {
    const competition = await insertCompetitionFromInputs(db)(body.inputs as CompetitionInputs);
    
    console.log("Posted competition: ", competition.name);
    
    return createReturnCompetitionFromSaveFormat(db)(competition);
  };

export const handleUpdateCompetition = (db: DB) => async (
  { 
    source, 
    date, 
    inputs
  }: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnCompetition>> => {
  const competition = await updateCompetition(db)(params.id, inputs as CompetitionInputs);

  console.log("Updated competition: ", competition.name);

  return createReturnCompetitionFromSaveFormat(db)(competition);
};
  
export const handleGetMatches = (db: DB) =>
  async ({ source, beforeDate, afterDate }: Record<string, string>): Promise<ReturnObject<ReturnMatch[]>> => {
    const dbMatches = await retrieveAllMatches(db);

    const matches = await Promise.allSettled(
      dbMatches.map(createReturnMatchFromSaveFormat(db)({
        source,
        beforeDate: parseInt(beforeDate),
        afterDate: parseInt(afterDate)
      })),
    );

    return handleSeveralSettledObjects(dbMatches, matches);
  };

export const handlePostMatch = (db: DB) =>
  async ({ 
    source, 
    date, 
    inputs 
  }: PostBody): Promise<ReturnObject<ReturnMatch>> => {
    const { match, matchTeams } = await insertMatchFromInputs(db)({ 
      source: source, 
      date: date 
    })(inputs as MatchInputs);
 
    console.log("Posted match: ", matchTeams.map(t => t.name).join(" vs. "));

    return createReturnMatchFromSaveFormat(db)({
      source
    })(match);
  };

export const handleUpdateMatch = (db: DB) => async (
  { 
    source, 
    date, 
    inputs
  }: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnMatch>> => {
    const { match, matchTeams } = await updateMatch(db)({ 
      source, 
      date 
    })(params.id, inputs as MatchInputs);

    console.log("Updated match: ", matchTeams.map(t => t.name).join(" vs. "));

    return createReturnMatchFromSaveFormat(db)({
      source
    })(match);
  };

export const handleGetTeams = (db: DB) =>
  async (): Promise<ReturnObject<ReturnTeam[]>> => {
    const dbTeams = await retrieveAllTeams(db);
    const teams = await Promise.allSettled(
      dbTeams.map(createReturnTeamFromSaveFormat),
    );

    return handleSeveralSettledPromises(dbTeams, teams);
  };

export const handlePostTeam = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnTeam>> => {
    const team = await insertTeamFromInputs(db)(body.inputs as TeamInputs);
    
    console.log("Posted team: ", team.name);
    
    return createReturnObject({
      result: createReturnTeamFromSaveFormat(team),
    });
  };

export const handleUpdateTeam = (db: DB) => async (
  { 
    source, 
    date, 
    inputs
  }: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnTeam>> => {
  const team = await updateTeam(db)(params.id, inputs as TeamInputs);

  return createReturnObject({
    result: createReturnTeamFromSaveFormat(team)
  });
};

export const handleGetBookmakers = (db: DB) =>
  async (): Promise<ReturnObject<ReturnBookmaker[]>> => {
    const dbBookmakers = await retrieveAllBookmakers(db);
    const bookmakers = await Promise.allSettled(
      dbBookmakers.map(createReturnBookmakerFromSaveFormat),
    );

    return handleSeveralSettledPromises(dbBookmakers, bookmakers);
  };

export const handlePostBookmaker = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnBookmaker>> => {
    const bookmaker = await insertBookmakerFromInputs(db)(
      body.inputs as BookmakerInputs,
    );
    
    return createReturnObject({
      result: createReturnBookmakerFromSaveFormat(bookmaker),
    });
  };

export const handleGetBetTypes = (db: DB) =>
  async (): Promise<ReturnObject<ReturnBetType[]>> => {
    const dbBetTypes = await retrieveAllBetTypes(db);
    const betTypes = await Promise.allSettled(
      dbBetTypes.map(createReturnBetTypeFromSaveFormat),
    );

    return handleSeveralSettledPromises(dbBetTypes, betTypes);
  };

export const handlePostBetType = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnBetType>> => {
    const betType = await insertBetTypeFromInputs(db)(
      body.inputs as BetTypeInputs,
    );

    console.log("Posted bet type", betType.name);
    
    return createReturnObject({
      result: createReturnBetTypeFromSaveFormat(betType),
    });
  };

export const handleGetRFBs = (db: DB) =>
  async ({ 
    source, 
    beforeDate,
    afterDate 
  }: Record<string, string>): Promise<ReturnObject<ReturnRFB[]>> => {
    const dbRFBs = await retrieveAllRFBs(db);
    const rfbs = await Promise.allSettled(
      dbRFBs.map(createReturnRFBFromSaveFormat(db)({ 
        source, 
        beforeDate: parseInt(beforeDate),
        afterDate: parseInt(afterDate)
      })),
    );

    return handleSeveralSettledObjects(dbRFBs, rfbs);
  };

export const handlePostRFB = (db: DB) =>
  async (body: PostBody): Promise<ReturnObject<ReturnRFB>> => {
    const rfb = await insertRFBFromInputs(db)(
      body.inputs as RFBInputs,
    );

    console.log("Posted rfb");
    
    return createReturnRFBFromSaveFormat(db)({})(rfb);
  };
  
export const handleUpdateRFB = (db: DB) => async (
  body: PostBody, 
  params: Record<string, string>={}
): Promise<ReturnObject<ReturnRFB>> => {
  const rfb = await updateRFB(db)(params.id, body.inputs as RFBInputs);

  return createReturnRFBFromSaveFormat(db)({})(rfb);
};

export const handleUnimplemented = (db: DB) =>
  async () => {
    return new NotImplementedException();
  };

await createFsDB({ id: CONFIG.DB_NAME });
console.log("Default DB Id:", CONFIG.DB_NAME);

let dbs = await createAllFsDBs();
console.log("These DBs were found: \n- " + Object.keys(dbs).join("\n- "));

const app = new Application();
app
  .get("/matches", handleGetRequest(dbs, handleGetMatches))
  .post("/matches", handlePostRequest(dbs, handlePostMatch, validatePostMatchRequest))
  .post(
    "/matches/:id",
    handlePostRequest(dbs, handleUpdateMatch, validateUpdateMatchRequest),
  )
  .get("/teams", handleGetRequest(dbs, handleGetTeams))
  .post("/teams", handlePostRequest(dbs, handlePostTeam, validatePostTeamRequest))
  .post(
    "/teams/:id",
    handlePostRequest(dbs, handleUpdateTeam, validateUpdateTeamRequest),
  )
  .get("/bookmakers", handleGetRequest(dbs, handleGetBookmakers))
  .post(
    "/bookmakers",
    handlePostRequest(dbs, handlePostBookmaker, validatePostBookmakerRequest),
  )
  .get("/bettypes", handleGetRequest(dbs, handleGetBetTypes))
  .post(
    "/bettypes",
    handlePostRequest(dbs, handlePostBetType, validatePostBetTypeRequest),
  ) 
  .get("/competitions", handleGetRequest(dbs, handleGetCompetitions))
  .post(
    "/competitions",
    handlePostRequest(dbs, handlePostCompetition, validatePostCompetitionRequest),
  )
  .post(
    "/competitions/:id",
    handlePostRequest(dbs, handleUpdateCompetition, validateUpdateCompetitionRequest),
  )
  .get("/countries", handleGetRequest(dbs, handleGetCountries))
  .post(
    "/countries",
    handlePostRequest(dbs, handlePostCountry, validatePostCountryRequest),
  )
  .post(
    "/countries/:id",
    handlePostRequest(dbs, handleUpdateCountry, validateUpdateCountryRequest),
  )
  .get("/sports", handleGetRequest(dbs, handleGetSports))
  .post(
    "/sports",
    handlePostRequest(dbs, handlePostSport, validatePostSportRequest),
  )
  .post(
    "/sports/:id",
    handlePostRequest(dbs, handleUpdateSport, validateUpdateSportRequest),
  )
  .get(
    "/dbs",
    handleDeleteDB(dbs)
  )
  .get("/rfbs", handleGetRequest(dbs, handleGetRFBs))
  .post(
    "/rfbs",
    handlePostRequest(dbs, handlePostRFB, validatePostRFBRequest)
  )
  .post(
    "/rfbs/:id",
    handlePostRequest(dbs, handleUpdateRFB, validateUpdateRFBRequest)
  )
  .start({ port: CONFIG.DB_PORT });

console.log(`http://localhost:${CONFIG.DB_PORT}/`);

function handleGetRequest(
  dbs: Record<string, DB>, 
  fn: (db: DB) => (params: Record<string, string>) => Promise<ReturnObject<any[]>>
) {
  return async (ctx: Context) => {
    try {
      const params = combineQueryParamsAndParams(ctx);

      const validation = validateGetParams(params);
      if (validation.successful) {
        const db = getDB(dbs, ctx);
        if (db) {
          const res = await handleRequest(fn)(db)(params);

          return ctx.json(JSON.stringify(res), 200);
        } else {
          const dbId = getDBId(ctx);
          const res = createReturnObject({
            result: [],
            messages: [createNotFoundDBWarning(dbId)]
          });
  
          console.log("Could not find DB: ", dbId);

          return ctx.json(JSON.stringify(res), 204);
        }
      } else {
        const res = createReturnObject({
          messages: validation.messages
        });
        
        console.log("Errors!", validation.messages);

        return ctx.json(JSON.stringify(res), 400)
      }      
    } catch (err) {
      console.error("an error occured", err);

      if (!(err instanceof HttpException)) {
        err = new InternalServerErrorException(
          `Internal Server Error: ${err}`,
        );
      }
      
      return ctx.json(err.message, err.status);
    }
  };
}

function handlePostRequest<ReturnT, InputT>(
  dbs: Record<string, DB>,
  fn: (db: DB) => (body: PostBody, params: Record<string, string>) => Promise<ReturnObject<ReturnT>>,
  validator: (db: DB, inputs: any, params: Record<string, string>) => Promise<ReturnObject<{
    inputs: InputT | null,
    params: Record<string, string> | null
  }>>,
) {
  return async (ctx: Context) => {
    try {
      const db = await getOrCreateDB(dbs, ctx);
      const validation = await validatePostRequest<InputT>(db, ctx, validator);
      const res = await handlePostRequestAfterValidation<ReturnT>(db, validation, fn);
      const code = getPostReturnCode(validation);

      return ctx.json(JSON.stringify(res), code);
    } catch (err) {
      console.error(err);

      if (!(err instanceof HttpException)) {
        err = new InternalServerErrorException(
          `Internal Server Error: ${err}`,
        );
      }
      
      return ctx.json(err.message, err.status);
    }
  };
}

async function getOrCreateDB(dbs: Record<string, DB>, ctx: Context) {
  const db = getDB(dbs, ctx);
  
  if (db) {
    return db;
  } else {
    const id = getDBId(ctx);
    
    dbs[id] = await createFsDB({ id });
    console.log("Added DB: ", id);

    return dbs[id];
  }
}

function getDB(dbs: Record<string, DB>, ctx: Context) {
  const id = getDBId(ctx);
  return dbs[id];
}

function getDBId(ctx: Context) {
  return ctx.queryParams.db || CONFIG.DB_NAME;
}

async function validatePostRequest<InputT>(
  db: DB,
  ctx: Context,
  validator: (db: DB, inputs: any, params: Record<string, string>) => Promise<ReturnObject<{
    inputs: InputT | null,
    params: Record<string, string> | null
  }>>
): Promise<ReturnObject<{
  body: PostBody | null,
  params: Record<string, string> | null,
}>> {
  const body = await ctx.body as PostBody;

  const params = combineQueryParamsAndParams(ctx);
  const generalValidation = validatePostBody(body);
  const specificValidation = await validator(db, body.inputs, params);

  const resultBody = generalValidation.successful && specificValidation.successful
    ? {
      ...(generalValidation.result ?? null),
      inputs: (specificValidation.result?.inputs ?? null),
    } as PostBody
    : null;

  return createReturnObject({
    messages: generalValidation.messages.concat(specificValidation.messages),
    result: {
      params: specificValidation.result?.params ?? null,
      body: resultBody 
    }
  });
}

async function handlePostRequestAfterValidation<ReturnT>(
  db: DB,
  validation: ReturnObject<{ 
    body: PostBody | null, 
    params: Record<string, string> | null 
  }>,
  fn: (db: DB) => (body: PostBody, params: Record<string, string>) => Promise<ReturnObject<ReturnT>>,
) {
  if (validation.successful) {
    const res = await handleRequest(fn)(db)(validation.result?.body, validation.result?.params);

    return createReturnObject({
      result: res.result,
      messages: validation.messages.concat(res.messages),
    });
  } else {
    console.log("Errors while posting", validation.messages);
    
    return createReturnObject({
      messages: validation.messages,
    });
  }
}

function getPostReturnCode<T>(validation: ReturnObject<T>): number {
  return validation.successful ? 201 : 400;
}

function combineQueryParamsAndParams(ctx: Context) {
  return {
    ...ctx.queryParams,
    ...ctx.params
  };
}

function handleRequest<T>(
  fn: (db: DB) => (...params: any[]) => Promise<ReturnObject<T>>,
): (db: DB) => (...params: any[]) => Promise<ReturnObject<T>> {
  // add user verification
  return fn;
}
