import { v4 as uuid } from "https://deno.land/std@0.93.0/uuid/mod.ts";
import { PropertyFilter } from "./RequestFormats.ts";

export type DateNumber = number;
export const isDateNumber = (i: any): i is DateNumber => {
  return Number.isInteger(i) && i >= 0;
};

export const isDateBeforeDate = (testDate: DateNumber, beforeDateNumber: DateNumber) => {
  return testDate < beforeDateNumber;
};

export const isDateAfterDate = (testDate: DateNumber, afterDateNumber: DateNumber) => {
  return testDate > afterDateNumber;
};

export type UUIDString = string;
export const isUUIDString = (s: any): s is UUIDString => {
  return uuid.validate(s);
};
export const generateUUIDString = (): UUIDString => {
  return uuid.generate();
};

export interface Property<T> {
  source: string;
  date: DateNumber;
  name: string;
  value: T;
}

export const isProperty = <T>(o: any): o is Property<T> => (
  typeof o?.source === "string" &&
  isDateNumber(o?.date) &&
  typeof o?.name === "string" &&
  o?.value !== undefined
);

export const createProperty = ({
  source = "",
  date,
}: {
  source?: string;
  date: DateNumber;
}) =>
  <T = null>(name: string, value: T): Property<T> => ({
    source,
    date,
    name,
    value,
  });

export const getProperty = <T>(
  properties: Property<any>[] = [],
  name: string = "",
): T | null => {
  return properties.find((p: Property<any>) => p.name === name)?.value as T ??
    null;
};

export const findProperty = <T>(name: string) => (propertyFilter: PropertyFilter) => {
  return pipe<Property<T> | null, Property<any>[]>(
    filterByName<T>(name),
    applyPropertyFilter<T>(propertyFilter),
    findNewestProperty
  );

  function filterByName<T>(name: string) {
    return (properties: Property<any>[]): Property<T>[] => (
      properties.filter((p: Property<any>) => p.name === name)
    );
  }

  function applyPropertyFilter<T>({ source, beforeDate, afterDate }: PropertyFilter) {
    return (properties: Property<T>[]): Property<T>[] => (
      properties
        .filter((p: Property<T>) => !source || p.source === source)
        .filter((p: Property<T>) => !beforeDate || isDateBeforeDate(p.date, beforeDate))
        .filter((p: Property<T>) => !afterDate || isDateAfterDate(p.date, afterDate))
    );
  }

  function findNewestProperty<T>(properties: Property<T>[]): Property<T> | null {
    return properties.reduceRight((newest: Property<T> | null, curr: Property<T>) => (
      !newest || curr.date > newest.date
        ? curr
        : newest
    ), null);
  }
}
  

export interface AnyObject {
  [x: string]: any;
}

export const pipe = <T2, T>(...fns: Function[]) => (y: T): T2 => (
  fns.reduce((x, fn) => fn(x), y) as unknown as T2
);

export const asyncPipe = <T2, T>(...fns: Function[]) => (y: T): Promise<T2> => (
  fns.reduce(async (x, fn) => fn(await x), Promise.resolve(y)) as unknown as Promise<T2>
);

export const trace = <T>(label: string) => (value: T): T => {
  console.log(label + ":", value);

  return value;
};

export const getDefaultValue = <T>(value: T, defaultValue: T) => value !== undefined ? value : defaultValue;

export const addToBeginningOfArrayIfNotIncludedYet = <T>(arr:T[]=[], val: T) => arr.includes(val) 
  ? arr
  : [val].concat(arr);

export const splitIntoArrays = <T>(fn: (e: T) => boolean, arr: T[]) => arr.reduce((acc: T[][], curr) => (
  fn(curr) 
    ? [
      acc[0].concat(curr),
      acc[1]
    ] : [
      acc[0],
      acc[1].concat(curr)
    ]
), [[], []]);