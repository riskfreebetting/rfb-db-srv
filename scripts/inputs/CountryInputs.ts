import { DB } from "../db/db.ts";
import { createNotFoundInputError, createReturnObject, createUnalignableTypeError, MessageWithInfo, ReturnObject } from "../factories/ReturnObject.ts";
import { isUUIDString, trace, UUIDString } from "../utils.ts";
import { validateInputsAndParams, validateTypes } from "./Validators.ts";

export interface CountryInputs {
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}

export const validatePostCountryRequest = async (
  db: DB,
  inputs: any,
  params: Record<string, string>
) => {
  const inputsValidation = await validateCountryInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params
    },
    messages: inputsValidation.messages
  });
};

export const validateCountryInputs = async (
  db: DB,
  inputs: any
): Promise<ReturnObject<CountryInputs>> => {
  return createReturnObject({
    result: inputs as CountryInputs,
    messages: validateTypes("Country", inputs, {
      name: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      alternativeNames: {
        type: "Record<string, string[]>",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        )
      }
    }),
  });
};

export const validateUpdateCountryInputs = async (db: DB, inputs: any) => {
  return createReturnObject({
    result: inputs as CountryInputs,
    messages: validateTypesOfCountry(inputs),
  });

  function validateTypesOfCountry(inputs: any) {
    return validateTypes("Country inputs", inputs, {
      name: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined,
      },
      alternativeNames: {
        type: "Record<string, string[]> | undefined",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        ) || x === undefined
      }
    });
  }
};

export const validateUpdateCountryParams = async (db: DB, params: Record<string, string>) => {
  const messages: MessageWithInfo[] = [];
  
  if (!isUUIDString(params.id))
    messages.push(createUnalignableTypeError({
      identifier: "params",
      propertyName: "id",
      actualType: typeof params.id,
      expectedType: "UUID"
    }));
  else if (!await db.retrieve("countries", params.id))
    messages.push(createNotFoundInputError({
      identifier: "params",
      propertyName: "id",
      dbId: db.id,
      type: "countries",
      id: params.id
    }));

  return createReturnObject({
    result: params,
    messages
  });
};

export const validateUpdateCountryRequest = validateInputsAndParams(
  validateUpdateCountryInputs,
  validateUpdateCountryParams
);