import { DB } from "../db/db.ts";
import { createNotFoundInputError, createReturnObject, createUnalignableTypeError, MessageWithInfo, ReturnObject } from "../factories/ReturnObject.ts";
import { isUUIDString, trace, UUIDString } from "../utils.ts";
import { validateInputsAndParams, validateTypes } from "./Validators.ts";

export interface SportInputs {
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}

export const validatePostSportRequest = async (
  db: DB,
  inputs: any,
  params: Record<string, string>
) => {
  const inputsValidation = await validateSportInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params
    },
    messages: inputsValidation.messages
  });
};

export const validateSportInputs = async (
  db: DB,
  inputs: any
): Promise<ReturnObject<SportInputs>> => {
  return createReturnObject({
    result: inputs as SportInputs,
    messages: validateTypes("Sport", inputs, {
      name: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      alternativeNames: {
        type: "Record<string, string[]>",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        )
      }
    }),
  });
};

export const validateUpdateSportInputs = async (db: DB, inputs: any) => {
  return createReturnObject({
    result: inputs as SportInputs,
    messages: validateTypesOfSport(inputs),
  });

  function validateTypesOfSport(inputs: any) {
    return validateTypes("Sport inputs", inputs, {
      name: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined,
      },
      alternativeNames: {
        type: "Record<string, string[]> | undefined",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        ) || x === undefined
      }
    });
  }
};

export const validateUpdateSportParams = async (db: DB, params: Record<string, string>) => {
  const messages: MessageWithInfo[] = [];
  
  if (!isUUIDString(params.id))
    messages.push(createUnalignableTypeError({
      identifier: "params",
      propertyName: "id",
      actualType: typeof params.id,
      expectedType: "UUID"
    }));
  else if (!await db.retrieve("sports", params.id))
    messages.push(createNotFoundInputError({
      identifier: "params",
      propertyName: "id",
      dbId: db.id,
      type: "sports",
      id: params.id
    }));

  return createReturnObject({
    result: params,
    messages
  });
};

export const validateUpdateSportRequest = validateInputsAndParams(
  validateUpdateSportInputs,
  validateUpdateSportParams
);