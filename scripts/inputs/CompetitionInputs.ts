import { DB } from "../db/db.ts";
import { createNotFoundInputError, createReturnObject, createUnalignableTypeError, MessageWithInfo, ReturnObject } from "../factories/ReturnObject.ts";
import { isUUIDString, trace, UUIDString } from "../utils.ts";
import { validateInputsAndParams, validateTypes } from "./Validators.ts";

export interface CompetitionInputs {
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
  sportId: UUIDString;
  countryId: UUIDString;
}

export const validatePostCompetitionRequest = async (
  db: DB,
  inputs: any,
  params: Record<string, string>
) => {
  const inputsValidation = await validateCompetitionInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params
    },
    messages: inputsValidation.messages
  });
};

export const validateCompetitionInputs = async (
  db: DB,
  inputs: any
): Promise<ReturnObject<CompetitionInputs>> => {
  return createReturnObject({
    result: inputs as CompetitionInputs,
    messages: validateTypes("Competition", inputs, {
      name: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      alternativeNames: {
        type: "Record<string, string[]>",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        )
      },
      sportId: {
        type: "string",
        fn: (x: any) => typeof x === "string"
      },
      countryId: {
        type: "string",
        fn: (x: any) => typeof x === "string"
      },
    }),
  });
};

export const validateUpdateCompetitionInputs = async (db: DB, inputs: any) => {
  return createReturnObject({
    result: inputs as CompetitionInputs,
    messages: validateTypesOfCompetition(inputs),
  });

  function validateTypesOfCompetition(inputs: any) {
    return validateTypes("Competition inputs", inputs, {
      name: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined,
      },
      alternativeNames: {
        type: "Record<string, string[]> | undefined",
        fn: (x: any) => (
          typeof x === "object" && 
          x !== null && 
          Object.values(x).every(x => (
            Array.isArray(x) && 
            x?.every(x => typeof x === "string")
          ))
        ) || x === undefined
      },
      sportId: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined
      },
      countryId: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined
      },
    });
  }
};

export const validateUpdateCompetitionParams = async (db: DB, params: Record<string, string>) => {
  const messages: MessageWithInfo[] = [];
  
  if (!isUUIDString(params.id))
    messages.push(createUnalignableTypeError({
      identifier: "params",
      propertyName: "id",
      actualType: typeof params.id,
      expectedType: "UUID"
    }));
  else if (!await db.retrieve("competitions", params.id))
    messages.push(createNotFoundInputError({
      identifier: "params",
      propertyName: "id",
      dbId: db.id,
      type: "competitions",
      id: params.id
    }));

  if (params.sportId) {
    if (!isUUIDString(params.sportId))
      messages.push(createUnalignableTypeError({
        identifier: "params",
        propertyName: "sportId",
        actualType: typeof params.sportId,
        expectedType: "UUID"
      }));
    else if (!await db.retrieve("sports", params.sportId))
      messages.push(createNotFoundInputError({
        identifier: "params",
        propertyName: "sportId",
        dbId: db.id,
        type: "sports",
        id: params.sportId
      }));
  }
  
  if (params.countryId) {
    if (!isUUIDString(params.countryId))
      messages.push(createUnalignableTypeError({
        identifier: "params",
        propertyName: "countryId",
        actualType: typeof params.countryId,
        expectedType: "UUID"
      }));
    else if (!await db.retrieve("countries", params.countryId))
      messages.push(createNotFoundInputError({
        identifier: "params",
        propertyName: "countryId",
        dbId: db.id,
        type: "countries",
        id: params.countryId
      }));
  }

  return createReturnObject({
    result: params,
    messages
  });
};

export const validateUpdateCompetitionRequest = validateInputsAndParams(
  validateUpdateCompetitionInputs,
  validateUpdateCompetitionParams
);