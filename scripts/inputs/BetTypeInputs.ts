import { DB } from "../db/db.ts";
import {
  createMessageWithInfo,
  createReturnObject,
  MessageWithInfo,
  ReturnObject,
} from "../factories/ReturnObject.ts";
import { validateTypes } from "./Validators.ts";

export interface BetTypeInputs {
  name: string;
  outcomeNames: string[];
}

export const validatePostBetTypeRequest = async (
  db: DB,
  inputs: any,
  params: Record<string, string>
) => {
  const inputsValidation = await validateBetTypeInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params
    },
    messages: inputsValidation.messages
  });
};

export const validateBetTypeInputs = async (
  db: DB,
  inputs: any,
): Promise<ReturnObject<BetTypeInputs>> => {
  return createReturnObject({
    result: inputs as BetTypeInputs,
    messages: validateTypes("BetType", inputs, {
      name: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      outcomeNames: {
        type: "string[]",
        fn: (x: any) =>
          Array.isArray(x) && x.every((e: any) => typeof e === "string"),
      },
    }),
  });
};
