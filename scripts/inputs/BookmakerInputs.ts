import { DB } from "../db/db.ts";
import { createReturnObject, ReturnObject } from "../factories/ReturnObject.ts";
import { validateTypes } from "./Validators.ts";
import { TaxTypes } from "../db/saveFormats/BookmakerSaveFormat.ts";
import { MessageWithInfo, createUnalignableTypeError } from "../factories/ReturnObject.ts";

export interface BookmakerInputs {
  name: string;
  tax: keyof typeof TaxTypes;
}

export const validatePostBookmakerRequest = async (
  db: DB,
  inputs: Record<string, string>,
  params: Record<string, string>
) => {
  const inputsValidation = await validateBookmakerInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params
    },
    messages: inputsValidation.messages
  });
};

export const validateBookmakerInputs = async (
  db: DB,
  inputs: Record<string, any>,
): Promise<ReturnObject<BookmakerInputs>> => {
  const messagesOfTypes = validateTypesOfBookmaker(inputs);
  const messagesOfValues = await validateValuesOfBookmaker(db, inputs);

  return createReturnObject({
    result: inputs as BookmakerInputs,
    messages: messagesOfTypes.concat(messagesOfValues),
  });

  function validateTypesOfBookmaker(inputs: Record<string, any>) {
    return validateTypes("Bookmaker", inputs, {
      name: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      tax: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
    });
  }
};

export const validateValuesOfBookmaker = (db: DB, inputs: Record<string, any>) => {
  let messages: MessageWithInfo[] = [];
  
  if (!(inputs?.tax in TaxTypes))
    messages.push(createUnalignableTypeError({
      identifier: "Bookmaker inputs",
      propertyName: "tax",
      actualType: inputs?.tax,
      expectedType: "TaxTypes (NONE | 5% STAKES | 5% WINNINGS)",
    }));

  return messages;
};