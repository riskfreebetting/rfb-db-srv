import { DB } from "../db/db.ts";
import {
  createMessageWithInfo,
  createReturnObject,
  createUnalignableTypeError,
  MessageWithInfo,
  ReturnObject,
} from "../factories/ReturnObject.ts";

export const validateTypes = (
  identifier: string,
  o: Record<string, any> = {},
  validators: Record<string, {
    type: string;
    fn: (x: any) => boolean;
  }> = {},
): MessageWithInfo[] => {
  return typeof o === "object" && o
    ? Object.keys(validators).reduce(
      (acc: MessageWithInfo[], key: keyof typeof validators) =>
        acc.concat(
          validators[key].fn(o[key]) ? [] : createUnalignableTypeError({
            identifier,
            propertyName: key,
            actualType: typeof o[key],
            expectedType: validators[key].type,
          }),
        ),
      [],
    )
    : [
      createUnalignableTypeError({
        identifier,
        propertyName: "self",
        actualType: typeof o + " | null",
        expectedType: "object",
      }),
    ];
};

export const validateInputsAndParams = <InputT>(
  validateMatchInputs: (
    db: DB,
    inputs: any,
  ) => Promise<ReturnObject<InputT>>,
  validateMatchParams: (
    db: DB,
    params: Record<string, string>
  ) => Promise<ReturnObject<Record<string, string>>>
) => async (
  db: DB, 
  inputs: any, 
  params: Record<string, string>
) => {
  const inputsValidation = await validateMatchInputs(db, inputs);
  const paramsValidation = await validateMatchParams(db, params);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params: paramsValidation.result,
    },
    messages: inputsValidation.messages.concat(paramsValidation.messages),
  });
};