import { DB } from "../db/db.ts";
import {
  createMessageWithInfo,
  createReturnObject,
  MessageWithInfo,
  ReturnObject,
  createUnalignableTypeError,
  createNotFoundInputError,
  createNumberNotInRightRangeError
} from "../factories/ReturnObject.ts";
import { validateInputsAndParams, validateTypes } from "./Validators.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import { MatchSaveFormat, retrieveMatchById } from "../db/saveFormats/MatchSaveFormat.ts"; 
import { BetTypeSaveFormat, retrieveBetTypeById } from "../db/saveFormats/BetTypeSaveFormat.ts"; 
import { retrieveBookmakerById } from "../db/saveFormats/BookmakerSaveFormat.ts"; 

export interface RFBInputs {
  matchId: UUIDString;
  betTypeId: UUIDString;
  bets: RFBBet[];
}

export interface RFBBet {
  profit: number;
  outcomes: Record<UUIDString, {
    bookmaker: UUIDString,
    odd: number
  }>;
} 

export const validatePostRFBRequest = async (
  db: DB,
  inputs: any,
  params: Record<string, string>
) => {
  const res = await validatePostRFBInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputs as RFBInputs,
      params
    },
    messages: res.messages
  });
};

export const validatePostRFBInputs = async (
  db: DB,
  inputs: any
) => {
  const messagesOfTypes = validateTypesOfRFB(inputs);
  const messagesOfValues = await validateValuesOfRFB(db, inputs);

  return createReturnObject({
    result: inputs,
    messages: messagesOfTypes.concat(messagesOfValues),
  });

  function validateTypesOfRFB(inputs: any) {
    return validateTypes("RFB", inputs, {
      matchId: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      betTypeId: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      bets: {
        type: `{
          profit: number,
          outcomes: {
            [string]: {
              bookmaker: string,
              odd: number
            }
          }
        }[]`,
        fn: (x: any) => (
          Array.isArray(x) &&
          x.every(isBet)
        )
      }
    });
  }
};

export const validateUpdateRFBInputs = async (db: DB, inputs: any) => {
  const messagesOfTypes = validateTypesOfRFBUpdate(inputs);
  const messagesOfValues = await validateValuesOfRFB(db, inputs);

  return createReturnObject({
    result: inputs as RFBInputs,
    messages: messagesOfTypes.concat(messagesOfValues),
  });

  function validateTypesOfRFBUpdate(inputs: any) {
    return validateTypes("RFB Updates", inputs, {
      matchId: {
        type: "string | undefined",
        fn: (x: any) => x === undefined || typeof x === "string",
      },
      betTypeId: {
        type: "string | undefined",
        fn: (x: any) => x === undefined || typeof x === "string",
      },
      bets: {
        type: `{
          profit: number,
          outcomes: {
            [string]: {
              bookmaker: string,
              odd: number
            }
          }
        }[] | undefined`,
        fn: (x: any) => (
          x === undefined || 
          Array.isArray(x) &&
          x.every(isBet)
        )
      }
    });
  }
};

function isBet(e: any) {
  return (
    typeof e?.profit === "number" &&
    typeof e?.outcomes === "object" &&
    Object.values(e?.outcomes).every(isOutcome)
  );
}

function isOutcome(e: any) {
  return (v: any) => (
    typeof v?.bookmaker === "string" &&
    typeof v?.odd === "number"
  )
}

export const validateUpdateRFBParams = async (db: DB, params: Record<string, string>) => {
  const messages: MessageWithInfo[] = [];
  
  if (!isUUIDString(params.id))
    messages.push(createUnalignableTypeError({
      identifier: "params",
      propertyName: "id",
      actualType: typeof params.id,
      expectedType: "UUID"
    }));
  else if (!await db.retrieve("rfbs", params.id))
    messages.push(createNotFoundInputError({
      identifier: "params",
      propertyName: "id",
      dbId: db.id,
      type: "rfbs",
      id: params.id
    }));

  return createReturnObject({
    result: params,
    messages
  });
};

export const validateUpdateRFBRequest = validateInputsAndParams(
  validateUpdateRFBInputs,
  validateUpdateRFBParams
);

async function validateValuesOfRFB(db: DB, inputs: any) {
  let messages: MessageWithInfo[] = [];

  if (inputs?.matchId)
    messages = messages.concat(await validateIdField<MatchSaveFormat>(inputs, db, "matchId", retrieveMatchById));
  
  if (inputs?.betTypeId)
    messages = messages.concat(await validateIdField<BetTypeSaveFormat>(inputs, db, "betTypeId", retrieveBetTypeById));
  
  if (isUUIDString(inputs?.betTypeId) && inputs?.bets) {
    const betType = await retrieveBetTypeById(db)(inputs?.betTypeId);
    
    messages = messages.concat(await inputs?.bets?.reduce(
      validateBets(db, betType), 
      Promise.resolve([])
    ) ?? []);  
  }
  
  return messages;
}

async function validateIdField<T>(
  inputs: any, 
  db: DB,
  idField: string, 
  retrieveById: (db: DB) => (id: UUIDString) => Promise<T | null>
) {
  if (isUUIDString(inputs?.[idField])) {
    if (await retrieveById(db)(inputs?.[idField]) === null) {
      return createNotFoundInputError({
        identifier: "RFB inputs",
        propertyName: idField,
        dbId: db.id,
        id: inputs?.[idField],
      });
    }
  } else {
    return createUnalignableTypeError({
      identifier: "RFB inputs",
      propertyName: idField,
      actualType: inputs?.[idField],
      expectedType: "UUID",
    });
  }

  return [];
}

function validateBets(db: DB, betType: BetTypeSaveFormat) {
  return async (acc: Promise<MessageWithInfo[]>, betInputs: any, betNr: number) => {
    let betMessages: MessageWithInfo[] = await acc;

    betMessages = await Object.entries(betInputs?.outcomes).reduce(async (acc: Promise<MessageWithInfo[]>, [outcomeId, outcome]: any) => {
      let messages = await acc;

      if (isUUIDString(outcomeId)) {
        if (betType.outcomeNames.every(outcome => outcome.id !== outcomeId))
          messages = messages.concat(createNotFoundInputError({
            identifier: "RFB inputs",
            propertyName: `key of bets[${betNr}][${outcomeId}]`,
            type: `outcomeName in BetType {{${betType}}}`,
            id: outcomeId,
            dbId: db.id,
          }));
      } else {
        messages = messages.concat(createUnalignableTypeError({
          identifier: "RFB inputs",
          propertyName: `key of bets[${betNr}][${outcomeId}]`,
          actualType: typeof outcomeId,
          expectedType: "UUID",
        }));
      }

      if (isUUIDString(outcome?.bookmaker)) {
        if (await retrieveBookmakerById(db)(outcome?.bookmaker) === null)
          messages = messages.concat(createNotFoundInputError({
            identifier: "RFB inputs",
            propertyName: `bets[${betNr}][${outcomeId}].bookmaker`,
            id: outcomeId,
            dbId: db.id,
          }));
      } else {
        messages = messages.concat(createUnalignableTypeError({
          identifier: "RFB inputs",
          propertyName: `bets[${betNr}][${outcomeId}].bookmaker`,
          actualType: typeof outcome?.bookmaker,
          expectedType: "UUID",
        }));
      }

      if (outcome?.odd < 1) {
        messages = messages.concat(createNumberNotInRightRangeError({
          identifier: "RFB inputs",
          propertyName: `bets[${betNr}][${outcomeId}].odd`,
          actual: outcome?.odd,
          expectedMin: 1,
        }));
      }

      return messages;
    }, Promise.resolve(betMessages));
    
    return betMessages;
  };
}