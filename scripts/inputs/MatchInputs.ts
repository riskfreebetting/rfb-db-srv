import { DB } from "../db/db.ts";
import { retrieveBetTypeById } from "../db/saveFormats/BetTypeSaveFormat.ts";
import { retrieveBookmakerById } from "../db/saveFormats/BookmakerSaveFormat.ts";
import { retrieveCompetitionById } from "../db/saveFormats/CompetitionSaveFormat.ts";
import { retrieveTeamById } from "../db/saveFormats/TeamSaveFormat.ts";
import {
  createNotFoundInputError,
  createNumberNotInRightRangeError,
  createReturnObject,
  createUnalignableTypeError,
  createOutcomeOfBetTypeNotSuppliedError,
  MessageWithInfo,
  ReturnObject,
} from "../factories/ReturnObject.ts";
import {
  DateNumber,
  isDateNumber,
  isUUIDString,
  trace,
  UUIDString,
} from "../utils.ts";
import { validateInputsAndParams, validateTypes } from "./Validators.ts";

export interface MatchInputs {
  teams: MatchTeamInputs[];
  result?: number[] | null;
  status: keyof typeof MatchStatusInputs;
  currentMinute?: number | null;
  startDate?: DateNumber | null;
  endDate?: DateNumber | null;
  odds: OddsInputs[];
  competitionId: UUIDString;
}

export interface MatchTeamInputs {
  teamId: UUIDString;
}

export interface OddsInputs {
  bookmakerId: UUIDString;
  bets: BetInputs[];
}

export interface BetInputs {
  typeId: UUIDString;
  date: DateNumber;
  outcomes: OutcomeInputs[];
}

export interface OutcomeInputs {
  name: UUIDString;
  odd: number;
}

export enum MatchStatusInputs {
  "TBD" = "Time To Be Defined",
  "NS" = "Not Started",
  "1H" = "First Half, Kick Off",
  "HT" = "Halftime",
  "2H" = "Second Half, 2nd Half Started",
  "ET" = "Extra Time",
  "P" = "Penalty In Progress",
  "FT" = "Match Finished",
  "AET" = "Match Finished After Extra Time",
  "PEN" = "Match Finished After Penalty",
  "BT" = "Break Time (in Extra Time)",
  "SUSP" = "Match Suspended",
  "INT" = "Match Interrupted",
  "PST" = "Match Postponed",
  "CANC" = "Match Cancelled",
  "ABD" = "Match Abandoned",
  "AWD" = "Technical Loss",
  "WO" = "WalkOver",
}

export const validateMatchInputs = async (
  db: DB,
  inputs: any,
): Promise<ReturnObject<MatchInputs>> => {
  const messagesOfTypes = validateTypesOfMatch(inputs);
  const messagesOfValues = await validateValuesOfMatch(db, inputs);

  return createReturnObject({
    result: inputs as MatchInputs,
    messages: messagesOfTypes.concat(messagesOfValues),
  });

  function validateTypesOfMatch(inputs: any) {
    return validateTypes("Match inputs", inputs, {
      teams: {
        type: "{ teamId: string }[]",
        fn: (x: any) =>
          Array.isArray(x) &&
          x.every((e: any) => typeof e?.teamId === "string"),
      },
      result: {
        type: "number[] | null | undefined",
        fn: (x: any) =>
          (Array.isArray(x) && x.every((e: any) => typeof e === "number")) ||
          x == null,
      },
      status: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
      currentMinute: {
        type: "number | null | undefined",
        fn: (x: any) => typeof x === "number" || x == null,
      },
      startDate: {
        type: "string | null | undefined",
        fn: (x: any) => Number.isInteger(x) || x == null,
      },
      endDate: {
        type: "string | null | undefined",
        fn: (x: any) => Number.isInteger(x) || x == null,
      },
      odds: {
        type: `{ 
          bookmakerId: string, 
          bets: { 
            typeId: string,
            date: integer,
            outcomes: {
              name: string,
              odd: number
            }
          }[] 
        }[]`,
        fn: (x: any) => (
          Array.isArray(x) &&
          x.every((e: any) => (
            typeof e?.bookmakerId === "string" &&
            Array.isArray(e?.bets) &&
            e?.bets.every((e: any) => (
              typeof e?.typeId === "string" &&
              Number.isInteger(e?.date) &&
              Array.isArray(e?.outcomes) &&
              e?.outcomes.every((e: any) => (
                typeof e?.name === "string" &&
                typeof e?.odd === "number"
              ))
            ))
          ))
        ),
      },
      competitionId: {
        type: "string",
        fn: (x: any) => typeof x === "string",
      },
    });
  }
};

export const validatePostMatchRequest = async (
  db: DB, 
  inputs: any, 
  params: Record<string, string>
) => {
  const inputsValidation = await validateMatchInputs(db, inputs);

  return createReturnObject({
    result: {
      inputs: inputsValidation.result,
      params,
    },
    messages: inputsValidation.messages,
  });
};

export const validateUpdateMatchInputs = async (db: DB, inputs: any) => {
  const messagesOfTypes = validateTypesOfMatch(inputs);
  const messagesOfValues = await validateValuesOfMatch(db, inputs);

  return createReturnObject({
    result: inputs as MatchInputs,
    messages: messagesOfTypes.concat(messagesOfValues),
  });

  function validateTypesOfMatch(inputs: any) {
    return validateTypes("Match inputs", inputs, {
      teams: {
        type: "{ teamId: string }[] | undefined",
        fn: (x: any) =>
          (Array.isArray(x) &&
            x.every((e: any) => typeof e?.teamId === "string")) ||
          x === undefined,
      },
      result: {
        type: "number[] | null | undefined",
        fn: (x: any) =>
          (Array.isArray(x) && x.every((e: any) => typeof e === "number")) ||
          x == null,
      },
      status: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined,
      },
      currentMinute: {
        type: "number | null | undefined",
        fn: (x: any) => typeof x === "number" || x == null,
      },
      startDate: {
        type: "string | null | undefined",
        fn: (x: any) => Number.isInteger(x) || x == null,
      },
      endDate: {
        type: "string | null | undefined",
        fn: (x: any) => Number.isInteger(x) || x == null,
      },
      odds: {
        type: `{ 
          bookmakerId: string, 
          bets: { 
            typeId: string,
            date: string,
            outcomes: {
              name: string,
              odd: number
            }
          }[] 
        }[] | undefined`,
        fn: (x: any) => (
          (Array.isArray(x) &&
            x.every((e: any) => (
              typeof e?.bookmakerId === "string" &&
              Array.isArray(e?.bets) &&
              e?.bets.every((e: any) => (
                typeof e?.typeId === "string" &&
                Number.isInteger(e?.date) &&
                Array.isArray(e?.outcomes) &&
                e?.outcomes.every((e: any) => (
                  typeof e?.name === "string" &&
                  typeof e?.odd === "number"
                ))
              ))
            ))) ||
          x === undefined
        ),
      },
      competitionId: {
        type: "string | undefined",
        fn: (x: any) => typeof x === "string" || x === undefined,
      },
    });
  }
};

export const validateUpdateMatchParams = async (db: DB, params: Record<string, string>) => {
  const messages: MessageWithInfo[] = [];
  if (!isUUIDString(params.id))
    messages.push(createUnalignableTypeError({
      identifier: "params",
      propertyName: "id",
      actualType: typeof params.id,
      expectedType: "UUID"
    }));
  else if (!await db.retrieve("matches", params.id))
    messages.push(createNotFoundInputError({
      identifier: "params",
      propertyName: "id",
      dbId: db.id,
      type: "matches",
      id: params.id
    }));

  return createReturnObject({
    result: params,
    messages
  });
};

export const validateUpdateMatchRequest = validateInputsAndParams(
  validateUpdateMatchInputs,
  validateUpdateMatchParams
);

async function validateValuesOfMatch(db: DB, inputs: any) {
  let messages: MessageWithInfo[] = [];

  if (Array.isArray(inputs?.teams)) {
    messages = await inputs.teams.reduce(
      async (acc: Promise<MessageWithInfo[]>, team: any, i: number) => {
        let teamMessages = await acc;
        if (isUUIDString(team?.teamId)) {
          if (await retrieveTeamById(db)(team?.teamId) === null) {
            return teamMessages.concat(createNotFoundInputError({
              identifier: "Match inputs",
              propertyName: `teams[${i}].teamId`,
              type: "teams",
              dbId: db.id,
              id: team?.teamId,
            }));
          }
        } else {
          return teamMessages.concat(createUnalignableTypeError({
            identifier: "Match inputs",
            propertyName: `teams[${i}].teamId`,
            actualType: team?.teamId,
            expectedType: "UUID",
          }));
        }

        return teamMessages;
      },
      messages,
    );
  }

  if (inputs?.status !== undefined && !(inputs?.status in MatchStatusInputs)) {
    messages.push(createUnalignableTypeError({
      identifier: "Match inputs",
      propertyName: "status",
      actualType: inputs?.status,
      expectedType: "MatchStatus",
    }));
  }

  if (inputs?.startDate != undefined && !isDateNumber(inputs?.startDate)) {
    messages.push(createUnalignableTypeError({
      identifier: "Match inputs",
      propertyName: "startDate",
      actualType: inputs?.startDate,
      expectedType: "Date in ms",
    }));
  }

  if (inputs?.endDate != undefined && !isDateNumber(inputs?.endDate)) {
    messages.push(createUnalignableTypeError({
      identifier: "Match inputs",
      propertyName: "endDate",
      actualType: inputs?.endDate,
      expectedType: "Date in ms",
    }));
  }

  if (Array.isArray(inputs?.odds)) {
    messages = await inputs.odds.reduce(
      validateValuesOfOdds(db, "Match inputs"),
      messages,
    );
  }

  if (inputs?.competitionId) {
    if (!isUUIDString(inputs?.competitionId))
      messages.push(createUnalignableTypeError({
        identifier: "Match inputs",
        propertyName: "competitionId",
        actualType: typeof inputs?.competitionId,
        expectedType: "UUID"
      }));
    else if (!await retrieveCompetitionById(db)(inputs?.competitionId))
      messages.push(createNotFoundInputError({
        identifier: "params",
        propertyName: "competitionId",
        dbId: db.id,
        type: "competitions",
        id: inputs?.competitionId
    }));
  }

  return Promise.all(messages);
}

function validateValuesOfOdds(db: DB, identifier: string) {
  return async (
    acc: Promise<MessageWithInfo[]>,
    inputs: any,
    oddsNr: number,
  ) => {
    let messages = await acc;
    if (isUUIDString(inputs?.bookmakerId)) {
      if (await retrieveBookmakerById(db)(inputs?.bookmakerId) === null) {
        messages.push(createNotFoundInputError({
          identifier,
          propertyName: `odds[${oddsNr}].bookmakerId`,
          type: "bookmaker",
          id: inputs?.bookmakerId,
          dbId: db.id,
        }));
      }
    } else {
      messages.push(createUnalignableTypeError({
        identifier,
        propertyName: `odds[${oddsNr}].bookmakerId`,
        actualType: inputs?.bookmakerId,
        expectedType: "UUID",
      }));
    }

    if (Array.isArray(inputs?.bets)) {
      messages = await inputs.bets.reduce(
        validateValuesOfBet(
          db,
          `${identifier}.odds[${oddsNr}]`,
        ),
        messages,
      );
    }

    return Promise.all(messages);
  };
}

function validateValuesOfBet(db: DB, identifier: string) {
  return async (
    acc: Promise<MessageWithInfo[]>,
    inputs: any,
    betNr: number,
  ) => {
    let messages = await acc;

    let betType = await retrieveBetTypeById(db)(inputs?.typeId);
    if (isUUIDString(inputs?.typeId)) {
      if (betType === null) {
        messages.push(createNotFoundInputError({
          identifier,
          propertyName: `bets[${betNr}].typeId`,
          type: "betType",
          id: inputs?.typeId,
          dbId: db.id,
        }));
      }
    } else {
      messages.push(createUnalignableTypeError({
        identifier,
        propertyName: `bets[${betNr}].typeId`,
        actualType: inputs?.typeId,
        expectedType: "UUID",
      }));
    }

    if (!isDateNumber(inputs?.date)) {
      messages.push(createUnalignableTypeError({
        identifier,
        propertyName: `bets[${betNr}].date`,
        actualType: inputs?.date,
        expectedType: "Date in ms",
      }));
    }

    if (Array.isArray(inputs?.outcomes)) {
      messages = messages.concat(betType?.outcomeNames.flatMap((
        { id, name }: { id: UUIDString; name: string },
      ) => {
        if (inputs.outcomes.every((input: any) => input?.name !== id)) {
          return createOutcomeOfBetTypeNotSuppliedError({
            identifier,
            propertyName: `bets[${betNr}]`,
            betTypeName: betType.name,
            outcomeName: name,
            outcomeId: id,
            dbId: db.id,
          });
        }

        return [];
      }));

      messages = messages.concat(inputs.outcomes.flatMap((
        outcomeInputs: any,
        outcomeNr: number,
      ) => {
        let outcomeMessages: MessageWithInfo[] = [];
        if (isUUIDString(outcomeInputs?.name)) {
          if (
            betType?.outcomeNames.every((o) => o.id !== outcomeInputs?.name)
          ) {
            outcomeMessages.push(createNotFoundInputError({
              identifier,
              propertyName: `outcomes[${outcomeNr}].name`,
              type: `outcomeName in BetType {{${betType}}}`,
              id: outcomeInputs?.name,
              dbId: db.id,
            }));
          }
        } else {
          outcomeMessages.push(createUnalignableTypeError({
            identifier,
            propertyName: `outcomes[${outcomeNr}].name`,
            actualType: outcomeInputs?.name,
            expectedType: "UUID",
          }));
        }

        if (outcomeInputs?.odd < 1) {
          outcomeMessages.push(createNumberNotInRightRangeError({
            identifier,
            propertyName: `outcomes[${outcomeNr}].odd`,
            actual: outcomeInputs.odd,
            expectedMin: 1,
          }));
        }

        return outcomeMessages;
      }));
    }

    return Promise.all(messages);
  };
}
