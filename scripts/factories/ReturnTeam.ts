import { DB } from "../db/db.ts";
import {
  retrieveTeamById,
  TeamSaveFormat,
} from "../db/saveFormats/TeamSaveFormat.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnTeam {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}

export const isReturnTeam = (o: any): o is ReturnTeam => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" &&
  Object.keys(o?.alternativeNames ?? {}).every(x => isUUIDString(x)) && 
  Object.values(o?.alternativeNames ?? {}).every(x => (
    Array.isArray(x) &&
    x?.every(x => typeof x === "string")
  ))
);

export const createReturnTeamById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnTeam>> => {
    const savedTeam: TeamSaveFormat = await retrieveTeamById(db)(id);

    if (savedTeam) {
      return createReturnObject({
        result: createReturnTeamFromSaveFormat(savedTeam),
      });
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "team",
            id,
          }),
        ],
      });
    }
  };

export const createReturnTeamFromSaveFormat = (
  saved: TeamSaveFormat,
): ReturnTeam => (
  createReturnTeam({
    id: saved.id,
    name: saved.name,
    alternativeNames: saved.alternativeNames
  })
);

export const createReturnTeam = ({
  id,
  name,
  alternativeNames
}: {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}) => ({
  id,
  name,
  alternativeNames
});
