import { DateNumber, getProperty, isDateNumber, trace, UUIDString } from "../utils.ts";
import {
  createReturnMatchTeamById,
  isReturnMatchTeam,
  ReturnMatchTeam,
} from "./ReturnMatchTeam.ts";
import {
  createReturnOddsById,
  isReturnOdds,
  ReturnOdds,
} from "./ReturnOdds.ts";
import {
  MatchSaveFormat,
  SaveMatchStatus,
  retrieveMatchById
} from "../db/saveFormats/MatchSaveFormat.ts";
import { DB } from "../db/db.ts";
import {
  createReturnObject,
  createNotFoundError,
  MessageWithInfo,
  ReturnObject,
} from "./ReturnObject.ts";
import { PropertyFilter } from "../RequestFormats.ts";
import { createReturnCompetitionById, isReturnCompetition, ReturnCompetition } from "./ReturnCompetition.ts";

export interface ReturnMatch {
  id: UUIDString;
  name: string;
  result: number[] | null;
  status: keyof typeof MatchStatus;
  currentMinute: number | null;
  startDate: DateNumber | null;
  endDate: DateNumber | null;
  odds: ReturnOdds[];
  teams: ReturnMatchTeam[];
  competition: ReturnCompetition | null;
}

export const isReturnMatch = (o: any): o is ReturnMatch => {
  return (
    typeof o?.id === "string" &&
    typeof o?.name === "string" &&
    (Array.isArray(o?.teams) && o?.teams.length > 0 &&
      o?.teams.every(isReturnMatchTeam)) &&
    o?.status in MatchStatus &&
    (typeof o?.currentMinute === "number" || o?.currentMinute === null) &&
    (isDateNumber(o?.startDate) || o?.startDate === null) &&
    (isDateNumber(o?.endDate) || o?.endDate === null) &&
    ((Array.isArray(o?.odds) && o?.odds.every(isReturnOdds))) &&
    ((
      Array.isArray(o?.result) &&
      o?.result.every((v: any) => typeof v === "number") 
    ) || o?.result === null) &&
    (isReturnCompetition(o?.competition) || o?.competition === null)
  );
};

export const createReturnMatchById = (db: DB) => (propertyFilter: PropertyFilter) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnMatch>> => {
    const savedMatch: MatchSaveFormat = await retrieveMatchById(db)(id);
    if (savedMatch) {
      return createReturnMatchFromSaveFormat(db)(propertyFilter)(savedMatch);
    } else {
      return createReturnObject<ReturnMatch>({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "match",
            id,
          }),
        ],
      });
    }
  };

export const createReturnMatchFromSaveFormat = (db: DB) => (propertyFilter: PropertyFilter) =>
  async (m: MatchSaveFormat): Promise<ReturnObject<ReturnMatch>> => {
    let messages: MessageWithInfo[] = [];

    const competition = await createReturnCompetitionById(db)(m.competitionId);
    messages = messages.concat(competition.messages);

    const teams = await Promise.all(
      m.childIds.map(createReturnMatchTeamById(db)),
    );
    messages = teams.reduce((acc, res) => acc.concat(res.messages), messages);

    const odds = await Promise.all(m.oddsIds.map(createReturnOddsById(db)(propertyFilter)));
    messages = odds.reduce((acc, res) => acc.concat(res.messages), messages);

    const returnMatch = createReturnMatch({
      id: m.id,
      name: m.name,
      teams: teams
        .filter((res) => res.result != null)
        .map((t) => t.result) as ReturnMatchTeam[],
      result: m.result,
      status: isMatchStatus(m.status) ? m.status : "TBD",
      startDate: m.startDate,
      endDate: getProperty<DateNumber>(m.properties, "endDate"),
      currentMinute: getProperty<number>(m.properties, "currentMinute"),
      odds: odds
        .filter((res) => res.result != null)
        .map((t) => t.result) as ReturnOdds[],
      competition: competition.result
    });

    return createReturnObject({
      messages,
      result: returnMatch,
    });
  };

export const createReturnMatch = ({
  id,
  name,
  teams,
  result = null,
  status = "TBD",
  currentMinute = null,
  startDate = null,
  endDate = null,
  odds = [],
  competition = null
}: {
  id: UUIDString;
  name: string;
  teams: ReturnMatchTeam[];
  result?: number[] | null;
  status?: keyof typeof SaveMatchStatus;
  currentMinute?: number | null;
  startDate?: DateNumber | null;
  endDate?: DateNumber | null;
  odds?: ReturnOdds[];
  competition: ReturnCompetition | null;
}): ReturnMatch => {
  return {
    id,
    name,
    result,
    status,
    currentMinute,
    startDate,
    endDate,
    odds,
    teams,
    competition
  };
};

export enum MatchStatus {
  "TBD" = "Time To Be Defined",
  "NS" = "Not Started",
  "1H" = "First Half, Kick Off",
  "HT" = "Halftime",
  "2H" = "Second Half, 2nd Half Started",
  "ET" = "Extra Time",
  "P" = "Penalty In Progress",
  "FT" = "Match Finished",
  "AET" = "Match Finished After Extra Time",
  "PEN" = "Match Finished After Penalty",
  "BT" = "Break Time (in Extra Time)",
  "SUSP" = "Match Suspended",
  "INT" = "Match Interrupted",
  "PST" = "Match Postponed",
  "CANC" = "Match Cancelled",
  "ABD" = "Match Abandoned",
  "AWD" = "Technical Loss",
  "WO" = "WalkOver",
}

function isMatchStatus(s: string): s is MatchStatus {
  return Object.keys(MatchStatus).find((key) => key === s) != null;
}
