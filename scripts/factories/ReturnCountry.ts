import { DB } from "../db/db.ts";
import {
  retrieveCountryById,
  CountrySaveFormat,
} from "../db/saveFormats/CountrySaveFormat.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnCountry {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}

export const isReturnCountry = (o: any): o is ReturnCountry => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" &&
  Object.keys(o?.alternativeNames ?? {}).every(x => isUUIDString(x)) && 
  Object.values(o?.alternativeNames ?? {}).every(x => (
    Array.isArray(x) &&
    x?.every(x => typeof x === "string")
  ))
);

export const createReturnCountryById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnCountry>> => {
    const savedCountry: CountrySaveFormat = await retrieveCountryById(db)(id);

    if (savedCountry) {
      return createReturnObject({
        result: createReturnCountryFromSaveFormat(savedCountry),
      });
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "country",
            id,
          }),
        ],
      });
    }
  };

export const createReturnCountryFromSaveFormat = (
  saved: CountrySaveFormat,
): ReturnCountry => (
  createReturnCountry({
    id: saved.id,
    name: saved.name,
    alternativeNames: saved.alternativeNames
  })
);

export const createReturnCountry = ({
  id,
  name,
  alternativeNames
}: {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}) => ({
  id,
  name,
  alternativeNames
});
