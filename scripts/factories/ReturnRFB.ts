import { UUIDString, trace, isUUIDString } from "../utils.ts";
import {
  createReturnMatchById,
  isReturnMatch,
  ReturnMatch,
} from "./ReturnMatch.ts";
import {
  createReturnBetTypeById,
  isReturnBetType,
  ReturnBetType,
} from "./ReturnBetType.ts";
import {
  createReturnBookmakerById,
  isReturnBookmaker,
  ReturnBookmaker,
} from "./ReturnBookmaker.ts";
import {
  RFBSaveFormat,
  RFBBetSaveFormat
} from "../db/saveFormats/RFBSaveFormat.ts";
import { DB } from "../db/db.ts";
import {
  createReturnObject,
  createOutcomeOfBetTypeNotSuppliedError,
  MessageWithInfo,
  ReturnObject,
} from "./ReturnObject.ts";
import { PropertyFilter } from "../RequestFormats.ts";

export interface ReturnRFB {
  id: UUIDString;
  isAvailable: boolean;
  match: ReturnMatch | null;
  betType: ReturnBetType | null;
  bets: RFBBet[];
}

interface RFBBet {
  profit: number;
  isAvailable: boolean;
  outcomes: Record<string, RFBOutcome>;
}

interface RFBOutcome {
  bookmaker: ReturnBookmaker | null;
  odd: number;
}

export const isReturnRFB = (o: any): o is ReturnRFB => {
  return (
    isUUIDString(o?.id) &&
    typeof o?.isAvailable === "boolean" &&
    Array.isArray(o?.bets) && o?.bets.every((bet: any) => (
      typeof bet?.profit === "number" &&
      typeof bet?.isAvailable === "boolean" &&
      typeof bet?.outcomes === "object" &&
      Object.values(bet?.outcomes).every((oc: any) => (
        (oc?.bookmaker === null || isReturnBookmaker(oc?.bookmaker) &&
        typeof oc?.odd === "number"
      )))
    )) &&
    (o?.match === null || isReturnMatch(o?.match)) &&
    o?.betType === null || isReturnBetType(o?.betType)
  );
};

export const createReturnRFBFromSaveFormat = (db: DB) => (propertyFilter: PropertyFilter) => 
  async (rfb: RFBSaveFormat): Promise<ReturnObject<ReturnRFB>> => {
    let messages: MessageWithInfo[] = [];

    const match = await createReturnMatchById(db)(propertyFilter)(rfb.matchId);
    messages = messages.concat(match.messages);

    const betType = await createReturnBetTypeById(db)(rfb.betTypeId);
    messages = messages.concat(betType.messages);

    const bets = await createBets(db)(rfb, betType.result);
    messages = messages.concat(bets.messages);

    const returnRFB = createReturnRFB({
      id: rfb.id,
      isAvailable: rfb.isAvailable,
      match: match.result,
      betType: betType.result,
      bets: bets.result as RFBBet[]
    });

    return createReturnObject({
      messages,
      result: returnRFB,
    });
  };

const createBets = (db: DB) => async (rfb: RFBSaveFormat, betType: ReturnBetType | null): Promise<ReturnObject<RFBBet[]>> => {
  return rfb.bets.reduce(async (acc: Promise<ReturnObject<RFBBet[]>>, bet: RFBBetSaveFormat, betNr: number) => {
    const res = await acc;

    const betRes = await Object.keys(bet.outcomes).reduce(
      createOutcomeObject(db, rfb.id, betType, bet, betNr), 
      Promise.resolve(createReturnObject({
        result: {}
      }))
    );

    return createReturnObject({
      result: res.result?.concat({
        profit: bet.profit,
        isAvailable: bet.isAvailable,
        outcomes: betRes.result as Record<string, RFBOutcome>
      }),
      messages: res.messages.concat(betRes.messages)
    });
  }, Promise.resolve(createReturnObject({
    result: []
  })));
};

const createOutcomeObject = (db: DB, rfbId: UUIDString, betType: ReturnBetType | null, bet: RFBBetSaveFormat, betNr: number) => 
  async (acc: Promise<ReturnObject<Record<string, RFBOutcome>>>, outcomeId: UUIDString): Promise<ReturnObject<Record<string, RFBOutcome>>> => {
    const res = await acc;
    const outcome = bet.outcomes[outcomeId];

    const bookmaker = await createReturnBookmakerById(db)(outcome.bookmaker);
    
    let messages = bookmaker.messages;
    let outcomeName = betType?.outcomeNames?.find(({ id }) => id === outcomeId)?.name;
    if (!outcomeName) {
      outcomeName = "UNKNOWN_OUTCOME";
      messages = messages.concat(createOutcomeOfBetTypeNotSuppliedError({
        identifier: `rfb ${rfbId}`,
        propertyName: `bets[${betNr}]`,
        betTypeName: betType?.name,
        outcomeName: outcomeName,
        outcomeId: outcomeId,
        dbId: db.id,
      }))
    }

    return createReturnObject({
      result: {
        ...res.result,
        [outcomeName]: {
          bookmaker: bookmaker.result,
          odd: outcome.odd,
        }
      },
      messages: res.messages.concat(bookmaker.messages)
    });
  };

export const createReturnRFB = ({
  id,
  isAvailable,
  match = null,
  betType = null,
  bets
}: {
  id: UUIDString;
  isAvailable: boolean;
  match?: ReturnMatch | null;
  betType?: ReturnBetType | null;
  bets: RFBBet[];
}): ReturnRFB => {
  return {
    id,
    isAvailable,
    match,
    betType,
    bets
  };
};