import { DB } from "../db/db.ts";
import {
  OddsSaveFormat,
  retrieveOddsById,
} from "../db/saveFormats/OddsSaveFormat.ts";
import { PropertyFilter } from "../RequestFormats.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import { createReturnBetById, isReturnBet, ReturnBet } from "./ReturnBet.ts";
import {
  createReturnBookmakerById,
  isReturnBookmaker,
  ReturnBookmaker,
} from "./ReturnBookmaker.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnOdds {
  id: UUIDString;
  name: string;
  bookmaker: ReturnBookmaker;
  bets: ReturnBet[];
}

export const isReturnOdds = (o: any): o is ReturnOdds => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  isReturnBookmaker(o?.bookmaker) &&
  Array.isArray(o?.bets) && o?.bets.every(isReturnBet)
);

export const createReturnOddsById = (db: DB) => (propertyFilters: PropertyFilter) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnOdds>> => {
    const savedOdds: OddsSaveFormat = await retrieveOddsById(db)(id);

    if (savedOdds) {
      return createReturnOddsFromSaveFormat(db)(propertyFilters)(savedOdds);
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "odds",
            id,
          }),
        ],
      });
    }
  };

export const createReturnOddsFromSaveFormat = (db: DB) => (propertyFilters: PropertyFilter) => 
  async (odds: OddsSaveFormat): Promise<ReturnObject<ReturnOdds>> => {
    const bookmaker = await createReturnBookmakerById(db)(odds.bookmakerId);

    let messages = bookmaker.messages || [], result = null;
    if (bookmaker.result) {
      const bets = await Promise.all(
        odds.childIds.map(createReturnBetById(db)(propertyFilters)),
      );

      messages = bets.reduce(
        (acc, bet) => acc.concat(bet.messages || []),
        messages,
      );

      result = createReturnOdds({
        id: odds.id,
        name: odds.name,
        bookmaker: bookmaker.result,
        bets: bets
          .filter((b) => b.result != null)
          .map((b) => b.result) as ReturnBet[],
      });
    }

    return createReturnObject({
      result,
      messages,
    });
  };

export const createReturnOdds = ({
  id,
  name,
  bookmaker,
  bets = [],
}: {
  id: UUIDString;
  name: string;
  bookmaker: ReturnBookmaker;
  bets: ReturnBet[];
}): ReturnOdds => ({
  id,
  name,
  bookmaker,
  bets,
});
