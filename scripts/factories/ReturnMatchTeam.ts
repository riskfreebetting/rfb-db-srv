import { DB } from "../db/db.ts";
import {
  MatchTeamSaveFormat,
  retrieveMatchTeamById,
} from "../db/saveFormats/MatchTeamSaveFormat.ts";
import { isReturnTeam, createReturnTeamById, ReturnTeam } from "./ReturnTeam.ts"
import { generateUUIDString, trace, isUUIDString, UUIDString } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnMatchTeam {
  id: UUIDString;
  name: string;
  team: ReturnTeam | null;
  matchId: UUIDString;
}

export const isReturnMatchTeam = (o: any): o is ReturnMatchTeam => {
  return (
    isUUIDString(o?.id) &&
    typeof o?.name === "string" &&
    isUUIDString(o?.matchId) &&
    (o?.team === null || isReturnTeam(o?.team))
  );
};

export const createReturnMatchTeamById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnMatchTeam>> => {
    const savedMatchTeam: MatchTeamSaveFormat = await retrieveMatchTeamById(db)(
      id,
    );
    if (savedMatchTeam) {
      return createReturnMatchTeamFromSaveFormat(db)(savedMatchTeam);
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "matchTeam",
            id,
          }),
        ],
      });
    }
  };

export const createReturnMatchTeamFromSaveFormat = (db: DB) => async (
  saved: MatchTeamSaveFormat
): Promise<ReturnObject<ReturnMatchTeam>> => {
  const { messages, result: team } = await createReturnTeamById(db)(saved.teamId);

  const matchTeam = createReturnMatchTeam({
    id: saved.id,
    name: saved.name,
    matchId: saved.parentId,
    team,
  });

  return createReturnObject<ReturnMatchTeam>({
    result: matchTeam,
    messages
  });
};

export const createReturnMatchTeam = ({
  id,
  name,
  team,
  matchId,
}: {
  id: UUIDString;
  name: string;
  team: ReturnTeam | null;
  matchId: UUIDString;
}): ReturnMatchTeam => ({
  id,
  name,
  team,
  matchId,
});
