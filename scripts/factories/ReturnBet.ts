import { DB } from "../db/db.ts";
import {
  BetSaveFormat,
  OutcomeSaveFormat,
  retrieveBetById,
} from "../db/saveFormats/BetSaveFormat.ts";
import { PropertyFilter } from "../RequestFormats.ts";
import {
  DateNumber,
  findProperty,
  isDateNumber,
  isUUIDString,
  Property,
  UUIDString,
} from "../utils.ts";
import {
  createReturnBetTypeById,
  isReturnBetType,
  ReturnBetType,
} from "./ReturnBetType.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnBet {
  id: UUIDString;
  type: ReturnBetType;
  date: DateNumber;
  outcomes: ReturnOutcome[] | null; // from properties
}

export const isReturnBet = (o: any): o is ReturnBet => (
  isUUIDString(o?.id) &&
  isReturnBetType(o?.type) &&
  ((Array.isArray(o?.outcomes) && o?.outcomes.every(isReturnOutcome)) ||
    o?.outcomes === null)
);

export interface ReturnOutcome {
  name: string;
  odd: number;
  date: DateNumber;
}

export const isReturnOutcome = (o: any): o is ReturnOutcome => (
  typeof o?.name === "string" &&
  typeof o?.odd === "number" &&
  isDateNumber(o?.date)
);

export const createReturnBetById = (db: DB) => (propertyFilters: PropertyFilter) => 
  async (id: UUIDString): Promise<ReturnObject<ReturnBet>> => {
    const savedBet: BetSaveFormat = await retrieveBetById(db)(id);

    if (savedBet) {
      return await createReturnBetFromSaveFormat(db)(propertyFilters)(savedBet);
    } else {
      return createReturnObject<ReturnBet>({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "bet",
            id,
          }),
        ],
      });
    }
  };

export const createReturnBetFromSaveFormat = (db: DB) => (propertyFilters: PropertyFilter) => 
  async (bet: BetSaveFormat): Promise<ReturnObject<ReturnBet>> => {
    const outcomes = createSeveralOutcomesFromSaveFormat(propertyFilters)(bet.properties);

    const betType = await createReturnBetTypeById(db)(bet.typeId);

    const result = betType.result 
      ? createReturnBet({
        id: bet.id,
        date: bet.date,
        type: betType.result,
        outcomes,
      })
      : null;

    return createReturnObject<ReturnBet>({
      result,
      messages: betType.messages,
    });
  };

export const createSeveralOutcomesFromSaveFormat = (propertyFilter: PropertyFilter) => (
  properties: Property<any>[],
): ReturnOutcome[] | null => {
  const outcomesProperty = findProperty<OutcomeSaveFormat[]>("outcomes")(propertyFilter)(properties);
  
  if (outcomesProperty) {
    return createOutcomesFromProperty(outcomesProperty);
  } else {
    return null;
  }
};

export const createOutcomesFromProperty = (
  { value, date }: Property<OutcomeSaveFormat[]>,
): ReturnOutcome[] => {
  return value.map(createOutcomeFromSaveFormat(date));
};

export const createOutcomeFromSaveFormat = (date: DateNumber) =>
  (outcome: OutcomeSaveFormat): ReturnOutcome => {
    return createReturnOutcome({
      name: outcome.name,
      odd: outcome.odd,
      date,
    });
  };

export const createReturnOutcome = ({
  name,
  odd,
  date,
}: {
  name: string;
  odd: number;
  date: DateNumber;
}): ReturnOutcome => ({
  name,
  odd,
  date,
});

export const createReturnBet = ({
  id,
  type,
  date,
  outcomes = [],
}: {
  id: UUIDString;
  date: DateNumber;
  type: ReturnBetType;
  outcomes?: ReturnOutcome[] | null;
}): ReturnBet => ({
  id,
  date,
  type,
  outcomes,
});