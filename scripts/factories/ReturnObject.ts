import { UUIDString } from "../utils.ts";

export interface ReturnObject<T> {
  result: T | null;
  messages: MessageWithInfo[];
  successful: boolean;
}

export interface MessageWithInfo {
  ns: "db";
  code: string;
  type: keyof typeof MessageTypes;
  msg: string;
  replaceableMsg: string;
  params: string[];
}

export enum MessageTypes {
  "E",
  "W",
  "I",
  "S",
  "M",
}

export interface ErrorWithInfo extends MessageWithInfo {
  type: "E";
  error: Error;
}

export const isErrorWithInfo = (o: any): o is ErrorWithInfo => (
  isMessageWithInfo(o) &&
  o.type === "E"
);

export const isMessageWithInfo = (o: any): o is MessageWithInfo => (
  o?.type in MessageTypes &&
  typeof o?.msg === "string" &&
  typeof o?.replaceableMsg === "string" &&
  Array.isArray(o?.params) && o?.params.every((e: any) => typeof e === "string")
);

export const createReturnObject = <T>({
  result = null,
  messages = [],
}: {
  result?: T | null;
  messages?: MessageWithInfo[];
}={}): ReturnObject<T> => {
  const successful = messages.every((m) => m.type.toUpperCase() !== "E");

  return {
    result,
    messages,
    successful,
  };
};

export const createNotFoundError = ({
  dbId = "",
  type = "",
  id,
}: {
  dbId: string;
  type: string;
  id: UUIDString;
}) =>
  createErrorWithInfo({
    ErrorClass: EntryNotFoundError,
    code: "E001",
    msg:
      `EntryNotFound: Could not find entry of type '{{${type}}}' with id '{{${id}}}' in database '{{${dbId}}}'.`,
  });

export class EntryNotFoundError extends Error {}

export const createUnalignableTypeError = ({
  identifier = "",
  propertyName = "",
  actualType = "undefined",
  expectedType = "undefined",
}) =>
  createErrorWithInfo({
    ErrorClass: UnalignableTypeError,
    code: "E002",
    msg:
      `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: '{{${actualType}}}' is not assignable to type '{{${expectedType}}}'.`,
  });

export class UnalignableTypeError extends Error {}

export const createNotFoundInputError = ({
  identifier = "",
  propertyName = "",
  type = "",
  id = "",
  dbId = "",
}) =>
  createErrorWithInfo({
    ErrorClass: NotFoundInputError,
    code: "E003",
    msg:
      `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: Could not find entry of type '{{${type}}}' with id '{{${id}}}' in database '{{${dbId}}}'.`,
  });

export class NotFoundInputError extends Error {}

export const createArrayNotOfRightSizeError = ({
  identifier = "",
  propertyName = "",
  expectedMinLength = 1,
  expectedMaxLength = Infinity,
  actualLength = 0,
}) =>
  createErrorWithInfo({
    ErrorClass: ArrayNotOfRightSizeError,
    code: "E004",
    msg:
      `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: Length of array has to be between {{${expectedMinLength}}} and {{${expectedMaxLength}}} but is {{${actualLength}}}.`,
  });

export class ArrayNotOfRightSizeError extends Error {}

export const createNumberNotInRightRangeError = ({
  identifier = "",
  propertyName = "",
  expectedMin = -Infinity,
  expectedMax = Infinity,
  actual = 0,
}) =>
  createErrorWithInfo({
    ErrorClass: ArrayNotOfRightSizeError,
    code: "E005",
    msg:
      `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: Number has to be larger than {{${expectedMin}}} and smaller than {{${expectedMax}}} but is {{${actual}}}.`,
  });

export class NumberNotInRightRangeError extends Error {}

export const createUnalignableTypeOfValueError = ({
  identifier = "",
  propertyName = "",
  value = "any",
  expectedType = "any",
}) => createErrorWithInfo({
  ErrorClass: UnalignableTypeOfValueError,
  code: "E006",
  msg: 
    `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: '{{${value}}}' is not assignable to type '{{${expectedType}}}'.`,
})

export class UnalignableTypeOfValueError extends Error {}

export const createNotFoundDBError = (dbId: string) =>
  createErrorWithInfo({
    ErrorClass: NotFoundDBError,
    code: "E007",
    msg: `Could not find a database with the inputted id '{{${dbId}}}'. No database was deleted.`
  });

export class NotFoundDBError extends Error {}

export const createOutcomeOfBetTypeNotSuppliedError = ({
  identifier = "",
  propertyName = "",
  betTypeName = "",
  outcomeName = "",
  outcomeId = "",
  dbId = "",
}) =>
  createErrorWithInfo({
    ErrorClass: OutcomeOfBetTypeNotSuppliedError,
    code: "E008",
    msg:
      `Input for property '{{${propertyName}}}' of '{{${identifier}}}' is falsy: Outcome '{{${outcomeName}}}' with id '{{${outcomeId}}}' of BetType '{{${betTypeName}}}' could not be found in supplied outcomes in database '{{${dbId}}}'.`,
  });

export class OutcomeOfBetTypeNotSuppliedError extends Error {}

export const createErrorWithInfo = ({
  ErrorClass = Error,
  code,
  msg = "",
}: {
  ErrorClass?: new (msg: string) => Error;
  code?: string;
  msg: string;
}): ErrorWithInfo => {
  const msgObj = createMessageWithInfo(code, msg);

  return {
    ...msgObj,
    type: "E",
    error: new ErrorClass(msgObj.msg),
  };
};

export const createNotFoundDBWarning = (dbId: string) => 
  createMessageWithInfo("W001", `Could not find a database with the inputted id '{{${dbId}}}'. Therefore no results were returned.`);

export const createMessageWithInfo = (
  code = "E000",
  replaceableMsg = "",
): MessageWithInfo => {
  const msgWithParams = code + ": " + replaceableMsg;
  const paramsWithBrackets = findParamsInMessage(msgWithParams);

  const params = paramsWithBrackets.map((s: string) =>
    s.slice(2, s.length - 2)
  );
  const msgWithPlaceholders = paramsWithBrackets.reduce((acc, param, i) => (
    acc.replace(param, `{{${i}}}`)
  ), msgWithParams);
  const msg = params.reduce(
    (acc, curr, i) => acc.replace(`{{${i}}}`, curr),
    msgWithPlaceholders,
  );

  let type: keyof typeof MessageTypes;
  const firstLetter = code[0].toUpperCase();
  if (firstLetter in MessageTypes) {
    type = firstLetter as keyof typeof MessageTypes;
  } else {
    type = "M";
  }

  return {
    ns: "db",
    code,
    type,
    replaceableMsg: msgWithPlaceholders,
    msg,
    params,
  };
};

function findParamsInMessage(msg = "") {
  let paramsWithBrackets: string[] = [],
    currentMatch = "";

  for (let i = 0; i < msg.length; i++) {
    const lastC = msg[i - 1],
      c = msg[i], 
      nextC = msg[i + 1];

    if (c === "{" && nextC === "{" && currentMatch === "") {
      currentMatch = c;
    } else if (currentMatch) {
      currentMatch += c;

      if (currentMatch && c === "}" && lastC === "}") {
        paramsWithBrackets.push(currentMatch);

        currentMatch = "";
      }
    }
  }

  return paramsWithBrackets;
}

type WithId = { id: UUIDString };
export const handleSeveralSettledObjects = <T>(dbObjects: WithId[], settledPromises: PromiseSettledResult<ReturnObject<T>>[]) => {
  const messages = settledPromises.flatMap((returnObject, i) => (
    returnObject.status === "fulfilled" 
      ? returnObject.value.messages.map(addObjectId(dbObjects[i]))
      : [addObjectId(dbObjects[i])(returnObject.reason)]
  ));
  
  const successful = settledPromises.filter((rfb) => rfb.status === "fulfilled") as PromiseFulfilledResult<ReturnObject<T>>[];
  const result = successful.map(res => res.value.result) as T[];

  return createReturnObject({
    result,
    messages
  });
};

export const handleSeveralSettledPromises = <T>(dbObjects: WithId[], settledPromises: PromiseSettledResult<T>[]) => {
  const messages = settledPromises.flatMap((returnObject, i) => (
    returnObject.status === "rejected" 
      ? [addObjectId(dbObjects[i])(returnObject.reason)]
      : []
  ));
  
  const successful = settledPromises.filter(rfb => rfb.status === "fulfilled") as PromiseFulfilledResult<T>[];
  const result = successful.map(res => res.value) as T[];

  return createReturnObject({
    result,
    messages
  });
};

function addObjectId(dbObject: WithId) {
  return (msg: MessageWithInfo) => ({
    ...msg,
    id: dbObject.id
  });
}