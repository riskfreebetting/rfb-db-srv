import { DB } from "../db/db.ts";
import {
  BookmakerSaveFormat,
  retrieveBookmakerById,
} from "../db/saveFormats/BookmakerSaveFormat.ts";
import { isUUIDString, UUIDString, trace } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  isErrorWithInfo,
  ReturnObject,
} from "./ReturnObject.ts";
import { TaxTypes } from "../db/saveFormats/BookmakerSaveFormat.ts";

export interface ReturnBookmaker {
  id: UUIDString;
  name: string;
  tax: keyof typeof TaxTypes;
}

export const isReturnBookmaker = (o: any): o is ReturnBookmaker => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  o?.tax in TaxTypes
);

export const createReturnBookmakerById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnBookmaker>> => {
    const savedBookmaker: BookmakerSaveFormat = await retrieveBookmakerById(db)(
      id,
    );

    if (savedBookmaker) {
      return createReturnObject({
        result: createReturnBookmakerFromSaveFormat(savedBookmaker),
      });
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "bookmaker",
            id,
          }),
        ],
      });
    }
  };

export const createReturnBookmakerFromSaveFormat = (
  saved: BookmakerSaveFormat,
): ReturnBookmaker => {
  return createReturnBookmaker({
    id: saved.id,
    name: saved.name,
    tax: saved.tax
  });
};

export const createReturnBookmaker = ({
  id,
  tax,
  name,
}: {
  id: UUIDString;
  tax: keyof typeof TaxTypes,
  name: string;
}): ReturnBookmaker => ({
  id,
  tax,
  name,
});
