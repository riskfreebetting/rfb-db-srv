import { DB } from "../db/db.ts";
import {
  retrieveCompetitionById,
  CompetitionSaveFormat,
} from "../db/saveFormats/CompetitionSaveFormat.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import { createReturnCountryById, isReturnCountry, ReturnCountry } from "./ReturnCountry.ts";
import {
  createNotFoundError,
  createReturnObject,
  MessageWithInfo,
  ReturnObject,
} from "./ReturnObject.ts";
import { createReturnSportById, isReturnSport, ReturnSport } from "./ReturnSport.ts";

export interface ReturnCompetition {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
  sport: ReturnSport | null;
  country: ReturnCountry | null;
}

export const isReturnCompetition = (o: any): o is ReturnCompetition => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" &&
  Object.keys(o?.alternativeNames ?? {}).every(x => isUUIDString(x)) && 
  Object.values(o?.alternativeNames ?? {}).every(x => (
    Array.isArray(x) &&
    x?.every(x => typeof x === "string")
  )) &&
  (isReturnSport(o?.sport) || o?.sport === null) &&
  (isReturnCountry(o?.country) || o?.country === null)
);

export const createReturnCompetitionById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnCompetition>> => {
    const savedCompetition: CompetitionSaveFormat = await retrieveCompetitionById(db)(id);

    if (savedCompetition) {
      return createReturnCompetitionFromSaveFormat(db)(savedCompetition);
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "competition",
            id,
          }),
        ],
      });
    }
  };

export const createReturnCompetitionFromSaveFormat = (db: DB) => async (
  saved: CompetitionSaveFormat,
): Promise<ReturnObject<ReturnCompetition>> => {
  let messages: MessageWithInfo[] = [];

  const sport = await createReturnSportById(db)(saved.sportId);
  messages = messages.concat(sport.messages);

  const country = await createReturnCountryById(db)(saved.countryId);
  messages = messages.concat(country.messages);

  const result = createReturnCompetition({
    id: saved.id,
    name: saved.name,
    alternativeNames: saved.alternativeNames,
    sport: sport.result,
    country: country.result
  });

  return createReturnObject<ReturnCompetition>({
    result,
    messages
  });
};

export const createReturnCompetition = ({
  id,
  name,
  alternativeNames,
  sport=null,
  country=null
}: {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
  sport?: ReturnSport | null;
  country?: ReturnCountry | null;
}) => ({
  id,
  name,
  alternativeNames,
  sport,
  country
});
