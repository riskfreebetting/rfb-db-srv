import { DB } from "../db/db.ts";
import {
  BetTypeSaveFormat,
  OutcomeNameSaveFormat,
  retrieveBetTypeById,
} from "../db/saveFormats/BetTypeSaveFormat.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnBetType {
  id: UUIDString;
  name: string;
  outcomeNames: ReturnOutcomeName[];
}

export interface ReturnOutcomeName {
  id: UUIDString;
  name: string;
}

export const isReturnBetType = (o: any): o is ReturnBetType => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  Array.isArray(o?.outcomeNames) && o?.outcomeNames.every(isReturnOutcomeName)
);

export const isReturnOutcomeName = (o: any): o is ReturnOutcomeName => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string"
);

export const createReturnBetTypeById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnBetType>> => {
    const savedBetType: BetTypeSaveFormat = await retrieveBetTypeById(db)(id);
    if (savedBetType) {
      return createReturnObject<ReturnBetType>({
        result: createReturnBetTypeFromSaveFormat(savedBetType),
      });
    } else {
      return createReturnObject<ReturnBetType>({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "betType",
            id,
          }),
        ],
      });
    }
  };

export const createReturnBetTypeFromSaveFormat = (
  saved: BetTypeSaveFormat,
): ReturnBetType => {
  const outcomeNames = saved.outcomeNames.map(
    createReturnOutcomeNameFromSaveFormat,
  );

  return createReturnBetType({
    id: saved.id,
    name: saved.name,
    outcomeNames,
  });
};

export const createReturnOutcomeNameFromSaveFormat = (
  outcomeName: OutcomeNameSaveFormat,
): ReturnOutcomeName => {
  return createReturnOutcomeName({
    id: outcomeName.id,
    name: outcomeName.name,
  });
};

export const createReturnOutcomeName = ({
  id,
  name,
}: {
  id: UUIDString;
  name: string;
}): ReturnOutcomeName => ({
  id,
  name,
});

export const createReturnBetType = ({
  id,
  name,
  outcomeNames = [],
}: {
  id: UUIDString;
  name: string;
  outcomeNames?: ReturnOutcomeName[];
}): ReturnBetType => ({
  id,
  name,
  outcomeNames,
});
