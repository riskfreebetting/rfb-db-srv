import { DB } from "../db/db.ts";
import {
  retrieveSportById,
  SportSaveFormat,
} from "../db/saveFormats/SportSaveFormat.ts";
import { isUUIDString, UUIDString } from "../utils.ts";
import {
  createNotFoundError,
  createReturnObject,
  ReturnObject,
} from "./ReturnObject.ts";

export interface ReturnSport {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}

export const isReturnSport = (o: any): o is ReturnSport => (
  isUUIDString(o?.id) &&
  typeof o?.name === "string" &&
  o?.alternativeNames != null && typeof o?.alternativeNames === "object" &&
  Object.keys(o?.alternativeNames ?? {}).every(x => isUUIDString(x)) && 
  Object.values(o?.alternativeNames ?? {}).every(x => (
    Array.isArray(x) &&
    x?.every(x => typeof x === "string")
  ))
);

export const createReturnSportById = (db: DB) =>
  async (id: UUIDString): Promise<ReturnObject<ReturnSport>> => {
    const savedSport: SportSaveFormat = await retrieveSportById(db)(id);

    if (savedSport) {
      return createReturnObject({
        result: createReturnSportFromSaveFormat(savedSport),
      });
    } else {
      return createReturnObject({
        messages: [
          createNotFoundError({
            dbId: db.id,
            type: "sport",
            id,
          }),
        ],
      });
    }
  };

export const createReturnSportFromSaveFormat = (
  saved: SportSaveFormat,
): ReturnSport => (
  createReturnSport({
    id: saved.id,
    name: saved.name,
    alternativeNames: saved.alternativeNames
  })
);

export const createReturnSport = ({
  id,
  name,
  alternativeNames
}: {
  id: UUIDString;
  name: string;
  alternativeNames: Record<UUIDString, string[]>;
}) => ({
  id,
  name,
  alternativeNames
});
