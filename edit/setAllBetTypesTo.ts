import { createFsDB } from "../scripts/db/fsDB.ts";
import { BetSaveFormat } from "../scripts/db/saveFormats/BetSaveFormat.ts";

const wantedTypeId = "00932ca9-5455-455b-a40c-5b53eba0a8a8";
const db = await createFsDB({ id: "bwin_0" });

const bets = await db.retrieveAll("bets");

const newBets = await Promise.all(bets.map((bet: BetSaveFormat) => {
  if (bet.typeId !== wantedTypeId) {
    return db.insert("bets", {
      ...bet,
      typeId: wantedTypeId
    });
  } else {
    return null;
  }
}));


console.log("updated bets", newBets.filter(b => b !== null).length);